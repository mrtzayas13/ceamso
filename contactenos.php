<?php include('inc/config.php'); ?>



<head>



<title>Contáctenos | <?=SITENAME;?></title>

<meta name="description" content="Estamos ubicados sobre Capitán Pedro Carpinelli 3704 y Cecilio Ávila. Puede ponerse en contacto con nosotros a los teléfonos (+595 21) 604 332 - (+595 21) 662 585." />

<meta name="keywords" content="<?=GRALKEYS;?>, telefono, email, direccion, ubicacion, como llegar" />



<?php include('inc/head.php'); ?>

<!-- jquery - utilidades para formularios -->
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-form.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<script type="text/javascript">
  $(document).ready(function($){

  	$("#form_contacto").validate({
		event: "blur",
		rules: {
			nombre: "required",
			apellido: "required",
			email: {
				required: true, 
				email: true
			},	
			telefono: "required",	
			mensaje: "required"	
		},	
		errorElement: "span",	
		submitHandler: function(form) {
	
			$.ajax({	
				beforeSend: function(objeto) {	
					$( "#msj_contacto" ).addClass( "aguarde" );	
					$('#msj_contacto span').html('Aguarde..');	
				},
	
				type: "POST",	
				url: "ajax/form_contacto.php",	
				contentType: "application/x-www-form-urlencoded",	
				data: $("#form_contacto").serialize(),	
				dataType: "json",	
				success: function(json){
					$(  "#msj_contacto" ).removeClass( "aguarde" )	;
					$(  "#msj_contacto" ).removeClass( "incorrecto" );	
					if (json.success == true) {	
						$("#form_contacto")[0].reset();	
						$( "#msj_contacto" ).addClass( "correcto" );	
						
					} else {	
						$( "#msj_contacto" ).addClass( "incorrecto" );	
					}
					$('#msj_contacto span').html(json.mensaje);	
	
				}
	
			});
	
		}
	
	});
	  
  });

  // Definir una devolución de llamada que se ejecuta cuando el recaptcha ha expirado
  function recaptchaExpired(){
    alert("Your Recaptcha has expired, please verify it again !");
  }
</script>



</head>



<body class="sec-contactenos">



	<?php include('inc/header.php'); ?>




	<section id="titulo">

		<div class="container">

			<h1 class="underline"><strong>Contáctenos</strong><span></span></h1>

		</div>

	</section>



	<section id="content">

		<div class="container">

			<div class="row">

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt20">

					<h2 class="underline" style="font-size: 21px; margin-bottom: 20px;">Datos de contacto <span></span></h2>

					<ul class="datos-contacto mb30">

						<li class="pt5"><img src="images/ico-dir-footer.png" alt="Direccion" class="mr10">Capitán Pedro Carpinelli 3704 y Cecilio Ávila, Asunción.</li>

						<li class="pt5"><img src="images/ico-tel-footer.png" alt="Telefono" class="mr10">(+595 21) 662 585</li>

						<li class="pt5"><img src="images/ico-tel-footer.png" alt="Telefono" class="mr10">(+595) 986 118 974</li>

						<li class="pt5"><img src="images/ico-mail-footer.png" alt="E-mail" class="mr10">ceamso@ceamso.org.py</li>

					</ul>



					<h2 class="underline" style="font-size: 21px; margin-bottom: 20px;">Horarios de atención<span></span></h2>

					<p>Lunes a jueves de 8:00 a 17:00 <br>Viernes de 8:00 a 12:00</p>

				</div>

				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				
					<div id="msj_contacto" class="mensajes"><span></span></div>

					<form id="form_contacto" class="form-contactos">

						<div class="row">

							<fieldset class="col-lg-6 col-md-6">

								<label for="nombre">Nombre</label>

								<input type="text" name="nombre">

							</fieldset>

							<!--
							<fieldset class="col-lg-6 col-md-6">

								<label for="apellido">Apellido</label>

								<input type="text" name="apellido">

							</fieldset>
							

							<fieldset class="col-lg-6 col-md-6">

								<label for="telefono">Teléfono</label>

								<input type="text" name="telefono">

							</fieldset>
							-->
							
							<fieldset class="col-lg-6 col-md-6">

								<label for="email">E-mail</label>

								<input type="text" name="email">

							</fieldset>

							<fieldset class="col-lg-12 col-md-12">

								<label for="mensaje">Mensaje</label>

								<textarea name="mensaje" id="" cols="30" rows="10"></textarea>

							</fieldset>

							<fieldset class="col-lg-12 col-md-12">
							
								<div class="g-recaptcha" data-sitekey="6LfPpYEaAAAAAEAeax0p_GzJyTkmoGLZ3OAil6e9" data-expired-callback="recaptchaExpired"></div>

								<input type="submit" value="Enviar mensaje" class="btn verde right">

							</fieldset>

						</div>

					</form>

				</div>

			</div>



		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

