<?php include('inc/config.php'); ?>



<?php



$id = TextHelper::cleanNumber($_GET['id']);

$qproyectos = new DBQuery("SELECT n.*, a.* 

						  FROM proyectos n 

						  left outer join areas a on a.id = n.area_id 

						  where n.activo = 'SI' and n.id = '{$id}' ");

$rs = $db->executeQuery($qproyectos);



$area_id = $rs[0]['area_id'];



if(empty($rs))

	header("Location: " . URL);



$qgaleria = new DBQuery("SELECT imagen FROM galeria_proyectos where proyecto_id = '{$id}'");

$galeria = $db->executeQuery($qgaleria);


$qods = new DBQuery("SELECT objetivo_desarrollo.titulo_esp, objetivo_desarrollo.titulo_eng, objetivo_desarrollo.img_esp, objetivo_desarrollo.img_eng 

					FROM objetivo_proyectos 

					INNER JOIN objetivo_desarrollo ON objetivo_proyectos.id_objetivo = objetivo_desarrollo.id 

					WHERE objetivo_proyectos.id_proyectos = '{$id}' ORDER BY id_objetivo ASC");

$ods = $db->executeQuery($qods);

//ULTIMAS NOTICIAS

$qnoticias_s = new DBQuery("SELECT *

									FROM noticias n 

									where n.activo = 'SI' AND n.area_id = '{$area_id}'

									order by n.id desc limit 4");

$noticias_s = $db->executeQuery($qnoticias_s);
?>

<head>



<title><?php echo $rs[0]['titulo'.$idioma]?> | <?=SITENAME;?></title>
<meta name="description" content="<?php echo strip_tags($rs[0]['copete'.$idioma]); ?>" />
<meta property="og:title" content="<?php echo $rs[0]['titulo'.$idioma]?>" />
<meta property="og:description" content="<?php echo strip_tags($rs[0]['copete'.$idioma]); ?>" />
<meta property="og:image" content="<?php echo CONF_SITE_URL.'/upload/galeria_proyectos/g__'.$galeria[0]['imagen'] ?>" />
<meta name="keywords" content="<?=GRALKEYS;?>, proyecto, proyectos" />

<?php include('inc/head.php'); ?>

<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css"/>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slider-galeria').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed: 800,
			slidesToShow: 1,
			centerMode: true,
			autoplay: true,
			variableWidth: true
		});

		$(".fancybox").fancybox();
	});
</script>

</head>

<body class="sec-proyectos sec-<? echo TextHelper::area_clase($rs[0]['area_id']); ?>">

	<?php include('inc/header.php'); ?>

	<section id="titulo-slider">

		<div class="container">
			
			<p class="breadcrumb"><a href="proyectos">PROYECTOS</a> / <a href="proyectos">PROYECTOS EN EJECUCIÓN</a> / <?php echo $rs[0]['area'.$idioma]?></p>

			<h1 class="underline"><?php echo $rs[0]['titulo'.$idioma]?><span></span></h1>

			<div class="copete texto-corrido">

				<p><?php echo $rs[0]['copete'.$idioma]?></p>

			</div>

		</div>

			

		<div class="slider-galeria">

			<?php foreach($galeria as $rs_img){ ?>

				<div><a href="<?php echo CONF_SITE_URL.'/upload/galeria_proyectos/g__'.$rs_img['imagen'] ?>" class="fancybox" title="Aca una breve descripcion" rel="galeria"><span class="zoom"></span><span class="degradado"></span><img src="<?php echo CONF_SITE_URL.'/upload/galeria_proyectos/g__'.$rs_img['imagen'] ?>" alt=""></a></div>

			<?php }?>

		</div>



		<div class="separador"></div>

	</section>



	<section id="content">

		<div class="container <?php if(count($noticias_s) > 0){ echo 'sidebar-bg';} ?>">

			<div class="row">

				<div class="col-lg-8 col-md-8 col-sm-12 pr30">

					

					<?php echo $rs[0]['descripcion'.$idioma]?>

					

				</div>

				<div class="col-lg-8 col-md-8 col-sm-12  container-fluid pr30 pt30">



					<?php
					if(count($ods) > 0){?>
					<div class="col-lg-4 col-md-4 col-ms-4 col-sm-6 col-xs-6 pt15 pr25 pl0">
						<?php 
						if($idioma == '_esp'){?>
							<img src="<?php echo CONF_SITE_URL . '/images/ods/esp/ODS.png';?>" alt = "Objetivos de desarrollo sostenible" class="img-responsive">
						<?php }else{ ?>
							<img src="<?php echo CONF_SITE_URL . '/images/ods/eng/ODS.png';?>" alt= "Sustainable development goals" class="img-responsive">
						<?php } ?>
					</div>

					<?php foreach($ods as $rs_ods){?>
							<div class="col-lg-2 col-md-2 col-ms-2 col-xs-3 pt20 pr7 pl0">
								<img src="<?php echo CONF_SITE_URL . '/images/ods/'. $rs_ods['img'.$idioma];?>" alt="<?php echo $rs_ods['titulo'.$idioma]; ?>" class="img-responsive">
							</div>
					<?php }}?>
					
						



				</div>

				<?php include('inc/sidebar-proyectos.php'); ?>

			</div>

		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

