<?php include('inc/config.php'); ?>



<?php



$id = TextHelper::cleanNumber($_GET['id']);



$sql = ($id != 0) ? "and n.id = '{$id}'" : "";



$historia_id = ($id != 0) ? $id : 0;



$qvideo = new DBQuery("SELECT n.*

						  FROM videos n 

						  where n.activo = 'SI' 

						  {$sql} 

						  ORDER BY id desc

						  LIMIT 1");

$rs_v = $db->executeQuery($qvideo);



$area_id = ($rs_v[0]['area_id'] != 0) ? $rs_v[0]['area_id'] : 0;

?>



<head>



<title>Videos | <?=SITENAME;?></title>

<meta name="description" content="Nuestros videos." />

<meta name="keywords" content="<?=GRALKEYS;?>, videos, youtube" />



<?php include('inc/head.php'); ?>



<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>

<script type="text/javascript">

	$(function () {

		  $('[data-toggle="tooltip"]').tooltip();

	});

</script>



<script type="text/javascript"> 



	$(document).ready(function(){	

		cargaPaginacion(1,<?php echo $historia_id?>,<?php echo $area_id?>);	

	});

	

	var pagina_actual = 1;

	function cargaPaginacion(p_pagina,historia_id, area_id ){



		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 

		$.ajax({

			type: 'GET',

			url: '<?=CONF_SITE_URL?>ajax/videos.php',

			data: { 

				p: p_pagina,

				historia_id : historia_id,

				area_id : area_id

			},

			success: function(p_html){

				pagina_actual = p_pagina;

				$('#contenido').html(p_html);

			}

		});

	}

</script>



</head>



<body class="sec-videos">



	<?php include('inc/header.php'); ?>



	<section id="titulo">

		<div class="container">

			<p class="breadcrumb">¿Qué hacemos?</p>

			<h1 class="underline">Galeria de <strong>Videos</strong><span></span></h1>

		</div>

	</section>

	<section id="content">
		<div class="container">

			<article class="video-article">
				<div class="row">
					<div class="col-lg-8 col-md-8 video"><figure class="videoWrapper"><iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo TextHelper::youtube_url($rs_v[0]['youtube_url'])?>" frameborder="0" allowfullscreen></iframe></figure></div>

					<div class="col-lg-4 col-md-4 desc">
						<h2 class="underline"><?php echo $rs_v[0]['titulo'.$idioma]?><span></span></h2>
						<?php echo $rs_v[0]['descripcion'.$idioma]?>
						<p class="fecha"><img src="images/ico-hora.png" alt="Hora/Fecha"> Publicado el <?php echo TextHelper::convertToLongDate($rs_v[0]['fecha_creacion'])?></p>
					</div>
				</div>
			</article>

			<div class="video-select">
				<ul class="btns-areas">
					<?php foreach($areas as $rs){?>
					<li><a href="javascript:;" onclick="cargaPaginacion(1,0,<?php echo $rs['id']?>)" rel="<?php echo $rs['area'.$idioma]?>"><?php echo $rs['area'.$idioma]?></a></li>
					<?php }?>
				</ul>
				<div class="video-thumbs" id="contenido"></div>
			</div>

		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>

</html>

