<?php include('inc/config.php'); ?>



<?php



$id = TextHelper::cleanNumber($_GET['id']);

$qnoticias = new DBQuery("SELECT n.* , u.nombre, u.apellido

							FROM blog n 

							LEFT OUTER JOIN usuarios u ON u.id_usuario = n.usuario_id

							WHERE n.activo = 'SI' AND n.id = '{$id}'");

$rs = $db->executeQuery($qnoticias);



if(empty($rs))

	header("Location: " . URL);



$qgaleria = new DBQuery("SELECT imagen FROM galeria_blog where blog_id = '{$id}'");

$galeria = $db->executeQuery($qgaleria);

?>



<head>



<title><?php echo $rs[0]['titulo'.$idioma]?> | <?=SITENAME;?></title>
<meta name="description" content="<?php echo $rs[0]['copete'.$idioma]?>" />
<meta name="keywords" content="<?=GRALKEYS;?>, noticias, novedades" />

<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $rs[0]['titulo'.$idioma]?>" />
<meta property="og:description" content="<?php echo strip_tags($rs[0]['copete'.$idioma]); ?>" />
<meta property="og:image" content="<?php echo CONF_SITE_URL.'/upload/galeria_blog/g__'.$galeria[0]['imagen'] ?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "a20e072a-7689-4359-b30f-f29d1e956e8a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slider-galeria-inn').slick({
			dots: false,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			adaptiveHeight: true
		});
	});
</script>

</head>

<body class="sec-blog">

	<?php include('inc/header.php'); ?>

	<section id="titulo">

		<div class="container">

			<p class="breadcrumb"><a href="blog.php">Blog</a></p>

			<h1 class="underline"><strong><?php echo $rs[0]['titulo'.$idioma]?></strong><span></span></h1>

		</div>

	</section>



	<section id="content">

		<div class="container">



			<div class="row mb30">

				<div class="col-lg-12 col-md-12">

					<p class="copete mb10"><?php echo $rs[0]['copete'.$idioma]?></p>

				</div>

				<figure class="col-lg-12 col-md-12 mb20">
					<div class="slider-galeria-inn">
						<?php foreach($galeria as $rs_img){ ?>
							<div>
								<img src="<?php echo CONF_SITE_URL.'/upload/galeria_blog/g__'.$rs_img['imagen'] ?>" alt="<?php echo $rs[0]['titulo'.$idioma]?>">
							</div>
						<?php }?>
					</div>
				</figure>

				<div class="col-lg-8 col-lg-push-4 col-md-8 col-md-push-4 col-sm-12 texto-corrido">
				<?php if(!empty($rs[0]['youtube_url'])){?>
					<figure class="videoWrapper mb20"><iframe width="100%" height="500" src="https://www.youtube.com/embed/<?php echo TextHelper::youtube_url($rs[0]['youtube_url'])?>" frameborder="0" allowfullscreen></iframe></figure>
				<?php }?>
					<?php echo $rs[0]['descripcion'.$idioma]?>

				</div>

				<div class="col-lg-4 col-lg-pull-8 col-md-4 col-md-pull-8 col-sm-12 text-center">

					<p class="fecha-publicacion"><em>Por <?php echo $rs[0]['nombre'] . " " .$rs[0]['apellido']?></em> • <em><img src="images/ico-hora.png" alt="Hora/Fecha"> <?php echo TextHelper::convertToLongDate($rs[0]['fecha_creacion'])?></em></p>

					<div class="compartir-block">

						<p><img src="images/ico-share.png" alt=""> COMPARTIR EN</p>

						<span class='st_facebook_large' displayText='Facebook'></span>

						<span class='st_twitter_large' displayText='Tweet'></span>

						<span class='st_googleplus_large' displayText='Google +'></span>

						<span class='st_linkedin_large' displayText='LinkedIn'></span>

						<span class='st_email_large' displayText='Email'></span>

					</div>

					<a href="blog.php" class="btn linea-gris mt20" style="display: block;"><img src="images/back-arrow.png" alt="Volver">Volver al Blog</a>

				</div>

			</div>

			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6&appId=859925837463925";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-comments" data-href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-width="100%" data-numposts="5"></div>

		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

