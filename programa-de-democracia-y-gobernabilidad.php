<?php include('inc/config.php'); ?>

<head>

<title>Programa de Democracia y Gobernabilidad | <?=SITENAME;?></title>
<meta name="description" content="Es un programa de la Agencia de los Estados Unidos para el Desarrollo Internacional (USAID) implementado por CEAMSO con el objetivo de fortalecer la capacidad de instituciones claves del gobierno, incrementar la transparencia y luchar contra la corrupción." />
<meta name="keywords" content="<?=GRALKEYS;?>, usaid, democracia, gobernabilidad" />

<?php include('inc/head.php'); ?>

<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css"/>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slider-galeria').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed: 800,
			slidesToShow: 1,
			centerMode: true,
			autoplay: true,
			variableWidth: true
		});

		$(".fancybox").fancybox();

		$('.item-pdg .leermas').click(function(){
			var $this = $(this);
			if($this.parent().children('.texto').is(':visible')) {
				$this.html('Mostrar más <span>+</span>')
			} else {
				$this.html('Mostrar menos <span>-</span>')
			}
			$this.parent().children('.texto').slideToggle();
		});

		$('.circle-pdg .img').click(function(){
			var $this = $(this);
			if($this.parent().children('p').is(':visible')) {
				$this.children('span').text('+')
			} else {
				$this.children('span').text('-')
			}
			$this.parent().children('p').slideToggle();
		});
	});
</script>

</head>

<body class="sec-proyectos sec-area1">

	<?php include('inc/header.php'); ?>

	<section id="titulo-slider">
		<div class="container">
			<p class="breadcrumb"><a href="proyectos">PROYECTOS</a> / <a href="proyectos">PROYECTOS EN EJECICIÓN</a> / <a href="proyectos">GOBERNABILIDAD Y FORTALECIMIENTO INSTITUCIONAL</a></p>
			<h1 class="underline">Programa de Democracia y Gobernabilidad<span></span></h1>
			<div class="copete">
				<p>Es un programa de la Agencia de los Estados Unidos para el Desarrollo Internacional (USAID) implementado por CEAMSO con el objetivo de fortalecer la capacidad de instituciones claves del gobierno, incrementar la transparencia y luchar contra la corrupción.</p>
			</div>
		</div>
			
		<div class="slider-galeria">
			<div><a href="images/slider/slide1.jpg" class="fancybox" title="Aca una breve descripcion" rel="galeria"><span class="zoom"></span><span class="degradado"></span><img src="images/slider/slide1.jpg" alt=""></a></div>
			<div><a href="images/slider/slide2.jpg" class="fancybox" title="Aca una breve descripcion" rel="galeria"><span class="zoom"></span><span class="degradado"></span><img src="images/slider/slide2.jpg" alt=""></a></div>
			<div><a href="images/slider/slide3.jpg" class="fancybox" title="Aca una breve descripcion" rel="galeria"><span class="zoom"></span><span class="degradado"></span><img src="images/slider/slide3.jpg" alt=""></a></div>
		</div>

		<div class="separador"></div>
	</section>

	<section id="content">
		<div class="container sidebar-bg">
			<div class="row">
				<div class="col-lg-8 pr30">
					<h2 class="underline">DETALLES DEL PROYECTO<span></span></h2>
					<p>Se inició en el 2013 y tiene 5 años de duración. Busca mejorar la eficacia del gobierno paraguayo mediante el desarrollo de sus recursos humanos e institucionales, asi como instalar mecanismos de control interno, transparencia y rendición de cuentas con incorporación de herremientas de gestión y tecnología.</p>
					<div class="row mt20">
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg1.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Gobierno Abierto</h3>
								<p>El Gobierno Electrónico es parte del proceso de Gobierno Abierto donde servicios e información son expuestos a la ciudadanía de manera eficaz a través de herramientas digitales que potencian la participación ciudadana.</p>
							</article>
						</div>
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg2.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Poder Judicial</h3>
								<p><strong>Carrera del Funcionario Judicial: </strong>Un sistema basado en los méritos del funcionario judicial que apunta a impulsar el desarrollo de recursos humanos profesionales dentro del área. Generando independencia de los individuos y eliminando el prebendarismo. Desembocando en una mejor administración de la justicia.</p>
								<p><strong>Reforma Administrativa del Poder Judicial: </strong>La reforma administrativa apunta a descongestionar el trabajo en la Corte Suprema de Justicia que en este momento debe realizar tareas de administración financiera además de las responsabilidades propias de los cargos relacionados con la práctica del derecho.</p>
							</article>
						</div>
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg3.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Servicio Civil</h3>
								<p>Un sistema basado en los méritos del funcionario público que apunta a impulsar el desarrollo de recursos humanos profesionales dentro del área. Generando independencia de los individuos y eliminando el prebendarismo. Desembocando en mejores servicios para la ciudadanía.</p>
							</article>
						</div>
					</div>
					<div class="row mt20">
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg4.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Control Interno</h3>
								<p> Los procedimientos relacionados con el control interno promueven la eficiencia y la eficacia en el uso de los recursos del Estado además de contribuir con la transparencia del gasto público.</p>
							</article>
						</div>
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg5.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>SENAC</h3>
								<p>Organismo encargado de la prevención de hechos de corrupción y apoyo a otras instituciones, aportando a la concienciación sobre los efectos de estos hechos sobre la sociedad.</p>
							</article>
						</div>
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg6.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Contrataciones Públicas</h3>
								<p>Fortalecimiento de las tecnologías y procedimientos para la adquisición transparente, efectiva y apropiada de bienes y servicios.</p>
							</article>
						</div>
					</div>
					<div class="row mt20">
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg7.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Declaraciones Juradas de Funcionarios Públicos</h3>
								<p>Recurso ineludible de control interno donde los funcionarios tienen la posibilidad de rendir cuentas a la ciudadanía, demostrando profesionalismo, honestidad y compromiso con el ejercicio de la función pública.</p>
							</article>
						</div>
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg8.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Ley de Acceso a la Información</h3>
								<p>Herramienta jurídica que amplía la capacidad de participación y veeduría ciudadana al exponer datos relacionados con la administración de los Recursos del Estado, Finanzas, Proyectos y Recursos Humanos.</p>
							</article>
						</div>
						<div class="col-lg-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg9.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Participación Ciudadana</h3>
								<p></p>
							</article>
						</div>
					</div>
					<div class="row mt20">
						<div class="col-lg-4 col-lg-offset-4">
							<article class="circle-pdg text-center">
								<a href="javascript:void(0);" class="img"><img src="images/pdg10.jpg" alt="" class="img-circle"><span class="mas">+</span></a>
								<h3>Aduanas - Comercio internacional </h3>
								<p>Se apoya la creación de un Sistema de Control de las operaciones basado en la selección de cargas en el que se establecen criterios objetivos para el canal rojo (de mayor riesgo) y también para el canal verde (menor riesgo). Este sistema agilizará el flujo del comercio exterior y mejorará la calidad de los controles.</p>
							</article>
						</div>
					</div>
					<br>
					<h2 class="underline">ÁREAS DE ACCIÓN<span></span></h2>
					<p>El primer objetivo del Programa de Democracia y Gobernabilidad (PDG) se centra en fortalecer la capacidad institucional de Entidades Públicas seleccionadas mediante las siguientes acciones:</p>
					<div class="item-pdg">
						<p class="nro">1</p>
						<h3>Mejorar los Sistemas de Gestión de Recursos Humanos</h3>
						<p>El PDG trabajará con el Poder Ejecutivo y Poder Judicial para implementar procesos transparentes y eficientes de selección, evaluación, promoción, capacitación y desvinculación de los funcionarios.</p>
						<div class="texto">
							<p>En ese sentido, se apoyará a la Secretaria de la Función Pública (SFP) en su rol de entidad rectora en materia de Recursos Humanos, para la cual se instalará la Carrera del Servicio Civil en Paraguay como parte del proceso de profesionalización del funcionariado público. Dicho proceso estará basado en criterios de méritos, igualdad e imparcialidad y para cumplir con estos criterios se implementará totalmente el Sistema Integrado Centralizado de la Carrera Administrativa (SICCA), que brindará soporte informático a la gestión de Recursos Humanos del Sector Público en organismos y entidades dependientes del Poder Ejecutivo.</p>
							<p>De igual manera, el PDG apoyará al Poder Judicial en la implementación de políticas y procedimientos que promuevan una Carrera Judicial basada en méritos y en el fortalecimiento de Centro Internacional de Entrenamiento Judicial (CIEJ). Mediante el establecimiento de criterios objetivos y competitivos de gestión de recursos humanos, así como el impulso de acciones que promuevan la formación continua de los magistrados y funcionarios del Poder Judicial, se espera que este Poder del Estado aumente su capacidad de gestión y por lo tanto el rol que cumple en el país.</p>
						</div>
						<a href="javascript:void(0);" class="leermas">Mostrar más <span>+</span></a>
					</div>
					<div class="item-pdg">
						<p class="nro">2</p>
						<h3>Procesos Presupuestarios y de Contrataciones Públicas Fortalecidos</h3>
						<p>Paraguay ha mejorado su desempeño en cuanto a la gestión y administración financiera pública en los últimos años, sin embargo los sistemas presupuestarios continúan demostrando debilidades en lo que respecta a la transparencia, la participación ciudadana y rendición de cuentas.</p>
						<div class="texto">
							<p>Por otro lado, los sistemas y procedimientos de administración de contrataciones públicas también deben ser optimizados, con el fin de que los recursos públicos sean utilizados de manera eficiente y eficaz.</p>
							<p>Tomando en cuenta estos objetivos, el PDG pretende fortalecer los procesos de presupuesto y el sistema de administración de adquisiciones con especial atención en las instituciones públicas rectores en estos ejes. Se buscarán los mecanismos que promuevan el fortalecimiento institucional y mayor efectividad y credibilidad de las instituciones determinadas, mediante el cumplimiento de los componentes de principios de transparencia, eficiencia y control.</p>
							<p>Particularmente, se trabajará con el Ministerio de Hacienda para implementar mejores mecanismos de elaboración y ejecución del Presupuesto General de la Nación y al mismo tiempo se promoverá un mayor involucramiento de Organizaciones de la Sociedad Civil en los procesos de formulación, ejecución y evaluación del gasto público.</p>
							<p>De igual forma, en lo que respecta a la Dirección Nacional de Contrataciones Públicas (DNCP), desde la promulgación de la Ley 2051/03 “De Contrataciones Públicas” la institución ha registrado un avance y fortalecimiento en la en el sistema de adquisiciones de bienes y servicios del Estado. Entre otros avances, se logró la unificación de marcos reguladores de los procesos de adquisición, la centralización de control de planeamiento, programación, presupuesto, contratación, ejecución contractual, erogación de las entidades públicas. Todos estos avances se han consolidado a través de la implementación del Sistema Informático de Contrataciones Públicas (SICP), que permite la participación y control ciudadano, pero todavía constan debilidades y disconformidades con el marco normativo.</p>
						</div>
						<a href="javascript:void(0);" class="leermas">Mostrar más <span>+</span></a>
					</div>
					<div class="item-pdg">
						<p class="nro">3</p>
						<h3>Capacidad de proveer servicios estratégicos y prioritarios mejorada</h3>
						<p>Las mejoras que se obtengan en la gestión de recursos humanos y de recursos financieros, pretenden especialmente fortalecer la capacidad de las instituciones públicas prestadoras de servicios básicos, así como la capacidad del Poder Judicial en la justicia.</p>
						<div class="texto">
							<p>Finalmente, el proceso de fortalecimiento tiene como meta que el Estado responda de manera eficiente y oportuna a las demandas de la sociedad en áreas clave como Salud, Educación, Desarrollo Productivo, Infraestructura, Justicia., para lo cual se promoverán herramientas informáticas apropiadas para el logro de estos objetivos.</p>
						</div>
						<a href="javascript:void(0);" class="leermas">Mostrar más <span>+</span></a>
					</div>
					<div class="item-pdg">
						<p class="nro">4</p>
						<h3>Rendición de cuentas y esfuerzos anticorrupción fortalecidos en el Sector Público</h3>
						<p>Este componente apunta a mejorar la calidad de las instituciones públicas por medio de un sistema de rendición de cuentas interno y externo. Sus objetivos pueden agruparse bajo dos áreas principales: Mejora del control interno y la rendición de cuentas en las instituciones públicas, y Fortalecimiento del control externo que realiza la ciudadanía sobre la gestión pública.</p>
						<div class="texto">
							<p>Bajo el punto de mejora del control interno y rendición de cuentas, promoveremos la mejora en la aplicación del control interno en instituciones claves del Estado, mejorando la implementación del Modelo Estándar de Control Interno del Paraguay (MECIP). Se prestará asistencia a instituciones claves en el gobierno, por el impacto de los servicios que proveen a la ciudadanía, como es el caso del Ministerio de Educación, el Ministerio de Salud Pública y Bienestar Social, el Poder Judicial, entre otros.</p>
							<p>Las entidades rectoras en materia de control interno y evaluación del MECIP son la Contraloría General de la República (CGR) y la Auditoria General del Poder Ejecutivo (AGPE). En este aspecto, estaremos apostando al fortalecimiento de dos instituciones claves para el Control Interno.</p>
							<p>Apoyaremos a instituciones claves en lo referente al impulso de políticas de transparencia e integridad como la Secretaría Nacional de Anticorrupción (SENAC), que conjuntamente con las Unidades de Transparencia y Anticorrupción cumplirán un rol fundamental en la construcción de ambientes de mayor integridad en las instituciones públicas y aumentarán la confianza para la ciudadanía. Además, consideramos de suma importancia la construcción, de manera incluyente, de un Plan Nacional Anticorrupción que guíe los esfuerzos del gobierno en esta materia, por lo cual estaremos apoyando esta iniciativa.</p>
							<p>Trabajamos apoyando técnicamente a las instituciones públicas a generar una cultura de la transparencia, en donde se instale la tradición de dar cuentas, explicar y justificar los actos a la ciudadanía, que es el último depositario de la soberanía en una democracia. Por consiguiente, el involucramiento de la sociedad civil en estos procesos de transparencia y rendición de cuenta será fundamental por lo que buscaremos su amplia y activa participación.</p>
							<p>Como una estrategias clave para promover la transparencia, apoyaremos la construcción y el cumplimiento del Plan de Acción de Gobierno Abierto del Paraguay, que consideramos una herramienta indispensable ya que exige al Estado a cumplir estándares elevados de transparencia, rendición de cuentas y mejora en su capacidad de respuesta hacia la ciudadanía.</p>
							<p>En cuanto al control externo apoyaremos fuertemente en la generación de capacidades en la ciudadanía promoviendo su participación en el monitoreo y el control del uso de recursos públicos. El trabajo en conjunto con organizaciones de la sociedad civil, gremios del sector privado y la academia es uno de los principales caminos a seguir.</p>
							<p>Consideramos que es necesario un espacio de investigación y formación en control ciudadano que sea capaz de generar datos y, con estos, campañas de concienciación en transparencia pública. Esta tarea será desarrollada tanto en el Poder Ejecutivo como en el Poder Judicial.</p>
							<p>Desde el Programa de Democracia y Gobernabilidad (PDG) se apoyan las acciones de reformas legales o reglamentarias que favorezcan la consolidación democrática y con ello la gobernanza efectiva y eficiente.</p>
							<p>No sólo se proponen iniciativas de reformas de marcos legales o reglamentarios al interior de las Instituciones Públicas seleccionadas por el Programa, sino también desde acciones de incidencia ciudadana derivadas –preferentemente- de grupos impulsores y activos que tienen entre sus prioridades este tipo de acciones, de modo a generar reformas que logren interconectar Estado con ciudadanía.</p>
							<p>Para hacer posible el cumplimiento de estas acciones el PDG, ha identificado aliados de la sociedad civil estratégicos y en este sentido promueve el proyecto de ley de acceso ciudadano a la información pública a través de la Organización no Gubernamental IDEA, miembro activo del Grupo de Acceso a la Información (GIAI).</p>
							<p>También promueve la participación de la Federación de Padres del Paraguay (FEDAPY) por un lado como sujetos activos en la mirada de la reforma orgánica del Ministerio de Educación y Cultura (MEC) y por el otro, en acciones de impulso y fortalecimiento institucional, ya que es un instancia aglutina a las Asociaciones de Cooperación Escolar (ACES), miembros de la comunidad escolar, y desde la promulgación de la Ley N° 4853/13 como obligados para algunas iniciativas del Centro Educativo para el cual están reconocidos.</p>
							<p>Por otra parte, se han identificado grupos de contraloría ciudadana aliadas al control público, para los cuales resulta importante la mirada respecto a reglamentaciones que hacen a la tarea del control y los efectos del mismo.</p>
						</div>
						<a href="javascript:void(0);" class="leermas">Mostrar más <span>+</span></a>
					</div>
				</div>
				<?php include('inc/sidebar-proyectos.php'); ?>
			</div>
		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
