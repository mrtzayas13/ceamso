<?php include('inc/config.php'); ?>



<head>



<title>Sobre la CEAMSO | <?=SITENAME;?></title>

<meta name="description" content="Somos una organización de la sociedad civil con más de 20 años de trayectoria que trabaja en diversas áreas fortaleciendo instituciones públicas, privadas y organizaciones ciudadanas que contribuyen al desarrollo del país. Nuestro trabajo apunta a mejorar la gestión de estas organizaciones y potenciar su impacto económico, social y ambiental." />

<meta name="keywords" content="<?=GRALKEYS;?>, nosotros, quienes somos, ong, que es" />



<?php include('inc/head.php'); ?>



<link rel="stylesheet" type="text/css" href="css/slick.css"/>

<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>

<script type="text/javascript" src="js/slick.min.js"></script>

<script type="text/javascript">

	$(function(){

		$('.slide-quienes').slick({

		  dots: true,

		  arrows: false,

		  infinite: true,

		  speed: 300,

		  slidesToShow: 1,

		  adaptiveHeight: true

		});

	});

</script>



</head>



<body class="sec-ceamso">



	<?php include('inc/header.php'); ?>



	<section id="titulo">

		<div class="container">

			<p class="breadcrumb">¿Quiénes Somos?</p>

			<h1 class="underline">Sobre <strong>CEAMSO</strong><span></span></h1>

			<p class="copete">Somos una organización de la sociedad civil con más de 25 años de trayectoria que trabaja en diversas áreas fortaleciendo instituciones públicas, privadas y organizaciones ciudadanas que contribuyen al desarrollo del país. Nuestro trabajo apunta a mejorar la gestión de estas organizaciones y potenciar su impacto económico, social y ambiental.</p>

		</div>

	</section>



	<section id="content">

		<div class="container">

			

			<p class="s750 text-center">El Centro de Estudios Ambientales y Sociales-CEAMSO, es una Organización No Gubernamental, reconocida con personería jurídica N° 22.367 del mes de agosto del año 1998.</p>



		</div>



		<div id="slide-quienes-container">

			<div class="container">

				<div class="slide-quienes">

					<div>

						<h2 class="underline">NUESTRA <strong>VISIÓN</strong><span></span></h2>

						<p class="s750 text-center">Paraguay Integro y Transparente, con Desarrollo <br>Sostenible y Equidad Social.</p>

					</div>

					<div>

						<h2 class="underline">NUESTRA <strong>MISIÓN</strong><span></span></h2>

						<p class="s750 text-center">Fortalecer a instituciones públicas, entes privados y organizaciones ciudadanas que contribuyen al desarrollo del país potenciando su impacto económico, social y ambiental</p>

					</div>

					<div class="valores">

						<h2 class="underline">NUESTROS <strong>VALORES</strong><span></span></h2>

						<div class="row">

							<div class="col-lg-2 col-md-2"><p><strong>INTEGRIDAD. </strong>A través de nuestras acciones y comportamiento nos esforzamos en ganar el respeto y la confianza de nuestros donantes, clientes y socios.</p></div>

							<div class="col-lg-3 col-md-3"><p><strong>COMPROMISO CON LA EXCELENCIA. </strong>Demostramos excelencia en nuestro trabajo, para superar las expectativas de nuestros clientes, y para tener el mayor impacto posible en el avance de nuestra misión con un enfoque de mejora continua.</p></div>

							<div class="col-lg-3 col-md-3"><p><strong>Creatividad e innovación. </strong>Ser líderes capaces de responder constantemente a las necesidades de los clientes, reflexionar sobre nuestro progreso, aprender de las mejores prácticas, generar innovaciones acordes a nuestra realidad, y adaptar cuidadosamente nuestros programas en el tiempo. </p></div>

							<div class="col-lg-2 col-md-2"><p><strong>Respeto por los socios y la organización. </strong>Tratarnos con respeto, celebrando el conocimiento y logro de nuestros compañeros.</p></div>

							<div class="col-lg-2 col-md-2"><p><strong>Vocación de Servicio. </strong>El staff y Consejo Directivo está insipado en nuestra misión, en la oportunidad de servir a nuestros clientes y en el privilegio de trabajar por la promoción de nuestros valores e ideales.</p></div>

						</div>

					</div>

				</div>

			</div>

		</div>


		<!--
		<div id="nos-proponemos">
		<div class="container">

			<h2 class="underline">Políticas <strong>CEAMSO</strong><span></span></h2>

			<div class="row">

				<div class="col-lg-4 col-md-4 col-sm-12">

					<ul class="iconos-colores">

						<li class="color1">Capacitación y Gestión permanente.</li>

						<li class="color2">Mejoramiento contínuo de los procesos de trabajo.</li>

					</ul>

				</div>

				<div class="col-lg-4 col-md-4 col-sm-12">

					<ul class="iconos-colores">

						<li class="color3">Rendición de cuenta y transparencia.</li>

						<li class="color4">Inclusión social y equidad.</li>

					</ul>

				</div>

				<div class="col-lg-4 col-md-4 col-sm-12">

					<ul class="iconos-colores">

						<li class="color5">Eficiencia y eficacia en el desempeño profesional e institucional.</li>

						<li class="color6">Innovación permanente.</li>

					</ul>

				</div>

			</div>

		</div>
		</div>

		-->

		<!-- <div id="nos-proponemos">

			<div class="container">

				<h2 class="underline">PARA <strong>CUMPLIR CON NUESTROS OBJETIVOS</strong> NOS PROPONEMOS<span></span></h2>

				<br>

				<div class="row mb20">

					<div class="col-lg-4 col-md-4 col-sm-4 item">

						<figure><img src="images/proponemos1.png" alt="Elaborar y ejecutar"></figure>

						<h3>Elaborar y ejecutar</h3>

						<p>proyectos de infraestructura urbana, para el mejoramiento de las condiciones de vida en las ciudades.</p>

					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 item">

						<figure><img src="images/proponemos2.png" alt="Desarrollar programas"></figure>

						<h3>Desarrollar programas</h3>

						<p>de apoyo, asistencia técnica en las áreas medioambiental y social, para el desarrollo de políticas sostenibles.</p>

					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 item">

						<figure><img src="images/proponemos3.png" alt="Asesorar y gestionar"></figure>

						<h3>Asesorar y gestionar</h3>

						<p>empresas del sector privado, para la ejecución de inversiones y actividades empresariales compatibles con el medio ambiente.</p>

					</div>

				</div>

				<div class="row">

					<div class="col-lg-4 col-md-4 col-sm-4 item">

						<figure><img src="images/proponemos4.png" alt="Promover los principios"></figure>

						<h3>Promover los principios</h3>

						<p>de la declaración de los derechos humanos y la igualdad de género, en el desarrollo de los programas ambientales y sociales.</p>

					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 item">

						<figure><img src="images/proponemos5.png" alt="Impulsar políticas y planes"></figure>

						<h3>Impulsar políticas y planes</h3>

						<p>de desarrollo sostenible, a través de programas de capacitación, publicación y eventos.</p>

					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 item">

						<figure><img src="images/proponemos6.png" alt="Fortalecimiento institucional"></figure>

						<h3>Fortalecimiento institucional</h3>

						<p>de los gobiernos locales y promoción de la participación comunitaria, como herramienta para el logro del desarrollo sostenible.</p>

					</div>

				</div>

			</div>

		</div> -->



		<div class="container text-center">

			<p>Uno de los objetivos que se propone la organización a mediano plazo, es la concienciación ciudadana sobre la necesidad de una mayor participación de las organizaciones de base en la vida pública,  a fin de mejorar las condiciones de vida en nuestro país.</p>

			<br>

			<a href="proyectos" class="btn linea-gris">VER PROYECTOS</a>

		</div>



	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

