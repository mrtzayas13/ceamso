<?php include('inc/config.php'); ?>



<head>



<title>Sobre el CAMPUS | <?=SITENAME;?></title>

<meta name="description" content="Somos una organización de la sociedad civil con más de 20 años de trayectoria que trabaja en diversas áreas fortaleciendo instituciones públicas, privadas y organizaciones ciudadanas que contribuyen al desarrollo del país. Nuestro trabajo apunta a mejorar la gestión de estas organizaciones y potenciar su impacto económico, social y ambiental." />

<meta name="keywords" content="<?=GRALKEYS;?>, nosotros, quienes somos, ong, que es" />



<?php include('inc/head.php'); ?>




</head>

<body class="sec-campus">



	<?php include('inc/header.php'); ?>


    <section id="titulo">

        <div class="container">

            <h1 class="underline"><strong>CAMPUS</strong><span></span></h1>

        </div>

    </section>
    
    <section id="content">

        <div class="container text-center">

            <p>ENTERATE DE LOS CURSOS VIGENTES ESCRIBINOS: <strong>CEAMSO@CEAMSO.ORG.PY</strong></p>

            <br>

            <a href="http://campus.ceamso.org.py/" class="btn linea-gris" target="_BLANK">IR AL CAMPUS</a>

        </div>

    </section>

    <?php include('inc/footer.php'); ?>

</body>