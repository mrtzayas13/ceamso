<?php include('inc/config.php'); ?>



<head>



<title>Nuestras Áreas | <?=SITENAME;?></title>

<meta name="description" content="Gobernabilidad y Fortalecimiento Institucional, Inclusión social y género, Medioambiente y saneamiento, Tecnologías innovadoras y Desarrollo socioeconómico." />

<meta name="keywords" content="<?=GRALKEYS;?>, areas, accion" />



<?php include('inc/head.php'); ?>



</head>



<body class="sec-ceamso">



	<?php include('inc/header.php'); ?>



	<section id="titulo">

		<div class="container">

			<p class="breadcrumb">¿Quiénes Somos?</p>

			<h1 class="underline">Nuestras <strong>Áreas</strong><span></span></h1>

			<p class="copete">Estas son las áreas en las que trabajamos</p>

		</div>

	</section>



	<section id="areas">

		<div class="container">

			<div class="clear">

				<article class="area-accion">

					<a href="proyectos?area=1" class="icon"><img src="images/ico-gobernabilidad.png" alt="Gobernabilidad y Fortalecimiento Institucional"></a>

					<h3>Gobernabilidad y Fortalecimiento Institucional</h3>

					<p>Impulsamos iniciativas para fortalecer las instituciones para que estas puedan proveer mejores servicios.</p>

					<a href="proyectos?area=1" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=2" class="icon"><img src="images/ico-inclusion.png" alt="Inclusión social y género"></a>

					<h3>Inclusión social y género</h3>

					<p>Trabajamos la inclusión social y la equidad de género en todas las iniciativas, para construir un país más equitativo.</p>

					<a href="proyectos?area=2" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=3" class="icon"><img src="images/ico-medioambiente.png" alt="Medioambiente y saneamiento"></a>

					<h3>Medioambiente y saneamiento</h3>

					<p>Implementamos programas que protegen el medioambiente, unas de las áreas vitales de nuestra organización.</p>

					<a href="proyectos?area=3" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=4" class="icon"><img src="images/ico-tecnologias.png" alt="Innovación y desarrollo social"></a>

					<h3>Innovación y desarrollo social</h3>

					<p>Las intervenciones se complementan con tecnologías innovadoras que mejoran la eficiencia.</p>

					<a href="proyectos?area=4" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<!-- <article class="area-accion">

					<a href="proyectos?area=5" class="icon"><img src="images/ico-desarrollo.png" alt="Desarrollo socioeconómico"></a>

					<h3>Desarrollo socioeconómico</h3>

					<p>Trabajamos con diversas iniciativas para estimular el desarrollo socioeconómico sostenible.</p>

					<a href="proyectos?area=5" class="btn-rectangular">VER PROYECTOS</a>

				</article> -->

			</div>

		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

