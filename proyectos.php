<?php include('inc/config.php'); ?>



<?php



$estado = TextHelper::cleanString($_GET['estado']);

$estado = ($estado == 'ejecutado') ? "ejecutado" : "ejecucion";

$area = TextHelper::cleanNumber($_GET['area']);
$area = (empty($area) || $area == 0) ? 0 : $area;

?>

<head>

<title>Programas y Proyectos | <?=SITENAME;?></title>

<meta name="description" content="Nuestro proyectos en ejecución y ejecutados." />

<meta name="keywords" content="<?=GRALKEYS;?>, proyectos, ejecutados, ejecucion" />



<?php include('inc/head.php'); ?>

<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>

<script type="text/javascript"> 



	$(document).ready(function(){	

		cargaPaginacion(1,<?php echo $area?>, '<?php echo $estado?>');	
		$(".btns-areas li a").click(function(){
			$(".btns-areas li a").removeClass('active');
			$(this).addClass('active');
		});

		// initialize
		//var $container = $('.proyectos');
	    var $container = $('.proyectos').masonry({
	      itemSelector: '.proyecto',
	      columnWidth: 279,
	      gutter: 8
	    });

	    $container.imagesLoaded().progress( function() {
			$container.masonry( 'reloadItems' );
			$container.masonry( 'layout' );
		});

	    /*var msnry = $container.data('masonry');
	    //$container.prepend( '<div class="item">foo</div>' );
		$container.masonry( 'reloadItems' );
		$container.masonry( 'layout' );*/

	});

	var pagina_actual = 1;

	function cargaPaginacion(p_pagina, area_id, estado ){



		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 

		$.ajax({
			type: 'GET',
			url: '<?=CONF_SITE_URL?>ajax/proyectos.php',
			data: { 
				p: p_pagina,
				area_id : area_id,
				estado : '<?php echo $estado?>'
			},

			success: function(p_html){
				pagina_actual = p_pagina;
				$('#contenido').html(p_html);
				//var $container = $('.proyectos');

			    // initialize

			    var $container = $('.proyectos').masonry({
			      itemSelector: '.proyecto',
			      columnWidth: 279,
			      gutter: 8
			    });

			    $container.imagesLoaded().progress( function() {
					$container.masonry( 'reloadItems' );
					$container.masonry( 'layout' );
				});

			    /*var msnry = $container.data('masonry');
			    //$container.prepend( '<div class="item">foo</div>' );
				$container.masonry( 'reloadItems' );
				$container.masonry( 'layout' );*/
			}
		});
	}
</script>



</head>



<body class="sec-proyectos">



	<?php include('inc/header.php'); ?>



	<section id="titulo">

		<div class="container">

			<p class="breadcrumb">¿Qué hacemos?</p>

			<h1 class="underline"><strong>PROGRAMAS</strong> Y <strong>PROYECTOS</strong><span></span></h1>

		</div>

	</section>



	<div id="tabs-proyectos">

		<div class="container">

			<ul class="tabs">

				<li><a href="proyectos/ejecucion" <?php if($estado == 'ejecucion'){echo 'class="active"';}?> ><strong>EN EJECUCIÓN</strong></a></li>

				<li><a href="proyectos/ejecutado" <?php if($estado == 'ejecutado'){echo 'class="active"';}?>><strong>ejecutados</strong></a></li>

			</ul>

		</div>

	</div>



	<section id="listado-proyectos">

		<div class="container">

			<ul class="btns-areas">

				<?php foreach($areas as $rs){?>

				<li><a <?php if($area == $rs['id']){echo 'class="active"';} ?> href="javascript:;" onclick="cargaPaginacion(1,<?php echo $rs['id']?>, '<?php echo $estado?>')" rel="<?php echo $rs['area'.$idioma]?>"><?php echo $rs['area'.$idioma]?></a></li>

				<?php }?>

			</ul>



			<div id="contenido">


			</div>



		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

