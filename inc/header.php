	<section id="top">
		<div class="container">
			
			<ul class="datos-contacto">
				<li><img src="images/ico-tel.png" alt="Telefono"><span class="smaller">+595 21</span> <strong>662 585</strong></li>
				<li><img src="images/ico-email.png" alt="E-mail"><span class="smaller">ceamso@ceamso.org.py</li>
			</ul>
			<ul class="idioma">
				<li><a href="<?php echo URL?>?idioma=_esp">ESPAÑOL</a></li>
				<!-- <li><a href="<?php echo URL?>?idioma=_eng">ENGLISH</a></li> -->
			</ul>
			<ul class="redes-sociales">
				<li><a href="https://www.facebook.com/ceamso.ong/?fref=ts" title="Seguinos en Facebook" target="_BLANK"><img src="images/fb-top.png" alt="Facebook"></a></li>
				<li><a href="https://twitter.com/Ceamsopy" title="Seguinos en Twitter" target="_BLANK"><img src="images/tw-top.png" alt="Twitter"></a></li>
				<li><a href="https://www.instagram.com/ceamsopy/?hl=es" title="Seguinos en Instagram" target="_BLANK"><img src="images/insta-top.png" alt="Instagram"></a></li>
				<li><a href=" https://py.linkedin.com/company/ceamso-ong" title="Seguinos en Linkedin" target="_BLANK"><img src="images/in-top.png" alt="Linkedin"></a></li>
				<li><a href="https://www.youtube.com/channel/UCphxfy1ogK4gjpffAlIYi6w" title="Seguinos en YouTube" target="_BLANK"><img src="images/yt-top.png" alt="YouTube"></a></li>
			</ul>

		</div>
	</section>

	<header id="header">
		<div class="container">
			
			<a href="./" class="logo"><img src="images/ceamso-logo.png" alt="CEAMSO | Centro de Estudio Ambientales y Sociales"></a>

			<!-- MENU -->
			<nav class="navbar navbar-default right">
				<div class="row">
				    <!-- Toggle for mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				    </div>
				</div>
				<div class="row">
					<div class="collapse navbar-collapse" id="menu">
						<ul class="nav navbar-nav">
							<li><a href="./" rel="inicio">Inicio<span></span></a></li>
							<li class="dropdown">
								<a href="#" rel="quienes" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">¿Quiénes somos?<span></span></a>
								<ul class="dropdown-menu">
									<li><a href="sobre-ceamso">SOBRE CEAMSO</a></li>
									<li role="separator" class="divider"></li>
									<!-- <li><a href="nuestras-areas">NUESTRAS ÁREAS</a></li> -->
									<!-- <li role="separator" class="divider"></li> -->
									<li><a href="staff">CONSEJO DIRECTIVO</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" rel="quehacemos" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">¿Qué hacemos?<span></span></a>
								<ul class="dropdown-menu">
									<li><a href="proyectos">PROGRAMAS Y PROYECTOS</a></li>

									<!-- <li role="separator" class="divider"></li>
									<li><a href="historias-de-exito">HISTORIAS DE ÉXITO</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="videos">VIDEOS</a></li> -->

									<li role="separator" class="divider"></li>
									<li><a href="blog">Blog<span></span></a></li>
								</ul>
							</li>
							<!-- <li><a href="noticias" rel="noticias">Noticias<span></span></a></li> -->
							<!-- <li><a href="http://campus.ceamso.org.py/" rel="campus">Campus<span></span></a></li> -->
							<li><a href="campus" rel="campus">Campus<span></span></a></li>
							<li><a href="llamados" rel="llamados">Llamados<span></span></a></li>
							<li><a href="publicaciones" rel="publicaciones">Publicaciones<span></span></a></li>
							<li><a href="contactenos" rel="contactenos">Contáctenos<span></span></a></li>
							<li class="dropdown">
								<a id="dropbuscador" class="dropdown-toggle" data-toggle="dropdown" href="#" rel="buscar"><!-- <img src="images/ico-buscar.png" alt="Buscar"> --></a>
								<div class="dropdown-menu buscador" role="menu" aria-labelledby="dropbuscador" onClick="event.stopPropagation();">
									<form action="resultados-busqueda" enctype="multipart/form-data" method="get">
			                        	<fieldset class="left">
				                        	<input type="text" class="searchbox" placeholder="Buscar..." id="q" name="q" autocomplete="off">
				                        </fieldset>
				                        <fieldset class="left ml10 select-input">
					                        <select name="seccion" id="seccion" class="select-style">
												<option value="llamados">En Llamados</option>
												<!--<option value="noticias">En Noticias</option>-->
												<option value="blog">En Blog</option>
												<option value="publicaciones">En Publicaciones</option>
												<option value="proyectos">En Programas y Proyectos</option>
												<!--<option value="videos">En Videos</option>-->
											</select>
										</fieldset>
										<fieldset class="left ml10">
				                        	<input type="submit" class="btn verde" value="BUSCAR">
				                        </fieldset>
			                        
			                        </form>
			                    </div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- TERMINA MENU -->

		</div>
	</header>

	<div id="en-construccion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
    		<div class="modal-content">
    			<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 20px; top: 10px; font-size: 30px;"><span aria-hidden="true">&times;</span></button>
    			<img src="images/contruccion.jpg" alt="Sitio Web en contruccion" class="img-responsive">
    		</div>
    	</div>
	</div>
