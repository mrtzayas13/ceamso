

	<footer>

		<div class="container">

			<div class="row">



				<div class="col-lg-3 col-md-3 col-sm-4 col-sm-12">

					<img src="images/logo-footer.png" alt="CEAMSO" class="mb20">

					<ul class="datos-contacto mb10">

						<li><img src="images/ico-dir-footer.png" alt="Direccion">Capitán Pedro Carpinelli 3704 y Cecilio Ávila, Asunción.</li>

						<li><img src="images/ico-tel-footer.png" alt="Telefono">(+595 21) 662 585</li>

						<li><img src="images/ico-tel-footer.png" alt="Telefono">(+595) 986 118 974</li>

						<li><img src="images/ico-mail-footer.png" alt="E-mail">ceamso@ceamso.org.py</li>
						<!-- <a href="" class="mt10" style="margin-left: -25px; display: block;"><img src="images/btn-licitaciones.png" alt="Inscribite y recibí invitaciones para licitaciones"></a> -->

					</ul>

					<ul class="redes-sociales mb20">

						<li><a href=" https://www.instagram.com/ceamsopy" title="Seguinos en Instagram" target="_BLANK"><img src="images/insta-top.png" alt="Instagram"></a></li>

						<li><a href="https://py.linkedin.com/company/ceamso-ong" title="Seguinos en Linkedin" target="_BLANK"><img src="images/in-top.png" alt="Linkedin"></a></li>

						<li><a href="https://www.facebook.com/ceamso.ong/?fref=ts" title="Seguinos en Facebook" target="_BLANK"><img src="images/fb-top.png" alt="Facebook"></a></li>

						<li><a href="https://twitter.com/Ceamsopy" title="Seguinos en Twitter" target="_BLANK"><img src="images/tw-top.png" alt="Twitter"></a></li>

						<li><a href="https://www.youtube.com/@ceamsoong4667/" title="Seguinos en YouTube" target="_BLANK"><img src="images/yt-top.png" alt="YouTube"></a></li>

					</ul>
					<!--
						<p style="font-size: 13px;"><strong style="font-size: 14px;">¿Eres nuevo por aquí?</strong><br>Dejanos tus datos</p>
						<a href="http://cv.ceamso.org.py/persona/registro" class="left mr10 btn-mas-info" target="_BLANK" style="font-size: 14px;"><img src="images/btn-cv.png" alt="Curriculum Personal"></a>
						<a href="http://cv.ceamso.org.py/persona/registro" class="left btn-mas-info" target="_BLANK" style="font-size: 14px;"><img src="images/btn-cv-empresas.png" alt="Curriculum de empresa"></a>
					-->
					<p style="font-size: 13px;"><strong style="font-size: 14px;">¿Eres nuevo por aquí?</strong><br>Dejanos tus datos</p>

					<p strong style="font-size: 13px;"><strong style="font-size: 14px;">Envíanos tu curriculum personal o el curriculum de tu empresa:</p>
					
					<a href="https://forms.gle/8HSAXz1gkz3xxTmN9" class="left mr10 btn-mas-info" target="_BLANK" style="font-size: 14px;"><img src="images/btn.png" alt="Curriculum Personal o Empresa"></a>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 hidden-xs">

					<h3 class="underline">ÚLTIMOS <strong>PROGRAMAS Y PROYECTOS</strong><span></span></h3>

					<ul class="articulos-footer">

						<?php foreach($proyectos as $i=> $rs) { 

							$id = $rs['id'];

							$qgaleria = new DBQuery("SELECT imagen FROM galeria_proyectos where proyecto_id = '{$id}' limit 1");

							$galeria = $db->executeQuery($qgaleria);

							$fecha=date_create($rs['fecha_creacion']);

						?>

						<li class="articulo">

							<a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">

								<figure><img src="<?php echo CONF_SITE_URL.'/upload/galeria_proyectos/'.$galeria[0]['imagen'] ?>" alt="Articulo"></figure>

								<strong class="title"><?php echo date_format($fecha, 'd/m/Y')?></strong>

								<span class="desc"><?php echo TextHelper::truncate($rs['titulo'.$idioma],80) ?></span>

							</a>

						</li>

						<?php } ?>

					</ul>

					<a href="proyectos" class="ver-todos">Ver todos</a>

				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 hidden-xs">

					<h3 class="underline">ÚLTIMOS <strong>Llamados</strong><span></span></h3>

					<ul class="articulos-footer">

						<?php foreach($licitaciones as $i=> $rs) { ?>

						<li class="articulo">

							<a href="<?php echo CONF_SITE_URL; ?>llamado/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">

								<strong class="title"><?php echo ($rs['codigo']) ?></strong>

								<span class="desc"><?php echo TextHelper::truncate($rs['titulo'.$idioma],120) ?></span>

							</a>

						</li>

						<?php } ?>	

					</ul>

					<a href="llamados" class="ver-todos">Ver todos</a>

				</div>

				<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">

					<h3 class="underline">ÚLTIMOS <strong>videos</strong><span></span></h3>

					<ul class="videos-footer clear">
						<?php foreach($video_list->items as $item) { ?>
							
						<li class="video">

						<!--
							<a href="<?php echo CONF_SITE_URL; ?>videos/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" title="<?php echo $rs['titulo'.$idioma]?>"><span class="play"></span><img width="100" src="http://img.youtube.com/vi/<? echo TextHelper::youtube_url($rs['youtube_url'])?>/0.jpg" alt="<?php echo $rs['titulo'.$idioma]?>"></a>
						-->

							<a href="https://www.youtube.com/watch?v=<?php echo $item->snippet->resourceId->videoId ?>" title="<?php echo $item->snippet->title ?>" target="_BLANK"><span class="play"></span><img width="100" src="<?php echo $item->snippet->thumbnails->medium->url  ?>" alt="<?php echo $item->snippet->title ?>"></a>

						</li>

						<?php } ?>

					</ul>

					<a href="https://www.youtube.com/@ceamsoong4667/" class="ver-todos" target="_BLANK">Ver todos</a>

				</div>



			</div>

		</div>

	</footer>



	<div id="bottom">

		<div class="container">

			<p>© 2016 CEAMSO. Portal construido con apoyo del Programa de Democracia y Gobernabilidad (USAID/CEAMSO)</p>

		</div>

	</div>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-83198858-1', 'auto');
	  ga('send', 'pageview');

	</script>