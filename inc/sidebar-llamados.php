				

				<?php 

					//LICITACIONES

					$qlicitaciones_s = new DBQuery("SELECT * FROM licitaciones n where n.activo = 'SI' order by id desc limit 10");

					$licitaciones_s = $db->executeQuery($qlicitaciones_s);

				?>



				<aside class="col-lg-4 col-md-4 col-sm-12 sidebar">

					<div class="sidebar-puzzle"></div>

					<h3 class="underline">Últimos <strong>llamados</strong><span></span></h3>

					<ul class="sidebar-articulos">

						<?php foreach($licitaciones_s as $i=> $rs) { ?>

						<li class="articulo clear">

							<a href="<?php echo CONF_SITE_URL; ?>llamado/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">

								<strong class="title"><?php echo ($rs['codigo']) ?></strong>

								<span class="desc"><?php echo TextHelper::truncate($rs['titulo'.$idioma],120) ?></span>

							</a>

						</li>

						<?php } ?>	

					</ul>

				</aside>