<?php

error_reporting(E_ERROR);

ini_set('display_errors', 1);

ini_set('post_max_size', '15M');

ini_set('upload_max_filesize', '15M'); 

define('CONF_SITE_TITLE', "ceamso");

define('CONF_ROOT_PATH', "/");

define('CONF_SITE_URL', "http://" . $_SERVER['HTTP_HOST'] . CONF_ROOT_PATH);

define('CONF_ADMIN_URL', CONF_SITE_URL . "admin/");

define('CONF_UPLOAD_PATH', CONF_ROOT_PATH . "upload/");



define('CONF_ABS_ROOT_PATH', realpath(dirname(__FILE__). '/../') . '/');

define('CONF_ABS_UPLOAD_PATH', CONF_ABS_ROOT_PATH . 'upload');



// BASE DE DATOS



include('db.inc.php');



define('CONF_REG_X_PAG', 15);



// UPLOAD DE ARCHIVOS



define('CONF_UPLOAD_ALLOWED_IMAGES', 'image\/x-png|image\/png|image\/pjpeg|image\/jpeg|image\/gif');

define('CONF_UPLOAD_ALLOWED_FILES',  'image\/x-png|image\/png|image\/pjpeg|image\/jpeg|image\/gif|application\/zip|application\/x-rar-compressed|application\/pdf|application\/msword|application\/vnd.ms-excel|application\/vnd.ms-powerpoint');

define('CONF_UPLOAD_MAX_SIZE', 10485760); // 10MB



// SEGURIDAD 



define('CONF_PASSWORD_LENGTH', '6');

define('CONF_PASSWORD_FORMAT', '');

define('CONF_PASSWORD_EXPIRATION', '0');

define('CONF_PASSWORD_CHECK_VULNERABILITY','N');

define('CONF_PASSWORD_REPEAT', '0');



// ENUMS

$conf_dias_semana = array('1' => 'Lunes', '2' => 'Martes', '3' => 'Miercoles', '4' => 'Jueves', '5' => 'Viernes', '6' => 'Sabado', '7' => 'Domingo');



function __autoload($classname) {

    if(file_exists(CONF_ABS_ROOT_PATH . "classes/" . strtolower($classname) . ".class.php")){

        include_once(CONF_ABS_ROOT_PATH . "classes/" . strtolower($classname) . ".class.php");

    }elseif(file_exists(CONF_ABS_ROOT_PATH . "classes/helpers/" . strtolower($classname) . ".class.php")){

        include_once( CONF_ABS_ROOT_PATH . "classes/helpers/" . strtolower($classname) . ".class.php");

    }

}



//COOKIE IDIOMAS

if(!empty($_GET["idioma"])){

   	   $idioma = ($_GET["idioma"] == '_esp' || $_GET["idioma"] == '_eng') ? $_GET["idioma"] : '_esp';

	   setcookie("atlas-idioma", $idioma, time() + (60 * 60 * 24 * 365),"/");

}else{

   if (isset($_COOKIE["atlas-idioma"])){

      $idioma = $_COOKIE["atlas-idioma"];

   }else

   {

   	 $idioma = '_esp';

   }

}



?>