<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="googlebot" content="Index, Follow" />
<meta name="robots" content="all" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?=URL;?>/favicon.ico" />
<link rel="image_src" href="<?=URL;?>/images/favicon.png" />

<base href="<?=URL;?>/" />

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/normalize3.0.1.css">
<link rel="stylesheet" href="css/ceamso.css">
<link rel="stylesheet" href="css/prog.css">

<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="css/ie.css" />
<![endif]-->

<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/modernizr-2.7.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/respond.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/jquery.blockUI.js"></script>

<script type="text/javascript">
	$(document).ajaxStop($.unblockUI); 

	$(document).ready(function(){
		$("#header").sticky({topSpacing:0});

		$('.select').selectpicker({
			style: 'btn-default'
		});
	});
</script>