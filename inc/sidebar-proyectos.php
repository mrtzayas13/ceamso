				<?php if(count($noticias_s) > 0){?>



				<aside class="col-lg-4 col-md-4 col-sm-12 sidebar">

					<div class="sidebar-puzzle"></div>

					<h3 class="underline">NOTICIAS <strong>RELACIONADAS</strong><span></span></h3>

					<ul class="sidebar-articulos">

						<?php foreach($noticias_s as $i=> $rs) {

							$id_s = $rs['id'];

							$qgaleria_s = new DBQuery("SELECT imagen FROM galeria_noticias where noticia_id = '{$id_s}' limit 1");

							$galeria_s = $db->executeQuery($qgaleria_s);

							$fecha=date_create($blog['fecha_creacion']);

							?>

						<li class="articulo clear">

							<a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">

								<figure><img src="<?php echo CONF_SITE_URL.'/upload/galeria_noticias/'.$galeria_s[0]['imagen'] ?>" alt="Articulo"></figure>

								<strong class="title"><?php echo date_format($fecha, 'd/m/Y')?></strong>

								<span class="desc"><?php echo TextHelper::truncate($rs['titulo'.$idioma],120) ?></span>

							</a>

						</li>

						<?php } ?>	

					</ul>

				</aside>
				<?php } ?>	