	<footer>

		<div class="container">

			<div class="row">



				<div class="col-lg-3">

					<img src="images/logo-footer.png" alt="CEAMSO" class="mb20">

					<ul class="datos-contacto mb20">

						<li><img src="images/ico-dir-footer.png" alt="Direccion">Cecilio Avila 3838 c/ Chaco Boreal</li>

						<li><img src="images/ico-tel-footer.png" alt="Telefono">(+595 21) 604 332 - (+595 21) 662 585</li>

						<li><img src="images/ico-tel-footer.png" alt="Telefono">(+595 21) 607 598</li>

						<li><img src="images/ico-mail-footer.png" alt="E-mail">ceamso@ceamso.org.py</li>
						<a href="" class="mt20" style="margin-left: -20px;"><img src="images/btn-licitaciones.png" alt="Inscribite y recibí invitaciones para licitaciones"></a>

					</ul>

					<ul class="redes-sociales">

						<li><a href="#" title="Seguinos en Facebook"><img src="images/fb-top.png" alt="Facebook"></a></li>

						<li><a href="#" title="Seguinos en Twitter"><img src="images/tw-top.png" alt="Twitter"></a></li>

						<li><a href="#" title="Seguinos en YouTube"><img src="images/yt-top.png" alt="YouTube"></a></li>

					</ul>

				</div>

				<div class="col-lg-3">

					<h3 class="underline">ÚLTIMOS <strong>ARTÍCULOS</strong><span></span></h3>

					<ul class="articulos-footer">

						<?php foreach($blog as $i=> $rs) { 

							$id = $rs['id'];

							$qgaleria = new DBQuery("SELECT imagen FROM galeria_blog where blog_id = '{$id}' limit 1");

							$galeria = $db->executeQuery($qgaleria);

							$fecha=date_create($blog['fecha_creacion']);

						?>

						<li class="articulo">

							<a href="<?php echo CONF_SITE_URL; ?>articulo/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">

								<figure><img src="<?php echo CONF_SITE_URL.'/upload/galeria_blog/thumb__'.$galeria[0]['imagen'] ?>" alt="Articulo"></figure>

								<strong class="title"><?php echo date_format($fecha, 'd/m/Y')?></strong>

								<span class="desc"><?php echo TextHelper::truncate($rs['copete'.$idioma],80) ?></span>

							</a>

						</li>

						<?php } ?>

					</ul>

					<a href="blog.php" class="ver-todos">Ver todos</a>

				</div>

				<div class="col-lg-3">

					<h3 class="underline">ÚLTIMOS <strong>Llamados</strong><span></span></h3>

					<ul class="articulos-footer">

						<?php foreach($licitaciones as $i=> $rs) { ?>

						<li class="articulo">

							<a href="<?php echo CONF_SITE_URL; ?>llamado/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">

								<strong class="title"><?php echo ($rs['codigo']) ?></strong>

								<span class="desc"><?php echo TextHelper::truncate($rs['titulo'.$idioma],120) ?></span>

							</a>

						</li>

						<?php } ?>	

					</ul>

					<a href="llamados.php" class="ver-todos">Ver todos</a>

				</div>

				<div class="col-lg-3">

					<h3 class="underline">ÚLTIMOS <strong>videos</strong><span></span></h3>

					<ul class="videos-footer clear">

						<?php foreach($videos as $i=> $rs) { ?>

						<li class="video">

							<a href="<?php echo CONF_SITE_URL; ?>videos/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" title="<?php echo $rs['titulo'.$idioma]?>"><span class="play"></span><img width="100" src="http://img.youtube.com/vi/<? echo TextHelper::youtube_url($rs['youtube_url'])?>/0.jpg" alt="<?php echo $rs['titulo'.$idioma]?>"></a>

						</li>

						<?php } ?>

					</ul>

					<a href="videos.php" class="ver-todos">Ver todos</a>

				</div>



			</div>

		</div>

	</footer>



	<div id="bottom">

		<div class="container">

			<p>© 2016 CEAMSO. Portal construido con apoyo del Programa de Democracia y Gobernabilidad (USAID/CEAMSO)</p>

		</div>

	</div>