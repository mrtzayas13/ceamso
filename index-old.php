<?php include('inc/config.php'); ?>



<?php 

//ULTIMAS NOTICIAS

$qnoticias = new DBQuery("SELECT n.*, a.area_esp, a.area_eng FROM noticias n left outer join areas a on a.id = n.area_id where n.activo = 'SI' order by n.fecha_creacion desc limit 3");

$noticias = $db->executeQuery($qnoticias);



//ULTIMOS PROYECTOS

$qproyectos = new DBQuery("SELECT n.*, a.area_esp, a.area_eng FROM proyectos n left outer join areas a on a.id = n.area_id where n.activo = 'SI' order by n.id desc limit 3");

$proyectos = $db->executeQuery($qproyectos);



//HISTORIAS DE EXITO

$qexitos = new DBQuery("SELECT * FROM historias_exitos where activo = 'SI' order by id desc limit 6");

$exitos = $db->executeQuery($qexitos);



//ALIANZAS

$qalianzas = new DBQuery("SELECT * FROM alianzas_convenios where activo = 'SI'");

$alianzas = $db->executeQuery($qalianzas);



?>



<head>



<title><?=SITENAME;?> | Centro de Estudio Ambientales y Sociales</title>

<meta name="description" content="CEAMSO es una organización interdisciplinaria, de carácter civil, técnico y científico sin fines de lucro que contribuye al desarrollo de las comunidades a través de iniciativas sociales y ambientales que promuevan el respeto a los derechos humanos, buscando construir una sociedad más informada, organizada y participativa." />

<meta name="keywords" content="<?=GRALKEYS;?>" />

<?php include('inc/head.php'); ?>

<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script src="js/parallax.min.js"></script>
<script type="text/javascript">

	$(document).ready(function(){

		$('.slider').slick({
			dots: true,
			appendDots: $('.slider-text'),
			arrows: false,
			infinite: true,
			speed: 800,
			slidesToShow: 1,
			centerMode: true,
			autoplay: true,
			variableWidth: true,
			responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        variableWidth: true,
			        centerMode: false,
			        adaptiveHeight: true
			      }
			    }
			]
		});

		$('[data-toggle="popover"]').popover();

		$('.historias-slide').slick({
		  dots: true,
		  arrows: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 2,
		  responsive: [
		    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});



		$('.logos-slide').slick({
		  dots: false,
		  arrows: true,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
	});



	$(window).load(function() {
	    var $container = $('.proyectos');

	    // initialize
	    $container.masonry({
	      itemSelector: '.proyecto',
	      columnWidth: 279,
	      gutter: 8
	    });

	    var msnry = $container.data('masonry');

	});

</script>



</head>



<body class="sec-inicio">



	<?php include('inc/header.php'); ?>



	<section id="banner">

		<div class="container">

			<div class="slider-text">

				<figure><img src="images/ico-capacidad.png" alt=""></figure>

				<h3><strong class="center-block">CREAMOS CAPACIDAD</strong> en Instituciones y personas para implementar proyectos innovadores que propicien el desarrollo sostenible</h3>

				<a href="sobre-ceamso.php" class="btn linea-blanca">Conocenos</a>

			</div>

		</div>

			

		<div class="slider">

			<div><img src="images/slider/slide1.jpg" alt=""></div>

			<div><img src="images/slider/slide2.jpg" alt=""></div>

			<div><img src="images/slider/slide3.jpg" alt=""></div>
			<div><img src="images/slider/slide4.jpg" alt=""></div>
			<div><img src="images/slider/slide5.jpg" alt=""></div>

		</div>

	</section>



	<section id="que-hacemos" class="parallax-window" data-parallax="scroll" data-image-src="images/bg-puzzle.png" naturalWidth="1920" naturalHeight="425">

		<div class="container">

			

			<figure class="ico"><img src="images/ico-quienes.png" alt="CEAMSO"></figure>

			<h2 class="underline">¿Qué <strong>hacemos?</strong><span></span></h2>

			<p><strong class="verde">CEAMSO</strong> es una organización interdisciplinaria, de carácter civil, técnico y científico sin fines de lucro que contribuye al desarrollo de las comunidades a través de iniciativas sociales y ambientales que promuevan el respeto a los derechos humanos, buscando construir una sociedad más <em>informada, organizada y participativa.</em></p>

			<a href="sobre-ceamso.php" class="btn-conocenos">Conocenos</a>



		</div>

	</section>



	<section id="areas">

		<div class="container">

			<h2 class="underline">nuestras <strong>áreas de acción</strong><span></span></h2>

			<p>Estas son las áreas en las que trabajamos:</p>



			<div class="clear">

				<article class="area-accion">

					<a href="proyectos?area=1" class="icon"><img src="images/ico-gobernabilidad.png" alt="Gobernabilidad y Fortalecimiento Institucional"></a>

					<h3>Gobernabilidad y Fortalecimiento Institucional</h3>

					<a href="proyectos?area=1" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=2" class="icon"><img src="images/ico-inclusion.png" alt="Inclusión social y género"></a>

					<h3>Inclusión social y género</h3>

					<a href="proyectos?area=2" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=3" class="icon"><img src="images/ico-medioambiente.png" alt="Medioambiente y saneamiento"></a>

					<h3>Medioambiente y saneamiento</h3>

					<a href="proyectos?area=3" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=4" class="icon"><img src="images/ico-tecnologias.png" alt="Tecnologías innovadoras"></a>

					<h3>Tecnologías innovadoras</h3>

					<a href="proyectos?area=4" class="btn-rectangular">VER PROYECTOS</a>

				</article>

				<article class="area-accion">

					<a href="proyectos?area=5" class="icon"><img src="images/ico-desarrollo.png" alt="Desarrollo socioeconómico"></a>

					<h3>Desarrollo socioeconómico</h3>

					<a href="proyectos?area=5" class="btn-rectangular">VER PROYECTOS</a>

				</article>

			</div>

		</div>

	</section>



	<div class="separador-noticias"></div>



	<section id="noticias">

		<div class="container">

			<div class="clear">

				<h2 class="underline left">últimas <strong>noticias</strong><span></span></h2>

				<a href="noticias.php" class="btn linea-gris right">VER TODAS</a>

			</div>



			<!-- NOTICIAS -->

			<div class="clear">



				<?php foreach($noticias as $rs) { 

					$id = $rs['id'];

					$qgaleria = new DBQuery("SELECT imagen FROM galeria_noticias where noticia_id = '{$id}' limit 1");

					$galeria = $db->executeQuery($qgaleria);

					$fecha = TextHelper::fecha_partes($rs{'fecha_creacion'});

					?>

				<article class="noticia <? echo TextHelper::area_clase($rs['area_id']); ?>">

					<a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="img">

						<span class="fecha"><strong><?php echo $fecha['dia']?></strong><br><?php echo $fecha['mes']?><br><span class="anho"><?php echo $fecha['anho']?></span></span>

						<img src="<?php echo CONF_SITE_URL.'/upload/galeria_noticias/'.$galeria[0]['imagen'] ?>" alt="Noticias CEAMSO">

					</a>

					<a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="area-link"><?php echo $rs['area'.$idioma] ?></a>

					<h3><a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>"><?php echo $rs['titulo'.$idioma] ?></a></h3>

					<p><?php echo TextHelper::truncate($rs['copete'.$idioma],150) ?></p>

				</article>

				<?php } ?>

			</div>

		</div>

	</section>



	<section id="ultimos-proyectos">

		<div class="bg parallax-window" data-parallax="scroll" data-image-src="images/puzzles2.png" naturalWidth="1920" naturalHeight="425" data-z-index="1">

			<div class="container">

				

				<div class="clear">

					<h2 class="underline left">últimos <strong>proyectos</strong><span></span></h2>

					<a href="proyectos.php" class="btn linea-blanca right">VER TODOS</a>

				</div>



				<div class="proyectos">

					<article class="proyecto destacado1 area1">

						<a href="programa-de-democracia-y-gobernabilidad" class="img"><img src="images/img-proyecto.jpg" alt=""><span></span></a>

						<div class="content">

							<a href="proyectos?area=1" class="area-link">GOBERNABILIDAD Y FORTALECIMIENTO INSTITUCIONAL</a>

							<h3 class="underline"><a href="programa-de-democracia-y-gobernabilidad">Programa de Democracia y Gobernabilidad<span></span></a></h3>

							<p>Es un programa de la Agencia de los Estados Unidos para el Desarrollo Internacional (USAID) implementado por CEAMSO con el objetivo de fortalecer la capacidad de instituciones claves...</p>

						</div>

						<div class="footer">

							<p><img src="images/ico-inicio.png" alt="Inicio"> <strong>Año de Inicio</strong> 2012</p>

							<p><img src="images/ico-final.png" alt="Finalización"> <strong>Año de Finalización</strong> 2018</p>

						</div>

					</article>



					<?php foreach($proyectos as $i=> $rs) { 

						$id = $rs['id'];

						$qgaleria = new DBQuery("SELECT imagen FROM galeria_proyectos where proyecto_id = '{$id}' limit 1");

						$galeria = $db->executeQuery($qgaleria);

						$fecha_inicio = TextHelper::fecha_partes($rs{'fecha_inicio'});

						$fecha_fin = TextHelper::fecha_partes($rs{'fecha_fin'});

					?>



					<article class="proyecto <? echo ($i == 0) ? 'destacado2' : '' ?> <? echo TextHelper::area_clase($rs['area_id']); ?>">

						<a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="img"><img src="<?php echo CONF_SITE_URL.'/upload/galeria_proyectos/'.$galeria[0]['imagen'] ?>" alt=""><span></span></a>

						<div class="content">

							<a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="area-link"><?php echo $rs['area'.$idioma]?></a>

							<h3 class="underline"><a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>"><?php echo $rs['titulo'.$idioma]?> <span></span></a></h3>

							<p><?php echo TextHelper::truncate($rs['copete'.$idioma],150) ?></p>

						</div>

						<div class="footer">

							<p><img src="images/ico-inicio.png" alt="Inicio"> <?php echo $fecha_inicio['anho']?></p>

							<p><img src="images/ico-final.png" alt="Finalización"> <?php echo $fecha_fin['anho']?></p>

						</div>

					</article>

					<?php } ?>

					

				</div>



			</div>

		</div>

	</section>



	<section id="historias-exito">

		<div class="container">



				<div class="clear">

					<h2 class="underline left">HISTORIAS <strong>DE ÉXITO</strong><span></span></h2>

					<a href="historias-de-exito.php" class="btn linea-gris right">VER TODAS</a>

				</div>



				<div class="historias-slide">

					<?php foreach($exitos as $i=> $rs) {  ?>

					<div>

						<a href="<?php echo CONF_SITE_URL; ?>historias-de-exito/<?php echo $rs['id']."-".TextHelper::urlString($rs{'nombre'}) ?>" class="img"><img src="<?php echo CONF_SITE_URL.'/upload/historias_exitos/'.$rs['imagen'] ?>" alt=""></a>

						<div class="content">

							<a href="<?php echo CONF_SITE_URL; ?>historias-de-exito/<?php echo $rs['id']."-".TextHelper::urlString($rs{'nombre'}) ?>" class="desc"><span class="comilla1"></span><?php echo $rs['descripcion'.$idioma] ?><img src="images/comilla.png" alt=""></a>

							<p class="nombre"><?php echo $rs['nombre'] ?></p>

							<p class="cargo"><?php echo $rs['institucion'.$idioma] ?></p>

						</div>

					</div>

					<?php } ?>

				</div>

			

		</div>

	</section>



	<section id="alianzas">

		<div class="container">

			

			<h2 class="underline">Alianzas<span></span></h2>

			<div class="content">

				<div class="logos-slide">

					<?php foreach($alianzas as $i=> $rs) {  ?>

					<div><a href="javascript:void(0);" data-toggle="popover" title="<?php echo $rs['titulo'.$idioma] ?>" data-content="<?php echo $rs['descripcion'.$idioma] ?>"><img src="<?php echo CONF_SITE_URL.'/upload/alianzas_convenios/'.$rs['logo'] ?>" alt="<?php echo $rs['titulo'.$idioma] ?>"></a></div>

					<?php } ?>

				</div>

			</div>



		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

