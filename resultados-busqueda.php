<?php include('inc/config.php'); ?>

<?php 

$seccion = TextHelper::cleanString($_GET['seccion']);

$query = TextHelper::cleanString($_GET['q']);

?>

<head>

<title>Resultados de Búsqueda | <?=SITENAME;?></title>
<meta name="description" content="CEAMSO es una organización interdisciplinaria, de carácter civil, técnico y científico sin fines de lucro que contribuye al desarrollo de las comunidades a través de iniciativas sociales y ambientales que promuevan el respeto a los derechos humanos, buscando construir una sociedad más informada, organizada y participativa." />
<meta name="keywords" content="<?=GRALKEYS;?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript">
	$(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>

<script type="text/javascript"> 

	$(document).ready(function(){	
		cargaPaginacion(1);	
	});
	
	var pagina_actual = 1;
	function cargaPaginacion(p_pagina, seccion ){

		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 
		$.ajax({
			type: 'GET',
			url: '<?=CONF_SITE_URL?>ajax/resultados-busqueda.php',
			data: { 
				p: p_pagina,
				seccion : '<?php echo $seccion?>',
				q : '<?php echo $query?>'
			},
			success: function(p_html){
				pagina_actual = p_pagina;
				$('#contenido').html(p_html);
			}
		});
	}
</script>

</head>

<body class="sec-publicaciones">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">
			<h1 class="underline"><strong>Resultados de Búsqueda</strong><span></span></h1>
		</div>
	</section>

	<section id="content">
		<div class="container" id="contenido">

			

		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
