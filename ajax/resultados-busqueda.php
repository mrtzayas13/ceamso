<?php 
	
	include("../inc/config.inc.php");
	
	$db = new DBManager();

	$seccion = TextHelper::cleanString($_GET['seccion']);
	$query_string = TextHelper::cleanString($_GET['q']);

	$sql = (!empty($seccion)) ? " and (titulo_esp like '%{$query_string}%' OR descripcion_esp like '%{$query_string}%' OR titulo_eng like '%{$query_string}%' OR descripcion_eng like '%{$query_string}%' )" : '';

	switch($seccion)
	{
		case 'llamados': 
			$tabla = 'licitaciones';
			$link = 'llamado';
		break;
		case 'noticias': 
			$tabla = 'noticias';
			$link = 'noticia';
		break;
		case 'blog': 
			$tabla = 'blog';
			$link = 'articulo';
		break;
		case 'publicaciones': 
			$tabla = 'publicaciones';
			$link = 'publicacion';
		break;
		case 'proyectos': 
			$tabla = 'proyectos';
			$link = 'proyecto';
		break;
		case 'videos': 
			$tabla = 'videos';
			$link = 'videos';
		break;
		default: 
			$tabla = 'noticias';
			$link = 'noticia';
	}
		
	$query = new DBQuery("SELECT count(*) cantidad FROM {$tabla} where activo = 'SI' {$sql}");
	$publicaciones = $db->executeQuery($query);
	
	$cant_filas = $publicaciones[0]['cantidad'];
	$reg_x_pag = 6;
	$num_pagina = (isset($_GET['p']) && (preg_match('/^\d+$/', $_GET['p']))) ? $_GET['p'] : 1;
	$cant_paginas = ceil($cant_filas / $reg_x_pag); 
	$num_inicio = (($num_pagina - 1) * $reg_x_pag); 
	
	$query = new DBQuery("SELECT * FROM {$tabla} 
						  where activo = 'SI' {$sql}
						  order by id desc
						  LIMIT {$reg_x_pag} OFFSET {$num_inicio} ");								
	$publicaciones = $db->executeQuery($query);
	
if(count($publicaciones)>0){?>

<p style="color: #bdbdbd;" class="mb20"><em>Mostrando <?php echo $cant_filas?> resultados para "<?php echo $query_string?>" en la seccion "<?php echo $tabla?>"</em></p>    

<?php foreach($publicaciones as $i=> $rs){?>             

<article class="resultado clear">
	<div class="content">
		<h3>
			<?php if($link != 'publicacion'){ ?>
				<a href="<?php echo CONF_SITE_URL ."{$link}/". $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>">
			<?php }else{?>
				<a href="<?php echo CONF_SITE_URL.'upload/publicaciones/'.$rs['archivo'] ?>" download="">
			<?php }?>
				<?php echo $rs['titulo'.$idioma] ?>
			</a>
		</h3>
		<p><?php echo TextHelper::truncate($rs['descripcion'.$idioma],250) ?></p>
		<p class="detalles">Publicado el <?php echo date('d/m/Y', strtotime($rs['fecha_creacion']))?></p>
	</div>
</article>

<?php }?>

<?php }else{ ?>

 	<h1 align="center" class="mt100 mb100">Sin publicaciones para esta sección.</h1>

 <?php }?>
 
  <?php if($cant_paginas > 1): ?>
<div id="paginacion">
	<ul>
	 <?php  
	 $botones_visibles = 5;
	 $limite_inicio = $num_pagina - (($botones_visibles - 1) / 2);
	 $limite_final 	= $num_pagina + (($botones_visibles - 1) / 2);
	 if($limite_inicio < 1){
		$limite_final = $limite_final + ($limite_inicio * -1) + 1;
		$limite_inicio = 1;
		if($limite_final > $cant_paginas)
			$limite_final = $cant_paginas;
	 }
	 if($limite_final > $cant_paginas){
		$limite_inicio = $limite_inicio + ($limite_final - $cant_paginas) + 1;
		$limite_final = $cant_paginas;
		if($limite_inicio < 1)
			$limite_inicio = 1;
	 }
	 
	if($num_pagina > 1){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo (num_pagina - 1); ?>, '<?php echo $seccion?>')">«</a> </li>
		<?php
	}
	for($i = $limite_inicio; $i <= $limite_final; $i++): 
		if($i == $num_pagina){
			echo '<li><a class="activo">'.$i.'</a></li>';
		}else{
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo $i; ?>, '<?php echo $seccion?>')"> <?php echo $i; ?></a></li>
		<?php
		}
	 endfor; 
	 
	if($num_pagina < $cant_paginas){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo ($num_pagina + 1); ?>, '<?php echo $seccion?>')">»</a> </li>
		<?php
	}
	?>
	</ul>
	 </div>
 <?php endif; ?>