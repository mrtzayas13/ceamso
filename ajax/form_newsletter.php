<?php 
include_once '../inc/config.inc.php'; 

$_parametros['nombre'] = TextHelper::cleanString($_POST['nombre']);
$_parametros['email'] = TextHelper::cleanString($_POST['email']);
$_parametros['llamados'] = ( $_POST['llamados'] == 'on') ? 1 : 0;
$_parametros['novedades'] = ( $_POST['novedades'] == 'on') ? 1 : 0;

$db = new DBManager();
//echo "SELECT count(*) cantidad FROM newsletter where email = '".$_parametros['email']."'";
$query = new DBQuery("SELECT count(*) cantidad FROM newsletter where email = '".$_parametros['email']."'");
$newsletter_q = $db->executeQuery($query);

if($newsletter_q[0]['cantidad']> 0)
{
	$data = array('success' => false, 'mensaje' => 'Esta cuenta de correo ya se encuentra registrada');
}else{

	$newsletter  = new newsletter(); 
	$newsletter->set_id(null); 
	$newsletter->set_nombre( $_POST['nombre']); 
	$newsletter->set_email( $_POST['email']); 
	$newsletter->set_recibir_llamados($_parametros['llamados']); 
	$newsletter->set_recibir_novedades( $_parametros['novedades']); 
	$newsletter->guarda(); 
	$data = array('success' => true, 'mensaje' => '¡Gracias por tu suscripción!');

}

# MENSAJE JSON

echo json_encode($data);
exit();
?>