<?php 
	
	include("../inc/config.inc.php");
	
	$db = new DBManager();
		
	$query = new DBQuery("SELECT count(*) cantidad FROM blog where activo = 'SI' ");
	$publicaciones = $db->executeQuery($query);
	
	$cant_filas = $publicaciones[0]['cantidad'];
	$reg_x_pag = 6;
	$num_pagina = (isset($_GET['p']) && (preg_match('/^\d+$/', $_GET['p']))) ? $_GET['p'] : 1;
	$cant_paginas = ceil($cant_filas / $reg_x_pag); 
	$num_inicio = (($num_pagina - 1) * $reg_x_pag); 
	
	$query = new DBQuery("SELECT n.* , u.nombre, u.apellido 
						  FROM blog n
						  LEFT OUTER JOIN usuarios u ON u.id_usuario = n.usuario_id
						  where n.activo = 'SI'
						  order by n.id desc
						  LIMIT {$reg_x_pag} OFFSET {$num_inicio} ");								
	$publicaciones = $db->executeQuery($query);

if(count($publicaciones)>0){?>

<?php foreach($publicaciones as $i=> $rs){
$id = $rs['id'];
$qgaleria = new DBQuery("SELECT imagen FROM galeria_blog where blog_id = '{$id}' limit 1");
$galeria = $db->executeQuery($qgaleria);
$fecha = TextHelper::fecha_partes($rs{'fecha_creacion'});
	?>             

<article class="articulo-big clear">
	<a href="<?php echo CONF_SITE_URL; ?>articulo/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="img">
	<img src="<?php echo CONF_SITE_URL.'upload/galeria_blog/'.$galeria[0]['imagen'] ?>" alt="Noticias CEAMSO"></a>
	<div class="content">
		<h2 class="underline"><a href="<?php echo CONF_SITE_URL; ?>articulo/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>"><?php echo TextHelper::truncate($rs['titulo'.$idioma],150) ?><span></span></a></h2>
		<p class="mb10"><?php echo TextHelper::truncate($rs['copete'.$idioma],150) ?></p>
		<p class="pub">Por <?php echo $rs['nombre'] . " " .$rs['apellido']?> • <img src="images/ico-hora.png" alt="Hora/Fecha"> <?php echo TextHelper::convertToLongDate($rs['fecha_creacion'])?></p>
	</div>
</article>

<?php }?>

<?php }else{ ?>

 	<h1 align="center" class="mt100 mb100">Sin publicaciones para esta sección.</h1>

 <?php }?>
 
  <?php if($cant_paginas > 1): ?>
<div id="paginacion">
	<ul>
	 <?php  
	 $botones_visibles = 5;
	 $limite_inicio = $num_pagina - (($botones_visibles - 1) / 2);
	 $limite_final 	= $num_pagina + (($botones_visibles - 1) / 2);
	 if($limite_inicio < 1){
		$limite_final = $limite_final + ($limite_inicio * -1) + 1;
		$limite_inicio = 1;
		if($limite_final > $cant_paginas)
			$limite_final = $cant_paginas;
	 }
	 if($limite_final > $cant_paginas){
		$limite_inicio = $limite_inicio + ($limite_final - $cant_paginas) + 1;
		$limite_final = $cant_paginas;
		if($limite_inicio < 1)
			$limite_inicio = 1;
	 }
	 
	if($num_pagina > 1){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo (num_pagina - 1); ?>)">«</a> </li>
		<?php
	}
	for($i = $limite_inicio; $i <= $limite_final; $i++): 
		if($i == $num_pagina){
			echo '<li><a class="activo">'.$i.'</a></li>';
		}else{
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo $i; ?>)"> <?php echo $i; ?></a></li>
		<?php
		}
	 endfor; 
	 
	if($num_pagina < $cant_paginas){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo ($num_pagina + 1); ?>)">»</a> </li>
		<?php
	}
	?>
	</ul>
	 </div>
 <?php endif; ?>