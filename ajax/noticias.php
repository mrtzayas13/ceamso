<?php 

	

	include("../inc/config.inc.php");

	

	$db = new DBManager();



	$historia_id = TextHelper::cleanNumber($_GET['historia_id']);

	$area_id = TextHelper::cleanNumber($_GET['area_id']);



	$historia_id = ($historia_id != 0) ? $historia_id : 0;



	$area_sql = ($area_id != 0) ? " and area_id = '{$area_id}'" : '';

		

	$query = new DBQuery("SELECT count(*) cantidad FROM noticias where activo = 'SI' {$area_sql}");

	$publicaciones = $db->executeQuery($query);

	

	$cant_filas = $publicaciones[0]['cantidad'];

	$reg_x_pag = 12;

	$num_pagina = (isset($_GET['p']) && (preg_match('/^\d+$/', $_GET['p']))) ? $_GET['p'] : 1;

	$cant_paginas = ceil($cant_filas / $reg_x_pag); 

	$num_inicio = (($num_pagina - 1) * $reg_x_pag); 

	

	$query = new DBQuery("SELECT noticias.*, areas.area_esp,areas.area_eng FROM noticias 
					      LEFT OUTER JOIN areas on noticias.area_id = areas.id
						  where noticias.activo = 'SI' {$area_sql}

						  order by noticias.fecha_creacion desc

						  LIMIT {$reg_x_pag} OFFSET {$num_inicio} ");								

	$publicaciones = $db->executeQuery($query);



if(count($publicaciones)>0){?>



<?php foreach($publicaciones as $i=> $rs){

$id = $rs['id'];

$qgaleria = new DBQuery("SELECT imagen FROM galeria_noticias where noticia_id = '{$id}' limit 1");

$galeria = $db->executeQuery($qgaleria);

$fecha = TextHelper::fecha_partes($rs{'fecha_creacion'});

	?>             



<article class="noticia <? echo TextHelper::area_clase($rs['area_id']); ?>">

	<a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="img">

		<span class="fecha"><strong><?php echo $fecha['dia']?></strong><br><?php echo $fecha['mes']?><br><span class="anho"><?php echo $fecha['anho']?></span></span>

		<img src="<?php echo CONF_SITE_URL.'upload/galeria_noticias/'.$galeria[0]['imagen'] ?>" alt="Noticias CEAMSO">

	</a>

	<a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="area-link"><?php echo $rs['area'.$idioma] ?></a>

	<h3><a href="<?php echo CONF_SITE_URL; ?>noticia/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>"><?php echo $rs['titulo'.$idioma] ?></a></h3>

	<p><?php echo TextHelper::truncate($rs['copete'.$idioma],150) ?></p>

</article>



<?php }?>



<?php }else{ ?>



 	<h1 align="center" class="mt100 mb100">Sin publicaciones para esta sección.</h1>



 <?php }?>

 

  <?php if($cant_paginas > 1): ?>

<div id="paginacion">

	<ul>

	 <?php  

	 $botones_visibles = 5;

	 $limite_inicio = $num_pagina - (($botones_visibles - 1) / 2);

	 $limite_final 	= $num_pagina + (($botones_visibles - 1) / 2);

	 if($limite_inicio < 1){

		$limite_final = $limite_final + ($limite_inicio * -1) + 1;

		$limite_inicio = 1;

		if($limite_final > $cant_paginas)

			$limite_final = $cant_paginas;

	 }

	 if($limite_final > $cant_paginas){

		$limite_inicio = $limite_inicio + ($limite_final - $cant_paginas) + 1;

		$limite_final = $cant_paginas;

		if($limite_inicio < 1)

			$limite_inicio = 1;

	 }

	 

	if($num_pagina > 1){

		?>

		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo (num_pagina - 1); ?>, <?php echo $area_id?>)">«</a> </li>

		<?php

	}

	for($i = $limite_inicio; $i <= $limite_final; $i++): 

		if($i == $num_pagina){

			echo '<li><a class="activo">'.$i.'</a></li>';

		}else{

		?>

		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo $i; ?>, <?php echo $area_id?>)"> <?php echo $i; ?></a></li>

		<?php

		}

	 endfor; 

	 

	if($num_pagina < $cant_paginas){

		?>

		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo ($num_pagina + 1); ?>, <?php echo $area_id?>)">»</a> </li>

		<?php

	}

	?>

	</ul>

	 </div>

 <?php endif; ?>