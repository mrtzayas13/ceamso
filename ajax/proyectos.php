<?php 

	

	include("../inc/config.inc.php");

	

	$db = new DBManager();



	$area_id = TextHelper::cleanNumber($_GET['area_id']);



	$area_sql = ($area_id != 0) ? " and area_id = '{$area_id}'" : '';



	$estado = TextHelper::cleanString($_GET['estado']);

	$estado = ($estado == 'ejecutado') ? "ejecutado" : "ejecucion";

		

	$query = new DBQuery("SELECT count(*) cantidad FROM proyectos where activo = 'SI' and tipo = '{$estado}' {$area_sql}");

	$publicaciones = $db->executeQuery($query);

	

	$cant_filas = $publicaciones[0]['cantidad'];

	$reg_x_pag = 6;

	$num_pagina = (isset($_GET['p']) && (preg_match('/^\d+$/', $_GET['p']))) ? $_GET['p'] : 1;

	$cant_paginas = ceil($cant_filas / $reg_x_pag); 

	$num_inicio = (($num_pagina - 1) * $reg_x_pag); 

	

	$query = new DBQuery("SELECT proyectos.*, areas.area_esp,areas.area_eng FROM proyectos 
						LEFT OUTER JOIN areas on proyectos.area_id = areas.id
						  where proyectos.activo = 'SI' and tipo = '{$estado}' {$area_sql}

						  order by proyectos.id desc

						  LIMIT {$reg_x_pag} OFFSET {$num_inicio} ");								

	$publicaciones = $db->executeQuery($query);



if(count($publicaciones)>0){?>

	<div class="proyectos">

<?php if($estado == 'ejecucion'){?>



<article class="proyecto area1">

	<a href="programa-de-democracia-y-gobernabilidad" class="img"><img src="images/img-proyecto.jpg" alt=""><span></span></a>

	<div class="content">

		<a href="proyectos?area=1" class="area-link">GOBERNABILIDAD Y FORTALECIMIENTO INSTITUCIONAL</a>

		<h3 class="underline"><a href="programa-de-democracia-y-gobernabilidad">Programa de Democracia y Gobernabilidad<span></span></a></h3>

		<p>Es un programa de la Agencia de los Estados Unidos para el Desarrollo Internacional (USAID) implementado por CEAMSO con el objetivo de fortalecer la capacidad de instituciones claves...</p>

	</div>

	<div class="footer">

		<p><img src="images/ico-inicio.png" alt="Inicio"> <strong>Año de Inicio</strong> 2012</p>

		<p><img src="images/ico-final.png" alt="Finalización"> <strong>Año de Finalización</strong> 2018</p>

	</div>

</article>

<?php }?>

<?php foreach($publicaciones as $i=> $rs){

$id = $rs['id'];

$qgaleria = new DBQuery("SELECT imagen FROM galeria_proyectos where proyecto_id = '{$id}' limit 1");

$galeria = $db->executeQuery($qgaleria);

$fecha_inicio = TextHelper::fecha_partes($rs{'fecha_inicio'});

$fecha_fin = TextHelper::fecha_partes($rs{'fecha_fin'});

?>             

	

<article class="proyecto <? //echo ($i == 0) ? 'destacado2' : '' ?> <? echo TextHelper::area_clase($rs['area_id']); ?>">

	<a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="img"><img src="<?php echo CONF_SITE_URL.'/upload/galeria_proyectos/'.$galeria[0]['imagen'] ?>" alt=""><span></span></a>

	<div class="content">

		<a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="area-link"><?php echo $rs['area'.$idioma]?></a>

		<h3 class="underline"><a href="<?php echo CONF_SITE_URL; ?>proyecto/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>"><?php echo $rs['titulo'.$idioma]?> <span></span></a></h3>

		<p><?php echo TextHelper::truncate($rs['copete'.$idioma],150) ?></p>

		<?php if($rs['categoria'] === 'Programa'){?>
			<p class="cartel cartel_programa"><?php echo $rs['categoria'] ?> <span></span></p>
		<?php }elseif($rs['categoria'] === 'Proyecto'){?>
			<p class="cartel cartel_proyecto"><?php echo $rs['categoria'] ?> <span></span></p>
		<?php } ?>

	</div>

	<div class="footer">

		<p><img src="images/ico-inicio.png" alt="Inicio"> <?php echo $fecha_inicio['anho']?></p>

		<p><img src="images/ico-final.png" alt="Finalización"> <?php echo $fecha_fin['anho']?></p>

	</div>

</article>



<?php }?>

</div>



<?php }else{ ?>



 	



 <?php }?>

 

  <?php if($cant_paginas > 1): ?>

<div id="paginacion">

	<ul>

	 <?php  

	 $botones_visibles = 5;

	 $limite_inicio = $num_pagina - (($botones_visibles - 1) / 2);

	 $limite_final 	= $num_pagina + (($botones_visibles - 1) / 2);

	 if($limite_inicio < 1){

		$limite_final = $limite_final + ($limite_inicio * -1) + 1;

		$limite_inicio = 1;

		if($limite_final > $cant_paginas)

			$limite_final = $cant_paginas;

	 }

	 if($limite_final > $cant_paginas){

		$limite_inicio = $limite_inicio + ($limite_final - $cant_paginas) + 1;

		$limite_final = $cant_paginas;

		if($limite_inicio < 1)

			$limite_inicio = 1;

	 }

	 

	if($num_pagina > 1){

		?>

		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo (num_pagina - 1); ?>,0, <?php echo $area_id?>)">«</a> </li>

		<?php

	}

	for($i = $limite_inicio; $i <= $limite_final; $i++): 

		if($i == $num_pagina){

			echo '<li><a class="activo">'.$i.'</a></li>';

		}else{

		?>

		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo $i; ?>,0, <?php echo $area_id?>)"> <?php echo $i; ?></a></li>

		<?php

		}

	 endfor; 

	 

	if($num_pagina < $cant_paginas){

		?>

		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo ($num_pagina + 1); ?>,0, <?php echo $area_id?>)">»</a> </li>

		<?php

	}

	?>

	</ul>

	 </div>

 <?php endif; ?>