<?php 
	
	include("../inc/config.inc.php");
	
	$db = new DBManager();
		
	$query = new DBQuery("SELECT count(*) cantidad FROM historias_exitos where activo = 'SI'");
	$publicaciones = $db->executeQuery($query);
	
	$cant_filas = $publicaciones[0]['cantidad'];
	$reg_x_pag = 6;
	$num_pagina = (isset($_GET['p']) && (preg_match('/^\d+$/', $_GET['p']))) ? $_GET['p'] : 1;
	$cant_paginas = ceil($cant_filas / $reg_x_pag); 
	$num_inicio = (($num_pagina - 1) * $reg_x_pag); 
	
	$query = new DBQuery("SELECT * FROM historias_exitos where activo = 'SI'
						  order by id desc
						  LIMIT {$reg_x_pag} OFFSET {$num_inicio} ");								
	$publicaciones = $db->executeQuery($query);

if(count($publicaciones)>0){?>
<div class="listado-historias">
<?php foreach($publicaciones as $i=> $rs){?>             
	
<article class="historia-exito">
	<figure class="img"><img src="<?php echo CONF_SITE_URL.'/upload/historias_exitos/'.$rs['imagen'] ?>" alt=""></figure>
	<div class="content">
		<a href="<?php echo CONF_SITE_URL; ?>historias-de-exito/<?php echo $rs['id']."-".TextHelper::urlString($rs{'nombre'}) ?>" class="desc"><img src="images/comilla.png" alt="'" class="mr10"><?php echo $rs['descripcion'.$idioma] ?><img src="images/comilla.png" alt="'" class="ml10"></a>
		<p class="nombre"><?php echo $rs['nombre'] ?></p>
		<p class="cargo"><?php echo $rs['institucion'.$idioma] ?></p>
	</div>
</article>

<?php }?>
 </div>
<?php }else{ ?>

 	<h1 align="center" class="mt100 mb100">Sin publicaciones para esta sección.</h1>

 <?php }?>
 
  <?php if($cant_paginas > 1): ?>
<div id="paginacion">
	<ul>
	 <?php  
	 $botones_visibles = 5;
	 $limite_inicio = $num_pagina - (($botones_visibles - 1) / 2);
	 $limite_final 	= $num_pagina + (($botones_visibles - 1) / 2);
	 if($limite_inicio < 1){
		$limite_final = $limite_final + ($limite_inicio * -1) + 1;
		$limite_inicio = 1;
		if($limite_final > $cant_paginas)
			$limite_final = $cant_paginas;
	 }
	 if($limite_final > $cant_paginas){
		$limite_inicio = $limite_inicio + ($limite_final - $cant_paginas) + 1;
		$limite_final = $cant_paginas;
		if($limite_inicio < 1)
			$limite_inicio = 1;
	 }
	 
	if($num_pagina > 1){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo (num_pagina - 1); ?>)">«</a> </li>
		<?php
	}
	for($i = $limite_inicio; $i <= $limite_final; $i++): 
		if($i == $num_pagina){
			echo '<li><a class="activo">'.$i.'</a></li>';
		}else{
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo $i; ?>)"> <?php echo $i; ?></a></li>
		<?php
		}
	 endfor; 
	 
	if($num_pagina < $cant_paginas){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo ($num_pagina + 1); ?>)">»</a> </li>
		<?php
	}
	?>
	</ul>
	 </div>
 <?php endif; ?>