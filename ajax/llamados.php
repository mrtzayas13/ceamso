<?php 
	
	include("../inc/config.inc.php");
	
	$db = new DBManager();

	$orden = TextHelper::cleanString($_GET['orden']);
	$estado = TextHelper::cleanNumber($_GET['estado']);

	$orderby = ($orden == 'inicio' || empty($orden)) ? 'fecha_inicio desc' : 'fecha_fin asc';
	
	$sqlestado = ($estado == 0) ? " and fecha_fin > NOW() " : "";
		
	$query = new DBQuery("SELECT count(*) cantidad FROM licitaciones where 1=1 and activo = 'SI' $sqlestado");
	$publicaciones = $db->executeQuery($query);
	
	$cant_filas = $publicaciones[0]['cantidad'];
	$reg_x_pag = 12;
	$num_pagina = (isset($_GET['p']) && (preg_match('/^\d+$/', $_GET['p']))) ? $_GET['p'] : 1;
	$cant_paginas = ceil($cant_filas / $reg_x_pag); 
	$num_inicio = (($num_pagina - 1) * $reg_x_pag); 
	
	$query = new DBQuery("SELECT n.* 
						  FROM licitaciones n
						  where 1=1 
						  and n.activo = 'SI'
						  $sqlestado
						  order by {$orderby} 
						  LIMIT {$reg_x_pag} OFFSET {$num_inicio} ");								
	$publicaciones = $db->executeQuery($query);

	$queryU = new DBQuery("SELECT n.id 
						  FROM licitaciones n
						  where n.activo = 'SI'
						  order by fecha_inicio desc
						  LIMIT 1");								
	$p_u = $db->executeQuery($queryU);

if(count($publicaciones)>0){?>

	<div class="listado-llamados">

<?php foreach($publicaciones as $i=> $rs){
$fecha_cierre = strtotime($rs['fecha_fin']);
$fecha_ahora = strtotime(date('Y-m-d h:i:s'));
	?>  

<article class="llamado <?php if($p_u[0]['id'] == $rs['id']){ echo 'nuevo';} ?> <?php if($fecha_cierre <= $fecha_ahora){ echo 'llamado-inactivo';}?>">
	<p class="llamado-id"> <?php echo $rs['codigo']?></p>
	<h3 class="underline"><a href="<?php echo CONF_SITE_URL; ?>llamado/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>"><?php echo $rs['titulo'.$idioma]?><span></span></a></h3>
	<div class="llamado-datos">
		<p><strong>Estado:</strong> <span class="activo"><?php echo ($fecha_cierre <= $fecha_ahora) ? 'INACTIVO' : 'ACTIVO';?></span></p>
		<p><strong>Publicado:</strong> <?php echo date('d/m/Y', strtotime($rs['fecha_inicio']))?></p>
		<p><strong>Cierre:</strong> <?php echo date('d/m/Y', strtotime($rs['fecha_fin']))?></p>
	</div>
	<div data-countdown="<?php echo date('Y-m-d H:i', strtotime($rs['fecha_fin']))?>" class="countdown" data-toggle="tooltip" data-placement="top" title="Tiempo aproximado para cierre"></div>
	<a href="<?php echo CONF_SITE_URL; ?>llamado/<?php echo $rs['id']."-".TextHelper::urlString($rs['titulo'.$idioma]) ?>" class="btn verde">VER DETALLES</a>
	<p class="cartel-nuevo">NUEVO<span></span></p>
</article>           



<?php }?>

</div>

<?php }else{ ?>

 	<h1 align="center" class="mt100 mb100">Sin publicaciones para esta sección.</h1>

 <?php }?>
 
  <?php if($cant_paginas > 1): ?>
<div id="paginacion">
	<ul>
	 <?php  
	 $botones_visibles = 5;
	 $limite_inicio = $num_pagina - (($botones_visibles - 1) / 2);
	 $limite_final 	= $num_pagina + (($botones_visibles - 1) / 2);
	 if($limite_inicio < 1){
		$limite_final = $limite_final + ($limite_inicio * -1) + 1;
		$limite_inicio = 1;
		if($limite_final > $cant_paginas)
			$limite_final = $cant_paginas;
	 }
	 if($limite_final > $cant_paginas){
		$limite_inicio = $limite_inicio + ($limite_final - $cant_paginas) + 1;
		$limite_final = $cant_paginas;
		if($limite_inicio < 1)
			$limite_inicio = 1;
	 }
	 
	if($num_pagina > 1){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo (num_pagina - 1); ?>)">«</a> </li>
		<?php
	}
	for($i = $limite_inicio; $i <= $limite_final; $i++): 
		if($i == $num_pagina){
			echo '<li><a class="activo">'.$i.'</a></li>';
		}else{
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo $i; ?>)"> <?php echo $i; ?></a></li>
		<?php
		}
	 endfor; 
	 
	if($num_pagina < $cant_paginas){
		?>
		<li><a href="javascript:;" onclick="cargaPaginacion(<?php echo ($num_pagina + 1); ?>)">»</a> </li>
		<?php
	}
	?>
	</ul>
	 </div>
 <?php endif; ?>