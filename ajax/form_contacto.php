<?php 
include('../inc/config.inc.php'); 
include('../classes/helpers/phpmailer_.class.php');

// GOOGLE RECAPTCHA
$clave_secreta_recaptcha = '6LfPpYEaAAAAADETRviv4u4hT_jEq1jVO2_NQ-Vu';
$g_recaptcha_response = $_POST['g-recaptcha-response'];
$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" .$clave_secreta_recaptcha. "&response=".$g_recaptcha_response."&remoteip=".$_SERVER['REMOTE_ADDR']); 
$response = (array) json_decode($response,true);

if(empty($response['success']))
	die(json_encode(array("success" => false, "mensaje"=> "<p> Datos invalidos. El codigo de verificacion no corresponde.</p>")));

$_parametros['nombre'] = TextHelper::cleanString($_POST['nombre']);
$_parametros['email'] = TextHelper::cleanString($_POST['email']);
$_parametros['mensaje'] = TextHelper::cleanString($_POST['mensaje']);
$_parametros['to'] = "ceamso@ceamso.org.py";	
//$_parametros['to'] = "valentin.zaracho@gmail.com";	

$cuerpo = '<html>
			<body>
				<table border="0" width="700" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">

				<tr>
					<td align="left" valign="top" width="100%" style="padding:20px; ">' . '
						<strong>Nombre:</strong> '.($_parametros['nombre'])'<br />
						<strong>Email:</strong> '.($_parametros['email']).'<br />
						<strong>Comentario:</strong> '.($_parametros['mensaje']).'<br />
					</td>
				</tr>
			</body>
		</html>';
	
$mail = new PHPMailer;
$mail->isSMTP();
$mail->CharSet="UTF-8";
$mail->SMTPSecure = 'ssl';
$mail->Host = 'smtp.gmail.com';
$mail->Port = 465;
$mail->Username = 'ceamso@ceamso.org.py';
$mail->Password = 'akophfnigudsmhee';
$mail->SMTPAuth = true;

$mail->From = $_parametros['email'];
$mail->FromName = $_parametros['nombre'];
$mail->IsHTML(true);
$mail->Timeout = 60;
$mail->CharSet = 'UTF-8';
$mail->AddAddress($_parametros['to']);
$mail->Subject = "Formulario de Contacto :: Ceamso";
$mail->MsgHTML($cuerpo);

if($mail->Send()){
	error_log ('Autorrespuesta ----'); //log de autorrespuesta enviada
	// Una vez que se haya enviado el mail de contacto se crea la autorrespuesta
	$cuerpo_autorespuesta = '<html>
	<body>
	 <table border="0" width="700" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
 
	 <tr>
	  <td align="left" valign="top" width="100%" style="padding:20px; ">' . '
	   ¡Muchas gracias por ponerse en contacto con nostros! Nos estaremos comunicando en la brevedad posible.
	  </td>
	 </tr>
	</body>
   </html>';
 
  $mail_autorespuesta = new PHPMailer;
  $mail_autorespuesta->isSMTP();
  $mail_autorespuesta->CharSet="UTF-8";
  $mail_autorespuesta->SMTPSecure = 'ssl';
  $mail_autorespuesta->Host = 'smtp.gmail.com';
  $mail_autorespuesta->Port = 465;
  $mail_autorespuesta->Username = 'ceamso@ceamso.org.py'; //usar no-reply@ceamso.com.py
  $mail_autorespuesta->Password = 'akophfnigudsmhee';//crear el pass de aplicación y agregar al codigo
  $mail_autorespuesta->SMTPAuth = true;
 
  $mail_autorespuesta->From = "ceamso@ceamso.org.py";
  $mail_autorespuesta->FromName = 'CEAMSO'; //Nombre del remitente
  $mail_autorespuesta->IsHTML(true);
  $mail_autorespuesta->Timeout = 60;
  $mail_autorespuesta->CharSet = 'UTF-8';
  $mail_autorespuesta->AddAddress($_parametros['email']); //aca va el email del contacto
  $mail_autorespuesta->Subject = "Mensaje de autorrespuesta"; //asunto del correo
  $mail_autorespuesta->MsgHTML($cuerpo_autorespuesta);//cuerpo del mensaje
 
  if($mail_autorespuesta->Send()){
   error_log ('Autorrespuesta enviada'); //log de autorrespuesta enviada
  }else{
   error_log ('Autorrespuesta no enviada');// log de autorrespuesta no enviada
  }
  die(json_encode(array("success" => true, 'mensaje' => '¡Hemos recibido su mensaje, muchas gracias por contactarnos!')));
}else{
	die(json_encode(array("success" => false, "mensaje"=> "Ocurrio un error en el envio. Intente Nuevamente en unos momentos.")));
}

/*$asunto   = ':: Contacto :: ceamso@ceamso.org.py';
$headers = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=UTF-8' . "\r\n";
$headers .= 'From: '.($_parametros['nombre']).' <'.($_parametros['email']).'>\r\n';
mail($_parametros['destinatario'],$asunto,$cuerpo,$headers);*/

# MENSAJE JSON
$data = array('success' => true, 'mensaje' => '¡Hemos recibido su mensaje, muchas gracias por contactarnos!');
echo json_encode($data);
exit();
?>