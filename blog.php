<?php include('inc/config.php'); ?>

<head>

<title>Blog | <?=SITENAME;?></title>
<meta name="description" content="Articulos publicados por CEAMSO y otros especialistas." />
<meta name="keywords" content="<?=GRALKEYS;?>, articulos, novedades" />

<?php include('inc/head.php'); ?>

<script type="text/javascript"> 

	$(document).ready(function(){	
		cargaPaginacion(1,0);	
	});
	
	var pagina_actual = 1;
	function cargaPaginacion(p_pagina ){

		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 
		$.ajax({
			type: 'GET',
			url: '<?=CONF_SITE_URL?>ajax/blog.php',
			data: { 
				p: p_pagina
			},
			success: function(p_html){
				pagina_actual = p_pagina;
				$('#contenido').html(p_html);
			}
		});
	}
</script>


</head>

<body class="sec-blog">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">
			<h1 class="underline"><strong>Blog</strong><span></span></h1>
			<!-- <p class="copete">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis laoreet lorem. Proin at sapien at lectus fringilla luctus.</p> -->
		</div>
	</section>

	<section id="content">
		<div class="container">

			<!-- blog -->
			<div class="clear" id="contenido">

				
				
			</div>


		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
