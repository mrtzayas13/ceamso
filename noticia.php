<?php include('inc/config.php'); ?>



<?php



$id = TextHelper::cleanNumber($_GET['id']);

$qnoticias = new DBQuery("SELECT n.*, area_esp, area_eng 

						  FROM noticias n 

						  left outer join areas a on a.id = n.area_id 

						  where n.activo = 'SI' and n.id = '{$id}' ");

$rs_noticia = $db->executeQuery($qnoticias);



if(empty($rs_noticia))

	header("Location: " . URL);



$qgaleria = new DBQuery("SELECT imagen FROM galeria_noticias where noticia_id = '{$id}'");

$galeria = $db->executeQuery($qgaleria);

$fecha = explode(" ", $rs_noticia[0]['fecha_creacion']);
$fecha_larga = TextHelper::convertToLongDate($fecha[0]);

?>



<head>



<title><?php echo $rs_noticia[0]['titulo'.$idioma]?> | Noticias | <?=SITENAME;?></title>

<meta name="description" content="<?php echo strip_tags($rs_noticia[0]['copete'.$idioma]); ?>" />
<meta name="keywords" content="<?=GRALKEYS;?>, noticias, novedades" />

<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $rs_noticia[0]['titulo'.$idioma]?>" />
<meta property="og:description" content="<?php echo strip_tags($rs_noticia[0]['copete'.$idioma]); ?>" />
<meta property="og:image" content="<?php echo CONF_SITE_URL.'upload/galeria_noticias/g__'.$galeria[0]['imagen'] ?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "a20e072a-7689-4359-b30f-f29d1e956e8a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slider-galeria-inn').slick({
			dots: false,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			adaptiveHeight: true
		});
	});
</script>

</head>


<body class="sec-noticias sec-<? echo TextHelper::area_clase($rs_noticia[0]['area_id']); ?>">



	<?php include('inc/header.php'); ?>



	<section id="titulo">
		<div class="container">
			<p class="breadcrumb"><a href="noticias.php">NOTICIAS</a> / <a href="noticias.php"><?php echo $rs_noticia[0]['area'.$idioma]?></a></p>
			<h1 class="underline"><strong><?php echo $rs_noticia[0]['titulo'.$idioma]?></strong><span></span></h1>
		</div>
	</section>



	<section id="content">

		<div class="container">



			<div class="row">

				<div class="col-lg-12 col-md-12">

					<p class="copete mb10"><?php echo $rs_noticia[0]['copete'.$idioma]?></p>

				</div>



				<figure class="col-lg-12 col-md-12 mb20">
					<div class="slider-galeria-inn">
					<?php foreach($galeria as $rs_noticia_img){ ?>
						<div>
							<img src="<?php echo CONF_SITE_URL.'/upload/galeria_noticias/g__'.$rs_noticia_img['imagen'] ?>" alt="">
						</div>
					<?php }?>
					</div>
				</figure>


				

				<div class="col-lg-8 col-lg-push-4 col-md-8 col-md-push-4 col-sm-12 texto-corrido">
				<?php if(!empty($rs_noticia[0]['youtube_url'])){?>
					<figure class="videoWrapper mb20"><iframe width="100%" height="500" src="https://www.youtube.com/embed/<?php echo TextHelper::youtube_url($rs_noticia[0]['youtube_url'])?>" frameborder="0" allowfullscreen></iframe></figure>
					<?php }?>
					<?php echo $rs_noticia[0]['descripcion'.$idioma];?>

				</div>

				<div class="col-lg-4 col-lg-pull-8 col-md-4 col-md-pull-8 col-sm-12 text-center">

					<p class="fecha-publicacion"><em>Publicado el <strong><?php echo $fecha_larga?></strong></em></p>

					<div class="compartir-block">

						<p><img src="images/ico-share.png" alt=""> COMPARTIR EN</p>

						<span class='st_facebook_large' displayText='Facebook'></span>

						<span class='st_twitter_large' displayText='Tweet'></span>

						<span class='st_googleplus_large' displayText='Google +'></span>

						<span class='st_linkedin_large' displayText='LinkedIn'></span>

						<span class='st_email_large' displayText='Email'></span>

					</div>

					<div class="cambiar-idioma mt20">
						<p>Cambiar idioma</p>
						<div id="google_translate_element"></div><script type="text/javascript">
						function googleTranslateElementInit() {
						  new google.translate.TranslateElement({pageLanguage: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
						}
						</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					</div>

					<a href="noticias.php" class="btn linea-gris mt20" style="display: block;"><img src="images/back-arrow.png" alt="Volver">Volver a noticias</a>

				</div>

			</div>



		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

