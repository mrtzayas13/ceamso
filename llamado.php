<?php include('inc/config.php'); ?>



<?php



$id = TextHelper::cleanNumber($_GET['id']);

$qnoticias = new DBQuery("SELECT *

							FROM licitaciones n 

							WHERE n.activo = 'SI' AND n.id = '{$id}'");

$rs = $db->executeQuery($qnoticias);



if(empty($rs))

	header("Location: " . URL);



$qarchivo = new DBQuery("SELECT * FROM archivo_licitaciones where licitacion_id = '{$id}'");

$archivo = $db->executeQuery($qarchivo);



$fecha_cierre = strtotime($rs[0]['fecha_fin']);

$fecha_ahora = strtotime(date('Y-m-d h:i:s'));

?>



<head>



<title><?php echo $rs[0]['codigo']?> : <?php echo $rs[0]['titulo'.$idioma]?> | <?=SITENAME;?></title>
<meta name="description" content="<?php echo $rs[0]['titulo'.$idioma]?>" />
<meta name="keywords" content="<?=GRALKEYS;?>, llamado, licitaciones, licitacion" />

<meta property="og:title" content="<?php echo $rs[0]['codigo']?> : <?php echo $rs[0]['titulo'.$idioma]?> | <?=SITENAME;?>" />
<meta property="og:description" content="<?php echo $rs[0]['titulo'.$idioma]?>" />
<meta property="og:image" content="http://www.ceamso.org.py/images/ceamso-fb.jpg" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="400" />

<?php include('inc/head.php'); ?>

<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/moment-timezone-with-data.js"></script>
<script type="text/javascript" src="js/jquery.countdown.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		$('[data-countdown]').each(function() {
			var date = $(this).data('countdown');
			var finalDate = moment.tz(date, "America/Asuncion");
			var $this = $(this);
			$this.countdown(finalDate.toDate(), function(event) {
				$(this).html(event.strftime(''
				+ '<p><strong>%D</strong>DIAS</p>'
				+ '<p><strong>%H</strong>HORAS</p>'
				+ '<p><strong>%M</strong>MIN</p>'));
			});
		});
	});
</script>



</head>



<body class="sec-llamados">



	<?php include('inc/header.php'); ?>



	<section id="titulo">

		<div class="container">

			<p class="breadcrumb"><a href="llamados.php">LLAMADOS</a></p>

			<h1 class="underline"><strong><em class="verde"><?php echo $rs[0]['codigo']?>:</em> <?php echo $rs[0]['titulo'.$idioma]?></strong><span></span></h1>

		</div>

	</section>



	<section id="content">

		<div class="container sidebar-bg">

			<div class="row">

				<div class="col-lg-8 col-md-8 col-sm-12 pr30">

					<div class="detalles-licitacion">

						<?php echo $rs[0]['descripcion'.$idioma]?>

					</div>



					<h2 class="underline">Datos de la <strong>licitación</strong><span></span></h2>

					<div class="datos-licitacion <?php echo ($fecha_cierre <= $fecha_ahora) ? 'llamado-inactivo' : '';?> clear">

						<div class="celda">

							<ul class="datos pr20">

								<li class="pb10"><strong>Estado:</strong> <span class="activo"><?php echo ($fecha_cierre <= $fecha_ahora) ? 'INACTIVO' : 'ACTIVO';?></span></li>

								<li><strong>ID:</strong> <?php echo $rs[0]['codigo']?></li>

							</ul>

							<ul class="datos">

								<li class="pb15"><strong>Publicado:</strong> <?php echo date('d/m/Y', strtotime($rs[0]['fecha_inicio']))?></li>

								<li><strong>Cierre:</strong> <?php echo date('d/m/Y', strtotime($rs[0]['fecha_fin']))?></li>

							</ul>

						</div>

						<div class="celda clear"><div data-countdown="<?php echo date('Y-m-d H:i', strtotime($rs[0]['fecha_fin']))?>" class="countdown"></div></div>

					</div>



					<h2 class="underline">Documentos <strong>anexos</strong><span></span></h2>

					<table class="descarga-documentos">

						<thead>

							<tr>

								<th>FECHA</th>

								<th>NOMBRE DEL DOCUMENTO</th>

								<th></th>

							</tr>

						</thead>

						<tbody>

						<?php foreach($archivo as $rs_doc){ ?>

							<tr>

								<td><?php echo date('d/m/Y', strtotime($rs_doc['fecha_creacion']))?></td>

								<td><a href="<?php echo CONF_SITE_URL.'/upload/archivo_licitaciones/'.$rs_doc['archivo'] ?>" download="<?php echo CONF_SITE_URL.'/upload/archivo_licitaciones/'.$rs_doc['archivo'] ?>"><?php echo $rs_doc['nombre'.$idioma]?></a></td>

								<td><a href="<?php echo CONF_SITE_URL.'/upload/archivo_licitaciones/'.$rs_doc['archivo'] ?>" class="descargar" download="<?php echo CONF_SITE_URL.'/upload/archivo_licitaciones/'.$rs_doc['archivo'] ?>" title="Descargar archivo"><img src="images/download.png" alt="Descargar"></a></td>

							</tr>

						<?php }?>

							

						</tbody>

					</table>



					<h2 class="underline">Sobre <strong>consultas</strong><span></span></h2>

					<p>Podrán remitir sus consultas a: <a href="mailto:llamadoaconcurso@ceamso.org.py" class="verde">llamadoaconcurso@ceamso.org.py</a> / <a href="mailto:llamadoaconcurso@ceamso.org.py" class="verde">llamadosceamso@gmail.com</a>. Todas las preguntas o consultas recibidas en fecha serán atendidas y contestadas vía correo electrónico.</p>



				</div>

				<!-- TERMINA COLUMNA CONTENIDO -->



				<!-- SIDEBAR -->

				<?php include('inc/sidebar-llamados.php'); ?>

			</div>

		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

