<?php include('inc/config.php'); ?>

<?php

$db = new DBManager();

$qgrupos_staff = new DBQuery("SELECT DISTINCT grupo FROM staff");
$grupos_staff = $db->executeQuery($qgrupos_staff);

?>

<head>

<title>Staff y Consejo directivo | <?=SITENAME;?></title>
<meta name="description" content="Nuestro Staff, miembros fundadores y consejo directivo." />
<meta name="keywords" content="<?=GRALKEYS;?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
	    var $container = $('.listado-staff');
	    // initialize
	    $container.masonry({
	      itemSelector: '.staff',
	      columnWidth: 370,
	      gutter: 15
	    });
	    var msnry = $container.data('masonry');
	});

</script>

</head>

<body class="sec-ceamso">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">
			<!-- <p class="breadcrumb">¿Quiénes Somos?</p> -->
			<h1 class="underline">¿Quiénes <strong>Somos?</strong><span></span></h1>
		</div>
	</section>

	<section id="content">
		<div class="container">

			<?php foreach($grupos_staff as $rs){ ?>

			<h2 class="underline"><strong><?php echo $rs['grupo']?></strong><span></span></h2>

			<div class="listado-staff">

				<?php 
					$grupo = $rs['grupo'];
					$qstaff = new DBQuery("SELECT * from staff where grupo = '$grupo'");
					$staff = $db->executeQuery($qstaff);

					foreach($staff as $sta){

				 ?>

				<article class="staff">
					<figure class="img"><img src="upload/staff/<?php echo $sta['imagen']?>" alt="Patricia Franco Bogado"></figure>
					<div class="content">
						<h3><?php echo $sta['nombre_apellido']?></h3>
						<p class="cargo"><?php echo $sta['cargo']?></p>
					</div>
				</article>	

				<?php }?>			
	
			</div>
	

			<?php }?>


		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
