<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 


/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('proyectos'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 
    $ods_proyectos  = new ods_proyectos();
    
	 switch($_POST['accion']) {
        case 'edita':
            $search = 'WHERE id_proyectos = '. $_POST['id'];
            $arr = ods_proyectos::lista(array('filtro' => $search));

            
            if(count($arr['datos']) > 0) {
                $result = array();
                foreach ($arr['datos'] as $row):
                    $result[] = $row['id_objetivo'];
                endforeach;
            }else{
                $result[] = [];
            }

            $json_arr = array_values($result);
            
            die(json_encode($json_arr)); 
            break; 

     }
}
?>