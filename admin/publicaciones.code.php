<?php 

 session_start(); 

include_once '../inc/config.inc.php'; 

include_once 'inc/validate-authetication.inc.php'; 

include_once 'inc/validate-request.inc.php'; 



/* Chequeo de permisos de usuario */

if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 

else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 

$nivel_acceso = $usuario_logueado->recupera_permisos('publicaciones'); 

if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 

	die('Acceso no permitido');



/* Acciones a realizar */

if (isset($_POST['accion'])) { 



	 $publicaciones  = new publicaciones(); 



	 switch($_POST['accion']) { 



		 case 'inserta': 

 			 if($nivel_acceso['alta'] == 'S'){ 

				 try{ 

				 $publicaciones->set_id(null); 

				 $publicaciones->set_area_id( $_POST['area_id']); 

				 $publicaciones->set_titulo_esp( $_POST['titulo_esp']); 

				 $publicaciones->set_titulo_eng( $_POST['titulo_eng']); 

				 $publicaciones->set_descripcion_esp( $_POST['descripcion_esp']); 

				 $publicaciones->set_descripcion_eng( $_POST['descripcion_eng']); 

			 $nom_archivo = time(); 

			 if((strlen($publicaciones->get_archivo()) > 0) && (empty($_POST['archivo']))) { 

				 if(file_exists(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo()); } 

			 } 

			 if(FileManager::subeArchivo($_FILES['file_archivo'], 'publicaciones', $nom_archivo)) { 

				 if((strlen($publicaciones->get_archivo()) > 0)) { 

 					 if(file_exists(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo()); } 

				 } 

				 $_POST['archivo'] = $nom_archivo . substr($_FILES['file_archivo'][name], strrpos($_FILES['file_archivo'][name], '.')); 

			 } 

				 $publicaciones->set_activo( $_POST['activo']); 

				 $publicaciones->set_fecha_creacion( $_POST['fecha_creacion']); 

				 $publicaciones->guarda(); 

				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 

 			 } 

 			 break; 

 

		 case 'actualiza': 

 			 if($nivel_acceso['modificacion'] == 'S'){ 

				 try{ 

				 $publicaciones->carga($_POST['id']); 

				 $publicaciones->set_id($_POST['id']); 

				 $publicaciones->set_area_id($_POST['area_id']); 

				 $publicaciones->set_titulo_esp($_POST['titulo_esp']); 

				 $publicaciones->set_titulo_eng($_POST['titulo_eng']); 

				 $publicaciones->set_descripcion_esp($_POST['descripcion_esp']); 

				 $publicaciones->set_descripcion_eng($_POST['descripcion_eng']); 

			 $nom_archivo = time(); 

			 if((strlen($publicaciones->get_archivo()) > 0) && (empty($_POST['archivo']))) { 

				 if(file_exists(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo()); } 

			 } 

			 if(FileManager::subeArchivo($_FILES['file_archivo'], 'publicaciones', $nom_archivo)) { 

				 if((strlen($publicaciones->get_archivo()) > 0)) { 

 					 if(file_exists(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $publicaciones->get_archivo()); } 

				 } 

				 $_POST['archivo'] = $nom_archivo . substr($_FILES['file_archivo'][name], strrpos($_FILES['file_archivo'][name], '.')); 

			 } 

				 $publicaciones->set_archivo($_POST['archivo']); 

			 

				 $publicaciones->set_activo($_POST['activo']); 

				 $publicaciones->set_fecha_creacion($_POST['fecha_creacion']); 

				 if(!$publicaciones->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 

				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 

 			 } 

 			 break; 

 

		 case 'edita': 

 			 $publicaciones->carga($_POST['id']); 

			 $json_arr = array( 'id' => $publicaciones->get_id(),'area_id' => $publicaciones->get_area_id(),'titulo_esp' => $publicaciones->get_titulo_esp(),'titulo_eng' => $publicaciones->get_titulo_eng(),'descripcion_esp' => $publicaciones->get_descripcion_esp(),'descripcion_eng' => $publicaciones->get_descripcion_eng(),'archivo' => $publicaciones->get_archivo(),'activo' => $publicaciones->get_activo(),'fecha_creacion' => (($publicaciones->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($publicaciones->get_fecha_creacion())) : '') ); 

 			 die(json_encode($json_arr)); 

 			 break; 

 

		 case 'elimina': 

 			 if($nivel_acceso['baja'] == 'S'){ 

				 $publicaciones->set_id($_POST['id']); 

 				 if(!$publicaciones->elimina()) { die('Ocurrio un error al elimina el registro.'); } 

				 die(''); 

 			 } 

 			 break; 

 

		 case 'lista': 

 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 

 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden' => '1 desc'); 

			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 

				 $opciones['buscar'] = $_POST['buscar']; 

			 } 

			 $arr = publicaciones::lista($opciones); 



			 if(count($arr['datos']) > 0) { 

			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 

			 $html.= '<thead><tr>'; 

				 $html.= '<th>Id</th>'; 

				 $html.= '<th>Area id</th>'; 

				 $html.= '<th>Titulo esp</th>'; 

				 $html.= '<th>Titulo eng</th>'; 

				 $html.= '<th>Descripcion esp</th>'; 

				 $html.= '<th>Descripcion eng</th>'; 

				 $html.= '<th>Archivo</th>'; 

				 $html.= '<th>Activo</th>'; 

				 $html.= '<th>Fecha creacion</th>'; 

				 $html.= '<th>&nbsp;</th>'; 

			 $html.= '</tr></thead>'; 

			 $estilo_fila = ''; 

			 foreach ($arr['datos'] as $row) { 

				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 

				 $html.= '<tr' . $estilo_fila . '>'; 

					 $html.= '<td>'.$row['id'].'</td>'; 

					 $html.= '<td>'.$row['areas_area_esp'].'</td>'; 

					 $html.= '<td>'.$row['titulo_esp'].'</td>'; 

					 $html.= '<td>'.$row['titulo_eng'].'</td>'; 

					 $html.= '<td>'.$row['descripcion_esp'].'</td>'; 

					 $html.= '<td>'.$row['descripcion_eng'].'</td>'; 

					 $html.= '<td><a href="'.CONF_SITE_URL."upload/publicaciones/".$row['archivo'].'">Archivo</a> </td>'; 

					 $html.= '<td>'.$row['activo'].'</td>'; 

					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 

					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 

					 if($nivel_acceso['modificacion'] == 'S') 

						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 

					 if($nivel_acceso['baja'] == 'S') 

						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 

					 $html.= '</td>'; 

				 $html.= '</tr>'; 

			 } 

			 $html.= '</table>'; 

			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 

			 $pagerCtrl->useJavaScript(true); 

			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 

			 $pagerCtrl->set_currentPage($pagina_actual); 

			 $html .= $pagerCtrl->get_control(); 

			 } else { 

				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 

			 } 

			 die($html); 

			 break; 

 	 } 

 

} 

 ?> 