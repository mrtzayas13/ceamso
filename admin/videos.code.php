<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('videos'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $videos  = new videos(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $videos->set_id(null); 
				 $videos->set_youtube_url( $_POST['youtube_url']); 
				 $videos->set_titulo_esp( $_POST['titulo_esp']); 
				 $videos->set_titulo_eng( $_POST['titulo_eng']); 
				 $videos->set_descripcion_esp( $_POST['descripcion_esp']); 
				 $videos->set_descripcion_eng( $_POST['descripcion_eng']); 
				 $videos->set_area_id( $_POST['area_id']); 
				 $videos->set_activo( $_POST['activo']); 
				 $videos->set_fecha_creacion( $_POST['fecha_creacion']); 
				 $videos->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $videos->carga($_POST['id']); 
				 $videos->set_id($_POST['id']); 
				 $videos->set_youtube_url($_POST['youtube_url']); 
				 $videos->set_titulo_esp($_POST['titulo_esp']); 
				 $videos->set_titulo_eng($_POST['titulo_eng']); 
				 $videos->set_descripcion_esp($_POST['descripcion_esp']); 
				 $videos->set_descripcion_eng($_POST['descripcion_eng']); 
				 $videos->set_area_id($_POST['area_id']); 
				 $videos->set_activo($_POST['activo']); 
				 $videos->set_fecha_creacion($_POST['fecha_creacion']); 
				 if(!$videos->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $videos->carga($_POST['id']); 
			 $json_arr = array( 'id' => $videos->get_id(),'youtube_url' => $videos->get_youtube_url(),'titulo_esp' => $videos->get_titulo_esp(),'titulo_eng' => $videos->get_titulo_eng(),'descripcion_esp' => $videos->get_descripcion_esp(),'descripcion_eng' => $videos->get_descripcion_eng(),'area_id' => $videos->get_area_id(),'activo' => $videos->get_activo(),'fecha_creacion' => (($videos->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($videos->get_fecha_creacion())) : '') ); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $videos->set_id($_POST['id']); 
 				 if(!$videos->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden'=> ' 1 desc'); if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = videos::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id</th>'; 
				 $html.= '<th>Youtube url</th>'; 
				 $html.= '<th>Titulo esp</th>'; 
				 $html.= '<th>Titulo eng</th>'; 
				 $html.= '<th>Area id</th>'; 
				 $html.= '<th>Activo</th>'; 
				 $html.= '<th>Fecha creacion</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id'].'</td>'; 
					 $html.= '<td>'.$row['youtube_url'].'</td>'; 
					 $html.= '<td>'.$row['titulo_esp'].'</td>'; 
					 $html.= '<td>'.$row['titulo_eng'].'</td>'; 
					 $html.= '<td>'.$row['areas_area_esp'].'</td>'; 
					 $html.= '<td>'.$row['activo'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 