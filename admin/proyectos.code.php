<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('proyectos'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) {

	 $proyectos  = new proyectos(); 

	 $ods_proyectos  = new ods_proyectos();
	 $dbmanager = new DBManager(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{
					$proyectos->set_id(null); 
					$proyectos->set_area_id( $_POST['area_id']); 
					$proyectos->set_tipo( $_POST['tipo']); 
					$proyectos->set_titulo_esp( $_POST['titulo_esp']); 
					$proyectos->set_titulo_eng( $_POST['titulo_eng']); 
					$proyectos->set_copete_esp( $_POST['copete_esp']); 
					$proyectos->set_copete_eng( $_POST['copete_eng']); 
					$proyectos->set_descripcion_esp( $_POST['descripcion_esp']); 
					$proyectos->set_descripcion_eng( $_POST['descripcion_eng']); 
					$proyectos->set_fecha_inicio( $_POST['fecha_inicio']); 
					$proyectos->set_fecha_fin( $_POST['fecha_fin']); 
					$proyectos->set_activo( $_POST['activo']); 
					$proyectos->set_fecha_creacion( $_POST['fecha_creacion']); 
					$proyectos->set_categoria( $_POST['categoria']); 
					$proyectos->guarda();
					
					$filas_afectadas = array();
					$filas_afectadas = $dbmanager->executeQuery(new DBQuery('SELECT * FROM proyectos ORDER BY id DESC LIMIT 1'));
					$fila_id = $filas_afectadas[0]['id'];

					foreach($_POST['ods'] as $ods_row):
						if($ods_row){
							$ods_proyectos->set_id_proyectos($fila_id);
							$ods_proyectos->set_id_objetivo($ods_row);
							$ods_proyectos->guarda();
						}
					endforeach;
				}catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
			
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $proyectos->carga($_POST['id']); 
				 $proyectos->set_id($_POST['id']); 
				 $proyectos->set_area_id($_POST['area_id']); 
				 $proyectos->set_tipo($_POST['tipo']); 
				 $proyectos->set_titulo_esp($_POST['titulo_esp']); 
				 $proyectos->set_titulo_eng($_POST['titulo_eng']); 
				 $proyectos->set_copete_esp($_POST['copete_esp']); 
				 $proyectos->set_copete_eng($_POST['copete_eng']); 
				 $proyectos->set_descripcion_esp($_POST['descripcion_esp']); 
				 $proyectos->set_descripcion_eng($_POST['descripcion_eng']); 
				 $proyectos->set_fecha_inicio($_POST['fecha_inicio']); 
				 $proyectos->set_fecha_fin($_POST['fecha_fin']); 
				 $proyectos->set_activo($_POST['activo']); 
				 $proyectos->set_fecha_creacion($_POST['fecha_creacion']);
				 $proyectos->set_categoria($_POST['categoria']); 
				 
				 //Actualiza multiselect
				 $ods_proyectos->set_id_proyectos($_POST['id']);
				 
				 if(!$ods_proyectos->elimina()) { die('Ocurrio un error al actualizar el registro.'); }
				 else{
					foreach($_POST['ods'] as $ods_row):
						if($ods_row){
							$ods_proyectos->set_id_proyectos($_POST['id']);
							$ods_proyectos->set_id_objetivo($ods_row);
							$ods_proyectos->guarda();
						}
					endforeach;
				 }

				 if(!$proyectos->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $proyectos->carga($_POST['id']); 
			 $json_arr = array( 'id' => $proyectos->get_id(),'area_id' => $proyectos->get_area_id(),'tipo' => $proyectos->get_tipo(),'titulo_esp' => $proyectos->get_titulo_esp(),'titulo_eng' => $proyectos->get_titulo_eng(),'copete_esp' => $proyectos->get_copete_esp(),'copete_eng' => $proyectos->get_copete_eng(),'descripcion_esp' => $proyectos->get_descripcion_esp(),'descripcion_eng' => $proyectos->get_descripcion_eng(),'fecha_inicio' => (($proyectos->get_fecha_inicio() != null) ? date('d/m/Y H:i', strtotime($proyectos->get_fecha_inicio())) : '') ,'fecha_fin' => (($proyectos->get_fecha_fin() != null) ? date('d/m/Y H:i', strtotime($proyectos->get_fecha_fin())) : ''),'categoria' => $proyectos->get_categoria(),'activo' => $proyectos->get_activo(),'fecha_creacion' => (($proyectos->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($proyectos->get_fecha_creacion())) : '') ); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				  
				 $proyectos->set_id($_POST['id']);

				 if(!$proyectos->elimina()){ 
					 die('Ocurrio un error al eliminar el registro.'); 
				 }
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden'=> ' 1 desc'); if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = proyectos::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id</th>'; 
				 $html.= '<th>Area id</th>'; 
				 $html.= '<th>Tipo</th>'; 
				 $html.= '<th>Titulo esp</th>'; 
				 $html.= '<th>Categoria</th>'; 
				 $html.= '<th>Fecha inicio</th>'; 
				 $html.= '<th>Fecha fin</th>'; 
				 $html.= '<th>Activo</th>'; 
				 $html.= '<th>Fecha creacion</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id'].'</td>'; 
					 $html.= '<td>'.$row['areas_area_esp'].'</td>'; 
					 $html.= '<td>'.$row['tipo'].'</td>'; 
					 $html.= '<td>'.$row['titulo_esp'].'</td>'; 
					 $html.= '<td>'.$row['categoria'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_inicio'] != '') ? date('d/m/Y', strtotime($row['fecha_inicio'])) : '') . '</td>'; 
					 $html.= '<td>' . (($row['fecha_fin'] != '') ? date('d/m/Y', strtotime($row['fecha_fin'])) : '') . '</td>'; 
					 $html.= '<td>'.$row['activo'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 $nivel_acceso_subform = $usuario_logueado->recupera_permisos('galeria_proyectos'); 
					 if( ($nivel_acceso_subform['alta']=='S') || ($nivel_acceso_subform['baja']=='S') || ($nivel_acceso_subform['modificacion'] == 'S') || ($nivel_acceso_subform['consulta'] == 'S') ) 
						 $html.= '<a href="galeria_proyectos.php?id=' . $row['id'] . '" title=\'galeria proyectos\' ><img style=\'vertical-align:middle;\' src=\'images/icons/buttons/cog.png\' alt=\'galeria proyectos\' /></a> '; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 