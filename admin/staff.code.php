<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('staff'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $staff  = new staff(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $staff->set_id(null); 
				 $staff->upload_imagen($_FILES['file_imagen']); 
				 $staff->set_nombre_apellido( $_POST['nombre_apellido']); 
				 $staff->set_cargo( $_POST['cargo']); 
				 $staff->set_grupo( $_POST['grupo']); 
				 $staff->set_descripcion( $_POST['descripcion']); 
				 $staff->set_activo( $_POST['activo']); 
				 $staff->set_fecha_creacion( $_POST['fecha_creacion']); 
				 $staff->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $staff->carga($_POST['id']); 
				 $staff->set_id($_POST['id']); 
				 $staff->upload_imagen($_FILES['file_imagen']); 
				 $staff->set_imagen($_POST['imagen']); 
				 $staff->upload_imagen($_FILES['file_imagen']); 
				 $staff->set_nombre_apellido($_POST['nombre_apellido']); 
				 $staff->set_cargo($_POST['cargo']); 
				 $staff->set_grupo($_POST['grupo']); 
				 $staff->set_descripcion($_POST['descripcion']); 
				 $staff->set_activo($_POST['activo']); 
				 $staff->set_fecha_creacion($_POST['fecha_creacion']); 
				 if(!$staff->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $staff->carga($_POST['id']); 
			 $json_arr = array( 'id' => $staff->get_id(),'imagen' => $staff->get_imagen(),'nombre_apellido' => $staff->get_nombre_apellido(),'cargo' => $staff->get_cargo(),'grupo' => $staff->get_grupo(),'descripcion' => $staff->get_descripcion(),'activo' => $staff->get_activo(),'fecha_creacion' => (($staff->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($staff->get_fecha_creacion())) : '') ); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $staff->set_id($_POST['id']); 
 				 if(!$staff->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => ''); 
			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = staff::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id</th>'; 
				 $html.= '<th>Imagen</th>'; 
				 $html.= '<th>Nombre apellido</th>'; 
				 $html.= '<th>Cargo</th>'; 
				 $html.= '<th>Grupo</th>';  
				 $html.= '<th>Activo</th>'; 
				 $html.= '<th>Fecha creacion</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id'].'</td>'; 
					  $html.= '<td><img src="'.CONF_SITE_URL."upload/staff/".$row['imagen'].'" height="100"/> </td>'; 
					 $html.= '<td>'.$row['nombre_apellido'].'</td>'; 
					 $html.= '<td>'.$row['cargo'].'</td>'; 
					 $html.= '<td>'.$row['grupo'].'</td>'; 
					 $html.= '<td>'.$row['activo'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 