<?php
session_start();
include_once '../inc/config.inc.php';
include_once 'inc/validate-authetication.inc.php';
include_once 'inc/validate-request.inc.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

    <head>

        <title><?php echo CONF_SITE_TITLE; ?></title>       
        <?php include 'inc/head.inc.php'; ?>

    </head>

    <body>
        
        <div id="body-wrapper"> 
            
            <?php include "inc/sidebar.tpl.php"; ?>
            
            <div id="main-content"> <!-- Main Content Section with everything -->
			
                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
			</div>
                    </div>
		</noscript>
                
                <h2>Dashboard</h2>
                <!-- <p id="page-intro">Breve descripción del formulario</p> -->
                
                <div class="clear"></div> <!-- End .clear -->
			
                <div class="content-box"><!-- Start Content Box -->
				
                    <div class="content-box-header">
					
                        <h3>Bienvenido</h3>
								
                        <div class="clear"></div>
                        
                    </div> <!-- End .content-box-header -->
                        
                    <div class="content-box-content">
                            
                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_mensajes" class="notification png_bg" style="display:none;"></div>
                        
                        <!-- #BLOQUE OBLIGATORIO --> 
                        
						
					
                    </div> <!-- End .content-box-header -->
                    
                </div>
                
                <div id="footer">
                    <small>
                        <!-- &#169; Copyright <?php echo date("Y"); ?> Nombre Empresa | --> | <a href="#">Top</a>
                    </small>
                </div><!-- End #footer -->
            
            </div>
            
        </div>

        <script language="javascript" type="text/javascript">
                      
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $.timepicker.setDefaults($.timepicker.regional['es']);
            $('.datepicker').datetimepicker({
                dateFormat: 'dd/mm/yy',
				timeFormat: 'hh:mm'
            });          
            
        </script>

    </body>

</html>