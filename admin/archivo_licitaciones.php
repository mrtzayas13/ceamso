<?php
session_start();
include_once '../inc/config.inc.php';
include_once 'inc/validate-authetication.inc.php';

$codigo_seccion_administrable = 'archivo_licitaciones';
$nivel_acceso = $usuario_logueado->recupera_permisos($codigo_seccion_administrable);
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') )
    header('location: index.php');
$licitaciones = new licitaciones();
if(!$licitaciones->carga($_GET['id'])){ die('No se recibieron todos los parametros esperados.'); }


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

    <head>

        <title><?php echo CONF_SITE_TITLE; ?></title>       
        <?php include 'inc/head.inc.php'; ?>

    </head>

    <body>

        <div id="body-wrapper"> 

            <?php include "inc/sidebar.tpl.php"; ?>

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <h2>Administración de Archivo licitaciones</h2>
                <p id="page-intro"><u>Licitaciones</u>: <?php echo $licitaciones->get_codigo(); ?><br /></p>

                <div class="clear"></div> <!-- End .clear -->

                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>Listado de Archivo licitaciones</h3>

                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_mensajes" class="notification png_bg" style="display:none;"></div>

                        <div class="button_row">

                            <a href="licitaciones.php" id="btn_ir_licitaciones" class="btn_volver">« ir a Licitaciones</a> 
                            <?php if($nivel_acceso['alta'] == 'S'): ?>
                            <button id="btn_nuevo">Nuevo</button>
                            <?php endif; ?>

                            <div style="float:right;">
                                <form name="frm_busqueda" id="frm_busqueda" action="" method="post">
                                    <input type="text" name="txt_busqueda" id="txt_busqueda" class="text-input" /> 
                                    <input type="hidden" name="hdn_busqueda" id="hdn_busqueda" />
                                    <button id="btn_busqueda">Buscar</button>
                                    <!-- <img src="<?php echo CONF_ADMIN_URL; ?>images/icons/zoom.png" /> -->
                                </form>
                            </div>

                        </div>

                        <div id="dv_filtro_aplicado" style="display: none;" class="notification information png_bg"></div>

                        <div id="dv_grilla"></div>

                        <!-- #BLOQUE OBLIGATORIO --> 

                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_formulario" title="edicion" style='display: none;'>

                            <form name='frm_datos' id="frm_datos" action="" method="post" enctype="multipart/formdata">
                                
                                <div id="dv_errores" class="notification png_bg" style="display:none;"></div>
                                
                                <input type="hidden" name='accion' id='accion' value='' />

				 <input type='hidden' name='licitacion_id' id='licitacion_id' value="<?php echo $_GET['id'] ?>" />
				 <input type='hidden' name='id' id='id' />

				 <table cellspacing='0' cellpadding='3'>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Nombre esp</td>
						 <td>
							 <input type='text' maxlength='500' name='nombre_esp' id='nombre_esp' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Nombre eng</td>
						 <td>
							 <input type='text' maxlength='500' name='nombre_eng' id='nombre_eng' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Archivo</td>
						 <td>
							 <input type='file' name='file_archivo' id='file_archivo' />
							 <div id='pnl_archivo' style='display:none;' class='file-info'>
								 <input type='checkbox' name='archivo' id='archivo' /> Mantener el archivo.<br />
 								 <span id='lbl_archivo'></span>
							 </div>
						 </td>
					 </tr>
					
				 </table>

                                <div class='button_row' style="text-align: center;">
                                    <input type="submit" name="btn_guarda" id="btn_guarda" class="button" value="Guardar" />
                                </div>

                            </form>

                        </div>

                        <!-- #BLOQUE OBLIGATORIO --> 

                    </div> <!-- End .content-box-header -->

                </div>

                <div id="footer">
                    <small>
                        <!-- &#169; Copyright <?php echo date("Y"); ?> Nombre Empresa | --> | <a href="#">Top</a>
                    </small>
                </div><!-- End #footer -->

            </div>

        </div>

        <script language="javascript" type="text/javascript">
            
            var code_file = 'archivo_licitaciones.code.php';
            
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $.timepicker.setDefaults($.timepicker.regional['es']);
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });
            $('.datetimepicker').datetimepicker({
                dateFormat: 'dd/mm/yy',
                timeFormat: 'hh:mm'
            });
            

			
            var pagina_actual = 1;
                       
            $("#btn_guarda, #btn_nuevo, .btn_volver").button();
            $("#btn_busqueda").button({icons:{ primary: "ui-icon-search" }});
            $("#frm_busqueda").submit(function(evento){ 
                evento.preventDefault();
                $('#hdn_busqueda').val($('#txt_busqueda').val()); 
                if($('#txt_busqueda').val() != ''){
                    $('#dv_filtro_aplicado').show();
                    $('#dv_filtro_aplicado').html('<div>Filtro aplicado: <strong>"' + $('#txt_busqueda').val() + '"</strong><a class="close" href="javascript:;" onclick="eliminaFiltro()"><img alt="Cerrar" title="Quitar filtro" src="<?php echo CONF_ADMIN_URL; ?>images/icons/cross_grey_small.png" /></a></div>');
                }else{
                    $('#dv_filtro_aplicado').hide();
                }
                cargaGrilla(1);
            });
            
            function eliminaFiltro(){
                $('#hdn_busqueda').val('');
                $('#txt_busqueda').val('');
                $('#dv_filtro_aplicado').effect('blind',{},500);
                cargaGrilla();
            }
            
            $("#dv_formulario").dialog({
                modal: true,
                autoOpen: false,
                width: 500,
                open: function(event, ui) { 
                    $('a[title]').qtip();
                    $("#dv_errores").hide();

                }
            });
            
            $("#frm_datos").validate({
                errorElement: "span"
		, rules: { 
			 fecha_creacion: {  required: true  }  
		 }
            });
            
            <?php if($nivel_acceso['alta'] == 'S'): ?>
            $("#btn_nuevo").click(function(){
                
                $("#accion").val("inserta");
                
		$('#id').val(''); 
		$('#nombre_esp').val(''); 
		$('#nombre_eng').val(''); 
			$('#archivo').removeAttr('checked'); 
			$('#pnl_archivo').hide(); 
			$('#lbl_archivo').html(''); 
		$('#file_archivo').replaceWith("<input type='file' name='file_archivo' id='file_archivo' />"); 
		$('#archivo').val(''); 
		$('#fecha_creacion').val(''); 


                $("#dv_formulario").dialog({ title: 'Insertar registro' });
                $("#dv_formulario").dialog("open");
                
            });
            <?php endif ?>
            
            <?php if( ($nivel_acceso['alta'] == 'S') || ($nivel_acceso['modificacion'] == 'S')): ?>
            $("#frm_datos").submit(function(evento){
                
                evento.preventDefault();
                
                $("#dv_errores").hide();
                
                if($(this).valid()){
                    $(this).ajaxSubmit({
                        type: 'POST',
                        url: code_file,
                        success: function(mensaje){
                            if(mensaje == ""){
                                $("#dv_mensajes").html("<div>Sus datos se actualizaron correctamente.</div>");
                                $("#dv_mensajes").addClass("success").removeClass("error");
                                $("#dv_mensajes").show("fade", null, 1000);
                                $("#dv_formulario").dialog("close");
                                cargaGrilla(pagina_actual);
                            }else{
                                $("#dv_errores").html("<div>" + mensaje + "</div>");
                                $("#dv_errores").addClass("error").removeClass("success");
                                $("#dv_errores").show("fade", null, 1000);
                            }
                            
                        }
                    });
                }
            });
            <?php endif; ?>
            
            function cargaGrilla(p_pagina){
                $.ajax({
                    type: 'POST',
                    url: code_file,
                    data: { accion: 'lista', pagina: p_pagina, buscar: $('#hdn_busqueda').val()  <?php if(isset($_GET['id']) && !empty($_GET['id'])){ echo ', id: ' . $_GET['id']; } ?> },
                    success: function(p_html){
						pagina_actual = p_pagina;
                        $('#dv_grilla').html(p_html);
						$('a[title]').qtip();
                    }
                });
            }
            
            <?php if($nivel_acceso['baja'] == 'S'): ?>
            function eliminaRegistro(p_id){
                if(confirm("Esta seguro que quiere eliminar el registro?")){
                    $.ajax({
                        type: 'POST',
                        url: code_file,
                        data: { accion: 'elimina', id: p_id },
                        success: function(p_mensaje){
                            if(p_mensaje == ""){
                                $("#dv_mensajes").html("<div>Sus datos se actualizaron correctamente.</div>");
                                $("#dv_mensajes").addClass("success").removeClass("error");
                                cargaGrilla(pagina_actual);
                            }else{
                                $("#dv_mensajes").html("<div>" + p_mensaje + "</div>");
                                $("#dv_mensajes").addClass("error").removeClass("success");
                            }
                            $("#dv_mensajes").show("fade", null, 1000);
                        }
                    });
                }
            }
            <?php endif; ?>
            
            <?php if($nivel_acceso['modificacion'] == 'S'): ?>
            function editaRegistro(p_id){
                $("#accion").val("actualiza");
                $.ajax({
                    type: 'POST',
                    url: code_file,
                    data: { accion: 'edita', id: p_id },
                    dataType: 'json',
                    success: function(datos){
                        
			$('#id').val(datos.id); 
			$('#nombre_esp').val(datos.nombre_esp); 
			$('#nombre_eng').val(datos.nombre_eng); 
			$('#file_archivo').replaceWith("<input type='file' name='file_archivo' id='file_archivo' />"); 
				if((datos.archivo != '') && (datos.archivo != null)) { 
					$('#archivo').attr('checked',true); 
					$('#pnl_archivo').show(); 
					$('#lbl_archivo').html('<a href="<?php echo CONF_UPLOAD_PATH ?>archivo_licitaciones/' + datos.archivo + '" target="_blank">' + datos.archivo + '</a>'); 
				}else{
 					$('#archivo').removeAttr('checked'); 
					$('#pnl_archivo').hide(); 
					$('#lbl_archivo').html(''); 
				} 
			$('#archivo').val(datos.archivo); 
			$('#fecha_creacion').val(datos.fecha_creacion); 

                        
                        $("#dv_formulario").dialog({ title: 'Edita registro' });
                        $("#dv_formulario").dialog("open");
                    }
                });
            }
            <?php endif; ?>
            
            $(document).ready(function(){
                cargaGrilla();
                $('a[title]').qtip();
            });
           
            
        </script>

    </body>

</html>