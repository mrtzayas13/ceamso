<?php
session_start();
include_once '../inc/config.inc.php';
include_once 'inc/validate-authetication.inc.php';

$codigo_seccion_administrable = 'newsletter';
$nivel_acceso = $usuario_logueado->recupera_permisos($codigo_seccion_administrable);
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') )
    header('location: index.php');

// load library
require CONF_ABS_ROOT_PATH . "classes/helpers/php-excel.class.php";

$opciones = array('orden' => 'nombre ASC', 'reg_x_pag'=> 100000000000000000); 

$data = array(1 => array ('Nombre','Email', 'Recibir LLamados', 'Recibir novedades'));

$arr = newsletter::lista($opciones); 

foreach($arr['datos']  as $row)
{
	$data[] = array($row['nombre'],$row['email'],(( $row['recibir_llamados'] == 1) ? 'SI' : 'NO'),(( $row['recibir_novedades'] == 1) ? 'SI' : 'NO')) ;	
}

// generate file (constructor parameters are optional)
$xls = new Excel_XML('UTF-8', false, 'Ordenes');
$xls->addArray($data);
$xls->generateXML('newsletter-'.date('d-m-Y'));

?>