<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

        <h1 id="sidebar-title"><a href="#"><?php echo CONF_SITE_TITLE; ?></a></h1>

        <!-- Logo (221px) -->
        <a href="#"><img id="logo" src="<?php echo CONF_ADMIN_URL; ?>images/logo.png" alt="Simpla Admin logo" /></a>

        <div id="profile-links">
            Bienvenido, <?php echo $usuario_logueado->get_usuario() ?></a><br />
            <br />
            <a href="<?php echo CONF_SITE_URL; ?>" title="Ir al sitio">Ir al sitio</a> | <a href="<?php echo CONF_ADMIN_URL; ?>logout.php" title="Cerrar sesión">Salir</a>
        </div>

        <ul id="main-nav">  <!-- Accordion Menu -->

            <li>
                <a href="<?php echo CONF_ADMIN_URL; ?>" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                    Dashboard
                </a>       
            </li>

            <?php
            $js_current_menu = '';
            $arr_secciones = $usuario_logueado->recupera_secciones();
            $grupo_actual = '';
            $numero_grupo = 0;
            foreach ($arr_secciones as $seccion):
                if ($seccion['formulario_principal'] == 'S'):
                    if ($grupo_actual != $seccion['grupo']):
                        if ($grupo_actual != ''):
                            echo "</ul></li>";
                            $numero_grupo++;
                        endif;
                        echo "<li>";
                        echo "<a href='#' class='nav-top-item' id='grupo_{$numero_grupo}'>{$seccion['grupo']}</a>";
                        echo "<ul>";
                        $grupo_actual = $seccion['grupo'];
                    endif;
                    if (isset($codigo_seccion_administrable) && ($codigo_seccion_administrable == $seccion['codigo_seccion_administrable'])) {
                        $js_current_menu = '<script language="javascript">$("#grupo_' . $numero_grupo . '").addClass("current");</script>';
                        echo "<li><a id='link_{$seccion['codigo_seccion_administrable']}' class='current' href='" . CONF_ADMIN_URL . $seccion['url'] . "'>{$seccion['seccion_administrable']}</a></li>";
                    } else {
                        echo "<li><a id='link_{$seccion['codigo_seccion_administrable']}' href='" . CONF_ADMIN_URL . $seccion['url'] . "'>{$seccion['seccion_administrable']}</a></li>";
                    }
                endif;
            endforeach;
            ?>
        </ul>
        <?php echo $js_current_menu; ?>
    </div></div>