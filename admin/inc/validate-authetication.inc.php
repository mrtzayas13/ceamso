<?php
$usuario_logueado = new usuarios();

if(!isset($_SESSION['s_id_usuario'])){
    header("location: login.php");
}else{
    if(!$usuario_logueado->carga($_SESSION['s_id_usuario'])){
        header("location: login.php");
    }
}
?>
