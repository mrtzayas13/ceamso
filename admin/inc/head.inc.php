<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

<!-- ENLACES A ESTILOS -->

<link rel="shortcut icon" href="<?php echo CONF_ADMIN_URL; ?>favicon.gif" type="image/gif"/>

<!-- Estilos del tema -->
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/jquery.qtip.min.css" type="text/css" media="screen" />

<!-- Estilo multiselect -->
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/chosen.css">

<!-- Esquemas de color
El color por defecto es verde, descomenta una de las linea de abajo para cambiar.
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/blue.css" type="text/css" media="screen" />
 
-->
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/green.css" type="text/css" media="screen" /> 
<!-- Internet Explorer Fixes Stylesheet -->
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/ie.css" type="text/css" media="screen" />
<![endif]-->
<!-- jquery ui -->
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/jquery-ui-1.8.16.custom.css" type="text/css" media="screen" title="no title" charset="utf-8" />

<!-- estilos personalizados -->
<link rel="stylesheet" href="<?php echo CONF_ADMIN_URL; ?>css/custom.css" type="text/css" media="screen" />

<!-- SCRIPTS -->

<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery-1.7.min.js"></script>
<!-- jquery ui -->
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery.ui.datepicker-es.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery-ui-timepicker-es.js"></script>
<!-- jquery - utilidades para formularios -->
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery.validate.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery-form.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/blockUI.js"></script>
<!-- tooltip -->
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery.qtip.min.js"></script>
<!-- ckeditor -->
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/ckeditor/ckeditor.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/ckeditor/adapters/jquery.js"></script>
<!-- theme scripts -->
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/DD_belatedPNG_0.0.7a.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/facebox.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/jquery.wysiwyg.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo CONF_ADMIN_URL; ?>js/theme.jquery.configuration.js"></script>
<!-- map scripts -->
<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false'></script>
<script src="<?php echo CONF_ADMIN_URL; ?>js/jquery.gmap.js"></script>

<!-- multi select -->
<script src="<?php echo CONF_ADMIN_URL; ?>js/chosen.jquery.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">

    // Ajax activity indicator bound to ajax start/stop document events
    $(document).ajaxStart(function(){ 
        $.blockUI({ message: '<div style="padding:25px;">Procesando, por favor aguarde...</div>' });
    }).ajaxStop(function(){ 
        $.unblockUI();
    }).ajaxError(function(event, request, settings) {
        $.unblockUI();
        var mensaje_error = "No se ha podido establecer una conexión con el servidor.\nPor favor aguarde un momento y vuelva a intentarlo.";
        mensaje_error += "<br />" + request.responseText;
        $("#dv_mensajes").removeClass("attention information success error").addClass("error");
        $("#dv_mensajes").html(mensaje_error);
        $("#dv_mensajes").effect( "fade", { }, 500);
    });
    
</script>
