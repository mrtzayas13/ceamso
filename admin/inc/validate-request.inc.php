<?php
$RequestMethods = array("COPY", "DELETE", "HEAD", "MKCOL", "MOVE", "OPTIONS", "POST", "PUT", "PROFIND", "TRACE");
if (in_array($_SERVER['REQUEST_METHOD'], $RequestMethods)):
    $servername = explode("/", $_SERVER['HTTP_REFERER']);
    
    $servername = (strpos($servername[2], ':') !== false) ? substr($servername[2],0, strpos($servername[2],':')) : $servername[2];
    if ($servername != $_SERVER['SERVER_NAME']):
        header("HTTP/1.0 401 Unauthorized");
        die('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN"><html><head><title>Invalid Request</title></head><body><h1>Invalid Request</h1></body></html>');
    endif;
endif;
?>
