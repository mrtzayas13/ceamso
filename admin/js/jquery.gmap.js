(function($) {

    var methods = {
        
        init : function( options ) {
            
            return this.each( function(){
             
                var marcador = null;
                var latlng = null;
                var mapa = null;
                var opc = $.extend({}, $.fn.gmap.opc_default, options);

                var elemento = $(this);
                
                $(this).css('width', opc.width);
                $(this).css('height', opc.height);
            
                latlng = new google.maps.LatLng(opc.position.lat, opc.position.lng);
                var myOptions = {
                    zoom: opc.zoom, 
                    center: latlng,
                    mapTypeControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP 
                }
                
                mapa = new google.maps.Map(this, myOptions);
            
                if(opc.editable == true){
                    google.maps.event.addListener(mapa, 'click', function(event) {
                        opc = elemento.data('opciones');
                        marcador = elemento.data('marcador');
                        if(marcador != null){
                            marcador.setMap(null); 
                        }
                        marcador = new google.maps.Marker({ 
                            position: event.latLng,
                            map: mapa
                        }); 
                        
                        if(opc.controls.position != '') 
                            $('#' + opc.controls.position).val(String(event.latLng.lat()) + '|' + String(event.latLng.lng())); 
                        if(opc.controls.lat != '')
                            $('#' + opc.controls.lat).val(String(event.latLng.lat())); 
                        if(opc.controls.lng != '')
                            $('#' + opc.controls.lng).val(String(event.latLng.lng())); 
                        
                        elemento.data('marcador', marcador);
                        elemento.data('latlng', latlng);
                  
                        
                    });
                }
                
                $(this).data('mapa', mapa);
                $(this).data('opciones', opc);
                $(this).data('latlng', latlng);
                
            });
        },
        
        resize: function (){
            return this.each( function(){
                var mapa = $(this).data('mapa');
                var latlng = $(this).data('latlng');
                google.maps.event.trigger(mapa, 'resize'); 
                mapa.setCenter(latlng); 
            });
        },
        
        refresh: function(){
            
            return this.each( function(){
                
                var marcador = $(this).data('marcador');
                var mapa = $(this).data('mapa');
                var opc = $(this).data('opciones');
                var latlng = $(this).data('latlng');
                
                var posicion_definida = false;
                
                if(marcador != null){
                    marcador.setMap(null); 
                }
                var re = new RegExp(/^(\-?\d*\.\d+|\d+)$/);
                if(opc.controls.position != '') {
                    var arr_latlng = String($('#' + opc.controls.position).val()).split('|');
                    if(arr_latlng.length == 2){
                        if(re.test(arr_latlng[0]) && re.test(arr_latlng[1])){
                            latlng =  new google.maps.LatLng(arr_latlng[0], arr_latlng[1]);
                            posicion_definida = true;
                        }
                    }
                }else{
                    if((opc.controls.lat != '' && re.test($('#' + opc.controls.lat).val())) && (opc.controls.lng != '' && re.test($('#' + opc.controls.lng).val()))){
                        latlng =  new google.maps.LatLng($('#' + opc.controls.lat).val(), $('#' + opc.controls.lng).val());
                        posicion_definida = true;
                    }
                }
                if(posicion_definida){
                    marcador = new google.maps.Marker({ 
                        position: latlng,
                        map: mapa
                    });
                    $(this).data('marcador', marcador);
                }else{
                    latlng =  new google.maps.LatLng( $.fn.gmap.opc_default.position.lat, $.fn.gmap.opc_default.position.lng);
                }
                mapa.setCenter(latlng);
            });
        }
    
    };
    
    $.fn.gmap = function(method){
 
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Metodo ' +  method + ' no existe en jQuery.gmap' );
        }  

    }
    
    $.fn.gmap.opc_default = {
        position: {
            lat: -25.29151736802436,
            lng: -57.60077879409789
        },
        editable: true,
        multiple: false,
        width : 300,
        height : 200,
        zoom: 16,
        controls: {
            position: '',
            lat: '',
            lng: ''
        }
    };
    
})(jQuery);