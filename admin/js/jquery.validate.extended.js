jQuery.validator.addMethod("time", function(value, element) { 
    return this.optional(element) || /([01]?[0-9]|2[0-3]):[0-5][0-9]/.test(value); 
}, "Ingrese una hora válida");