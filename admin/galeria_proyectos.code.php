<?php 

 session_start(); 

include_once '../inc/config.inc.php'; 

include_once 'inc/validate-authetication.inc.php'; 

include_once 'inc/validate-request.inc.php'; 



/* Chequeo de permisos de usuario */

if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 

else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 

$nivel_acceso = $usuario_logueado->recupera_permisos('galeria_proyectos'); 

if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 

	die('Acceso no permitido');



/* Acciones a realizar */

if (isset($_POST['accion'])) { 



	 $galeria_proyectos  = new galeria_proyectos(); 



	 switch($_POST['accion']) { 



		 case 'inserta': 

 			 if($nivel_acceso['alta'] == 'S'){ 

				 try{ 

				 $galeria_proyectos->set_id(null); 

				 $galeria_proyectos->upload_imagen($_FILES['file_imagen']); 

				 $galeria_proyectos->set_fecha_creacion( $_POST['fecha_creacion']); 

				 $galeria_proyectos->set_proyecto_id( $_POST['proyecto_id']); 

				 $galeria_proyectos->guarda(); 

				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 

 			 } 

 			 break; 

 

		 case 'actualiza': 

 			 if($nivel_acceso['modificacion'] == 'S'){ 

				 try{ 

				 $galeria_proyectos->carga($_POST['id']); 

				 $galeria_proyectos->set_id($_POST['id']); 

				 $galeria_proyectos->upload_imagen($_FILES['file_imagen']); 

				 $galeria_proyectos->set_imagen($_POST['imagen']); 

				 $galeria_proyectos->upload_imagen($_FILES['file_imagen']); 

				 $galeria_proyectos->set_fecha_creacion($_POST['fecha_creacion']); 

				 $galeria_proyectos->set_proyecto_id($_POST['proyecto_id']); 

				 if(!$galeria_proyectos->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 

				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 

 			 } 

 			 break; 

 

		 case 'edita': 

 			 $galeria_proyectos->carga($_POST['id']); 

			 $json_arr = array( 'id' => $galeria_proyectos->get_id(),'imagen' => $galeria_proyectos->get_imagen(),'fecha_creacion' => (($galeria_proyectos->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($galeria_proyectos->get_fecha_creacion())) : '') ,'proyecto_id' => $galeria_proyectos->get_proyecto_id()); 

 			 die(json_encode($json_arr)); 

 			 break; 

 

		 case 'elimina': 

 			 if($nivel_acceso['baja'] == 'S'){ 

				 $galeria_proyectos->set_id($_POST['id']); 

 				 if(!$galeria_proyectos->elimina()) { die('Ocurrio un error al elimina el registro.'); } 

				 die(''); 

 			 } 

 			 break; 

 

		 case 'lista': 

 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 

 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden'=> ' 1 desc'); if(isset($_POST['id']) && !empty($_POST['id'])) 

				 $opciones['filtro'] .= (($opciones['filtro'] == '')?' WHERE ':' AND ') . sprintf(' galeria_proyectos.proyecto_id = %s ', DBManager::formatSQLValue($_POST['id'])); 

			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 

				 $opciones['buscar'] = $_POST['buscar']; 

			 } 

			 $arr = galeria_proyectos::lista($opciones); 



			 if(count($arr['datos']) > 0) { 

			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 

			 $html.= '<thead><tr>'; 

				 $html.= '<th>Id</th>'; 

				 $html.= '<th>Imagen</th>'; 

				 $html.= '<th>Fecha creacion</th>'; 

				 $html.= '<th>&nbsp;</th>'; 

			 $html.= '</tr></thead>'; 

			 $estilo_fila = ''; 

			 foreach ($arr['datos'] as $row) { 

				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 

				 $html.= '<tr' . $estilo_fila . '>'; 

					 $html.= '<td>'.$row['id'].'</td>'; 

					 $html.= '<td><img src="'.CONF_SITE_URL."upload/galeria_proyectos/".$row['imagen'].'" height="50"/> </td>'; 

					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 

					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 

					 if($nivel_acceso['modificacion'] == 'S') 

						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 

					 if($nivel_acceso['baja'] == 'S') 

						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 

					 $html.= '</td>'; 

				 $html.= '</tr>'; 

			 } 

			 $html.= '</table>'; 

			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 

			 $pagerCtrl->useJavaScript(true); 

			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 

			 $pagerCtrl->set_currentPage($pagina_actual); 

			 $html .= $pagerCtrl->get_control(); 

			 } else { 

				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 

			 } 

			 die($html); 

			 break; 

 	 } 

 

} 

 ?> 