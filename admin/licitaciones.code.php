<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('licitaciones'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $licitaciones  = new licitaciones(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $licitaciones->set_id(null); 
				 $licitaciones->set_codigo( $_POST['codigo']); 
				 $licitaciones->set_titulo_esp( $_POST['titulo_esp']); 
				 $licitaciones->set_titulo_eng( $_POST['titulo_eng']); 
				 $licitaciones->set_descripcion_esp( $_POST['descripcion_esp']); 
				 $licitaciones->set_descripcion_eng( $_POST['descripcion_eng']); 
				 $licitaciones->set_fecha_inicio( $_POST['fecha_inicio']); 
				 $licitaciones->set_fecha_fin( $_POST['fecha_fin']); 
				 $licitaciones->set_estado_licitacion( $_POST['estado_licitacion']); 
				 $licitaciones->set_activo( $_POST['activo']); 
				 $licitaciones->set_fecha_creacion( $_POST['fecha_creacion']); 
				 $licitaciones->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $licitaciones->carga($_POST['id']); 
				 $licitaciones->set_id($_POST['id']); 
				 $licitaciones->set_codigo($_POST['codigo']); 
				 $licitaciones->set_titulo_esp($_POST['titulo_esp']); 
				 $licitaciones->set_titulo_eng($_POST['titulo_eng']); 
				 $licitaciones->set_descripcion_esp($_POST['descripcion_esp']); 
				 $licitaciones->set_descripcion_eng($_POST['descripcion_eng']); 
				 $licitaciones->set_fecha_inicio($_POST['fecha_inicio']); 
				 $licitaciones->set_fecha_fin($_POST['fecha_fin']); 
				 $licitaciones->set_estado_licitacion($_POST['estado_licitacion']); 
				 $licitaciones->set_activo($_POST['activo']); 
				 $licitaciones->set_fecha_creacion($_POST['fecha_creacion']); 
				 if(!$licitaciones->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $licitaciones->carga($_POST['id']); 
			 $json_arr = array( 'id' => $licitaciones->get_id(),'codigo' => $licitaciones->get_codigo(),'titulo_esp' => $licitaciones->get_titulo_esp(),'titulo_eng' => $licitaciones->get_titulo_eng(),'descripcion_esp' => $licitaciones->get_descripcion_esp(),'descripcion_eng' => $licitaciones->get_descripcion_eng(),'fecha_inicio' => (($licitaciones->get_fecha_inicio() != null) ? date('d/m/Y H:i', strtotime($licitaciones->get_fecha_inicio())) : '') ,'fecha_fin' => (($licitaciones->get_fecha_fin() != null) ? date('d/m/Y H:i', strtotime($licitaciones->get_fecha_fin())) : '') ,'estado_licitacion' => $licitaciones->get_estado_licitacion(),'activo' => $licitaciones->get_activo(),'fecha_creacion' => (($licitaciones->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($licitaciones->get_fecha_creacion())) : '') ); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $licitaciones->set_id($_POST['id']); 
 				 if(!$licitaciones->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden'=> ' 1 desc'); if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = licitaciones::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id</th>'; 
				 $html.= '<th>Codigo</th>'; 
				 $html.= '<th>Titulo esp</th>'; 
				 $html.= '<th>Titulo eng</th>'; 
				 $html.= '<th>Fecha inicio</th>'; 
				 $html.= '<th>Fecha fin</th>'; 
				 $html.= '<th>Estado licitacion</th>';
				 $html.= '<th>Activo</th>'; 
				 $html.= '<th>Fecha creacion</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 

			 	$fecha_cierre = strtotime($row['fecha_fin']);
				$fecha_ahora = strtotime(date('Y-m-d h:i:s'));

				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id'].'</td>'; 
					 $html.= '<td>'.$row['codigo'].'</td>'; 
					 $html.= '<td>'.$row['titulo_esp'].'</td>'; 
					 $html.= '<td>'.$row['titulo_eng'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_inicio'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_inicio'])) : '') . '</td>'; 
					 $html.= '<td>' . (($row['fecha_fin'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_fin'])) : '') . '</td>'; 
					 $html.= '<td>'.(($fecha_cierre <= $fecha_ahora) ? 'INACTIVO' : 'ACTIVO').'</td>'; 
					 $html.= '<td>'.$row['activo'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 $nivel_acceso_subform = $usuario_logueado->recupera_permisos('archivo_licitaciones'); 
					 if( ($nivel_acceso_subform['alta']=='S') || ($nivel_acceso_subform['baja']=='S') || ($nivel_acceso_subform['modificacion'] == 'S') || ($nivel_acceso_subform['consulta'] == 'S') ) 
						 $html.= '<a href="archivo_licitaciones.php?id=' . $row['id'] . '" title=\'archivo licitaciones\' ><img style=\'vertical-align:middle;\' src=\'images/icons/buttons/cog.png\' alt=\'archivo licitaciones\' /></a> '; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 