<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('historias_exitos'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $historias_exitos  = new historias_exitos(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $historias_exitos->set_id(null); 
				 $historias_exitos->set_nombre( $_POST['nombre']); 
				 $historias_exitos->set_institucion_esp( $_POST['institucion_esp']); 
				 $historias_exitos->set_institucion_eng( $_POST['institucion_eng']); 
				 $historias_exitos->set_titulo_esp( $_POST['titulo_esp']); 
				 $historias_exitos->set_titulo_eng( $_POST['titulo_eng']); 
				 $historias_exitos->set_cita_esp( $_POST['cita_esp']); 
				 $historias_exitos->set_cita_eng( $_POST['cita_eng']); 
				 $historias_exitos->set_copete_esp( $_POST['copete_esp']); 
				 $historias_exitos->set_copete_eng( $_POST['copete_eng']); 
				 $historias_exitos->set_descripcion_largo_esp( $_POST['descripcion_largo_esp']); 
				 $historias_exitos->set_descripcion_largo_eng( $_POST['descripcion_largo_eng']); 
				 $historias_exitos->upload_imagen($_FILES['file_imagen']); 
				 $historias_exitos->set_activo( $_POST['activo']); 
				 $historias_exitos->set_fecha_creacion( $_POST['fecha_creacion']); 
				 $historias_exitos->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $historias_exitos->carga($_POST['id']); 
				 $historias_exitos->set_id($_POST['id']); 
				 $historias_exitos->set_nombre($_POST['nombre']); 
				 $historias_exitos->set_institucion_esp($_POST['institucion_esp']); 
				 $historias_exitos->set_institucion_eng($_POST['institucion_eng']); 
				 $historias_exitos->set_titulo_esp($_POST['titulo_esp']); 
				 $historias_exitos->set_titulo_eng($_POST['titulo_eng']); 
				 $historias_exitos->set_cita_esp($_POST['cita_esp']); 
				 $historias_exitos->set_cita_eng($_POST['cita_eng']); 
				 $historias_exitos->set_copete_esp($_POST['copete_esp']); 
				 $historias_exitos->set_copete_eng($_POST['copete_eng']); 
				 $historias_exitos->set_descripcion_largo_esp($_POST['descripcion_largo_esp']); 
				 $historias_exitos->set_descripcion_largo_eng($_POST['descripcion_largo_eng']); 
				 $historias_exitos->upload_imagen($_FILES['file_imagen']); 
				 $historias_exitos->set_imagen($_POST['imagen']); 
				 $historias_exitos->upload_imagen($_FILES['file_imagen']); 
				 $historias_exitos->set_activo($_POST['activo']); 
				 $historias_exitos->set_fecha_creacion($_POST['fecha_creacion']); 
				 if(!$historias_exitos->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $historias_exitos->carga($_POST['id']); 
			 $json_arr = array( 'id' => $historias_exitos->get_id(),'nombre' => $historias_exitos->get_nombre(),'institucion_esp' => $historias_exitos->get_institucion_esp(),'institucion_eng' => $historias_exitos->get_institucion_eng(),'titulo_esp' => $historias_exitos->get_titulo_esp(),'titulo_eng' => $historias_exitos->get_titulo_eng(),'cita_esp' => $historias_exitos->get_cita_esp(),'cita_eng' => $historias_exitos->get_cita_eng(),'copete_esp' => $historias_exitos->get_copete_esp(),'copete_eng' => $historias_exitos->get_copete_eng(),'descripcion_largo_esp' => $historias_exitos->get_descripcion_largo_esp(),'descripcion_largo_eng' => $historias_exitos->get_descripcion_largo_eng(),'imagen' => $historias_exitos->get_imagen(),'activo' => $historias_exitos->get_activo(),'fecha_creacion' => (($historias_exitos->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($historias_exitos->get_fecha_creacion())) : '') ); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $historias_exitos->set_id($_POST['id']); 
 				 if(!$historias_exitos->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => ''); 
			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = historias_exitos::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id</th>'; 
				 $html.= '<th>Nombre</th>'; 
				 $html.= '<th>Institucion esp</th>';  
				 $html.= '<th>Imagen</th>'; 
				 $html.= '<th>Activo</th>'; 
				 $html.= '<th>Fecha creacion</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id'].'</td>'; 
					 $html.= '<td>'.$row['nombre'].'</td>'; 
					 $html.= '<td>'.$row['institucion_esp'].'</td>';  
					 $html.= '<td><img src="'.CONF_SITE_URL."upload/historias_exitos/".$row['imagen'].'" height="50"/> </td>'; 
					 $html.= '<td>'.$row['activo'].'</td>'; 
					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 