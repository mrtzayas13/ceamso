<?php
session_start();
include_once '../inc/config.inc.php';
include_once 'inc/validate-authetication.inc.php';

$codigo_seccion_administrable = 'proyectos';
$nivel_acceso = $usuario_logueado->recupera_permisos($codigo_seccion_administrable);
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') )
    header('location: index.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

    <head>

        <title><?php echo CONF_SITE_TITLE; ?></title>       
        <?php include 'inc/head.inc.php'; ?>

    </head>

    <body>
        <div id="body-wrapper"> 

            <?php include "inc/sidebar.tpl.php"; ?>

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>
               
                <h2>Administración de Programas y Proyectos</h2>
                <p id="page-intro"></p>

                <div class="clear"></div> <!-- End .clear -->

                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>Listado de Programas y Proyectos</h3>

                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_mensajes" class="notification png_bg" style="display:none;"></div>

                        <div class="button_row">

                            
                            <?php if($nivel_acceso['alta'] == 'S'): ?>
                            <button id="btn_nuevo">Nuevo</button>
                            <?php endif; ?>

                            <div style="float:right;">
                                <form name="frm_busqueda" id="frm_busqueda" action="" method="post">
                                    <input type="text" name="txt_busqueda" id="txt_busqueda" class="text-input" /> 
                                    <input type="hidden" name="hdn_busqueda" id="hdn_busqueda" />
                                    <button id="btn_busqueda">Buscar</button>
                                    <!-- <img src="<?php echo CONF_ADMIN_URL; ?>images/icons/zoom.png" /> -->
                                </form>
                            </div>

                        </div>

                        <div id="dv_filtro_aplicado" style="display: none;" class="notification information png_bg"></div>

                        <div id="dv_grilla"></div>

                        <!-- #BLOQUE OBLIGATORIO --> 

                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_formulario" title="edicion" style='display: none;'>

                            <form name='frm_datos' id="frm_datos" action="" method="post" enctype="multipart/formdata">
                                
                                <div id="dv_errores" class="notification png_bg" style="display:none;"></div>
                                
                                <input type="hidden" name='accion' id='accion' value='' />

				 <input type='hidden' name='id' id='id' />

				 <table cellspacing='0' cellpadding='3'>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Area id</td>
						 <td>
							 <select name='area_id' id='area_id' class='large-input'>
							 <?php 
							 $rs_areas = areas::lista(array('paginar' => false)); 
							 if(count($rs_areas['datos']) > 0) { 
								 ?> 
								 <option value=''></option> 
								 <?php 
								 foreach($rs_areas['datos'] as $fila_areas): 
								 ?> 
								 <option value='<?php echo $fila_areas['id'] ?>'><?php echo $fila_areas['area_esp'] ?></option> 
								 <?php 
								 endforeach; 
							 }else{ ?><option value=''> -- no hay datos -- </option> <?php } 
							 ?> 
							 </select>
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Tipo</td>
						 <td>
							 <select name='tipo' id='tipo' class='large-input'> 
								 <option value='Ejecutado'>Ejecutado</option>  
								 <option value='Ejecución'>Ejecución</option>  
							 </select> 
						 </td>
					 </tr>

                     <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Categoria</td>
						 <td>
							 <select name='categoria' id='categoria' class='large-input'> 
								 <option value='Programa'>Programa</option>  
								 <option value='Proyecto'>Proyecto</option>  
							 </select> 
						 </td>
					 </tr>

					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Titulo</td>
						 <td>
							 <input type='text' maxlength='500' name='titulo_esp' id='titulo_esp' class='text-input large-input ' />
						 </td>
					 </tr>
                     <!--
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Titulo eng</td>
						 <td>
							 <input type='text' maxlength='500' name='titulo_eng' id='titulo_eng' class='text-input large-input ' />
						 </td>
					 </tr>
                            -->
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Copete</td>
						 <td>
							 <input type='text' maxlength='500' name='copete_esp' id='copete_esp' class='text-input large-input ' />
						 </td>
					 </tr>
                     <!--
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Copete eng</td>
						 <td>
							 <input type='text' maxlength='500' name='copete_eng' id='copete_eng' class='text-input large-input ' />
						 </td>
					 </tr>
                            -->
                     <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>ODS</td>
						 <td>
                             <select name='ods[]' id='ods' data-placeholder=" " class="chosen-select large-input ods" multiple='multiple'  rows='8'>
                            <?php 
                                $rs_ods = ods::lista(array('paginar' => false));

                                if(count($rs_ods['datos']) > 0) {
                            ?> 
                                <option value=''></option> 
							<?php
                                    foreach($rs_ods['datos'] as $fila_ods):
                            ?>
                                <option value='<?php echo $fila_ods['id'] ?>'><?php echo ($fila_ods['id']. '. '.$fila_ods['titulo_esp']); ?></option> 
							<?php
                                    endforeach;
                                }else{ 
                            ?>
                                <option value=''> -- no hay datos -- </option> 
                            <?php 
                                }
                            ?> 
                             </select>
						 </td>
					 </tr>

					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Descripcion</td>
						 <td>
							 <textarea class='text-input textarea wysiwyg'  maxlength='65535' name='descripcion_esp' id='descripcion_esp' rows='8'></textarea>						 </td>
					 </tr>
                     <!--
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Descripcion eng</td>
						 <td>
							 <textarea class='text-input textarea wysiwyg'  maxlength='65535' name='descripcion_eng' id='descripcion_eng' rows='8'></textarea>						 </td>
					 </tr>
                            -->
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Fecha inicio</td>
						 <td>
							 <input type='text' maxlength='' name='fecha_inicio' id='fecha_inicio' class='text-input large-input datepicker' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Fecha fin</td>
						 <td>
							 <input type='text' maxlength='' name='fecha_fin' id='fecha_fin' class='text-input large-input datepicker' />
						 </td>
					 </tr>

					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Activo</td>
						 <td>
							 <select name='activo' id='activo' class='large-input'> 
								 <option value='SI'>SI</option>  
								 <option value='NO'>NO</option>  
							 </select> 
						 </td>
					 </tr>
					 
				 </table>

                                <div class='button_row' style="text-align: center;">
                                    <input type="submit" name="btn_guarda" id="btn_guarda" class="button" value="Guardar" />
                                </div>

                            </form>

                        </div>

                        <!-- #BLOQUE OBLIGATORIO --> 

                    </div> <!-- End .content-box-header -->

                </div>

                <div id="footer">
                    <small>
                        <!-- &#169; Copyright <?php echo date("Y"); ?> Nombre Empresa | --> | <a href="#">Top</a>
                    </small>
                </div><!-- End #footer -->
            </div>

        </div>
        <script type="text/javascript">
            var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"95%"},
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        </script>

        <script language="javascript" type="text/javascript">
            
            var code_file = 'proyectos.code.php';
            var code_file_ods = 'ods_proyectos.code.php'
            
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $.timepicker.setDefaults($.timepicker.regional['es']);
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });
            $('.datetimepicker').datetimepicker({
                dateFormat: 'dd/mm/yy',
                timeFormat: 'hh:mm'
            });
            

			
            var pagina_actual = 1;
                       
            $("#btn_guarda, #btn_nuevo, .btn_volver").button();
            $("#btn_busqueda").button({icons:{ primary: "ui-icon-search" }});
            $("#frm_busqueda").submit(function(evento){ 
                evento.preventDefault();
                $('#hdn_busqueda').val($('#txt_busqueda').val()); 
                if($('#txt_busqueda').val() != ''){
                    $('#dv_filtro_aplicado').show();
                    $('#dv_filtro_aplicado').html('<div>Filtro aplicado: <strong>"' + $('#txt_busqueda').val() + '"</strong><a class="close" href="javascript:;" onclick="eliminaFiltro()"><img alt="Cerrar" title="Quitar filtro" src="<?php echo CONF_ADMIN_URL; ?>images/icons/cross_grey_small.png" /></a></div>');
                }else{
                    $('#dv_filtro_aplicado').hide();
                }
                cargaGrilla(1);
            });
            
            function eliminaFiltro(){
                $('#hdn_busqueda').val('');
                $('#txt_busqueda').val('');
                $('#dv_filtro_aplicado').effect('blind',{},500);
                cargaGrilla();
            }
            
            $("#dv_formulario").dialog({
                modal: true,
                autoOpen: false,
                width: 500,
                open: function(event, ui) { 
                    $('a[title]').qtip();
                    $("#dv_errores").hide();

                }
            });
            
            $("#frm_datos").validate({
                errorElement: "span"
		, rules: { 
			 area_id: {  number: true  } ,
			 fecha_inicio: {  date: true  } ,
			 fecha_fin: {  date: true  }  
		 }
            });
            
            <?php if($nivel_acceso['alta'] == 'S'): ?>
            $("#btn_nuevo").click(function(){
                
                $("#accion").val("inserta");
                
                $('#id').val(''); 
                $('#area_id').val(''); 
                $('#tipo').val(''); 
                $('#categoria').val(''); 
                $('#titulo_esp').val(''); 
                $('#titulo_eng').val(''); 
                $('#copete_esp').val(''); 
                $('#copete_eng').val(''); 
                $('#descripcion_esp').wysiwyg('setContent', ''); 
                $('#descripcion_eng').wysiwyg('setContent', ''); 
                $('#fecha_inicio').val(''); 
                $('#fecha_fin').val(''); 
                $('#activo').val(''); 
                $('#fecha_creacion').val(''); 

                //Chosen's Multi select clear
                $('#ods').val('').trigger("chosen:updated");

                $("#dv_formulario").dialog({ title: 'Insertar registro' });
                $("#dv_formulario").dialog("open");
                
            });
            <?php endif ?>
            
            <?php if( ($nivel_acceso['alta'] == 'S') || ($nivel_acceso['modificacion'] == 'S')): ?>
            $("#frm_datos").submit(function(evento){
                
                evento.preventDefault();
                
                $("#dv_errores").hide();
                
                if($(this).valid()){
                    $(this).ajaxSubmit({
                        type: 'POST',
                        url: code_file,
                        success: function(mensaje){
                            if(mensaje == ""){
                                $("#dv_mensajes").html("<div>Sus datos se actualizaron correctamente.</div>");
                                $("#dv_mensajes").addClass("success").removeClass("error");
                                $("#dv_mensajes").show("fade", null, 1000);
                                $("#dv_formulario").dialog("close");
                                cargaGrilla(pagina_actual);
                            }else{
                                $("#dv_errores").html("<div>" + mensaje + "</div>");
                                $("#dv_errores").addClass("error").removeClass("success");
                                $("#dv_errores").show("fade", null, 1000);
                            }
                            
                        }
                    });
                }
            });
            <?php endif; ?>
            
            function cargaGrilla(p_pagina){
                $.ajax({
                    type: 'POST',
                    url: code_file,
                    data: { accion: 'lista', pagina: p_pagina, buscar: $('#hdn_busqueda').val()  },
                    success: function(p_html){
						pagina_actual = p_pagina;
                        $('#dv_grilla').html(p_html);
						$('a[title]').qtip();
                    }
                });
            }
            
            <?php if($nivel_acceso['baja'] == 'S'): ?>
            function eliminaRegistro(p_id){
                if(confirm("Esta seguro que quiere eliminar el registro?")){
                    $.ajax({
                        type: 'POST',
                        url: code_file,
                        data: { accion: 'elimina', id: p_id },
                        success: function(p_mensaje){
                            if(p_mensaje == ""){
                                $("#dv_mensajes").html("<div>Sus datos se actualizaron correctamente.</div>");
                                $("#dv_mensajes").addClass("success").removeClass("error");
                                cargaGrilla(pagina_actual);
                            }else{
                                $("#dv_mensajes").html("<div>" + p_mensaje + "</div>");
                                $("#dv_mensajes").addClass("error").removeClass("success");
                            }
                            $("#dv_mensajes").show("fade", null, 1000);
                        }
                    });
                }
            }
            <?php endif; ?>
            
            <?php if($nivel_acceso['modificacion'] == 'S'): ?>
            function editaRegistro(p_id){
                $("#accion").val("actualiza");
                $.ajax({
                    type: 'POST',
                    url: code_file,
                    data: { accion: 'edita', id: p_id },
                    dataType: 'json',
                    success: function(datos){ 
                        $('#id').val(datos.id); 
                        $('#area_id').val(datos.area_id); 
                        $('#tipo').val(datos.tipo); 
                        $('#categoria').val(datos.categoria); 
                        $('#titulo_esp').val(datos.titulo_esp); 
                        $('#titulo_eng').val(datos.titulo_eng); 
                        $('#copete_esp').val(datos.copete_esp); 
                        $('#copete_eng').val(datos.copete_eng); 
                        $('#descripcion_esp').wysiwyg('setContent', datos.descripcion_esp); 
                        $('#descripcion_eng').wysiwyg('setContent', datos.descripcion_eng); 
                        $('#fecha_inicio').val(datos.fecha_inicio); 
                        $('#fecha_fin').val(datos.fecha_fin); 
                        $('#activo').val(datos.activo); 
                        $('#fecha_creacion').val(datos.fecha_creacion); 
                        $("#dv_formulario").dialog({ title: 'Edita registro' });
                        $("#dv_formulario").dialog("open");
                    }
                });

                /*Chosen multi select updater*/
                $.ajax({
                    type: 'POST',
                    url: code_file_ods,
                    data:{accion: 'edita', id: p_id},
                    dataType: 'json',
                    success: function(datos){
                        $('#ods').val(datos);
                        $('#ods').trigger("chosen:updated");
                    }
                });
            }
            <?php endif; ?>
            
            $(document).ready(function(){
                cargaGrilla();
                $('a[title]').qtip();
            });
           
            
        </script>

    </body>

</html>