<?php 

 session_start(); 

include_once '../inc/config.inc.php'; 

include_once 'inc/validate-authetication.inc.php'; 

include_once 'inc/validate-request.inc.php'; 



/* Chequeo de permisos de usuario */

if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 

else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 

$nivel_acceso = $usuario_logueado->recupera_permisos('archivo_licitaciones'); 

if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 

	die('Acceso no permitido');



/* Acciones a realizar */

if (isset($_POST['accion'])) { 



	 $archivo_licitaciones  = new archivo_licitaciones(); 



	 switch($_POST['accion']) { 



		 case 'inserta': 

 			 if($nivel_acceso['alta'] == 'S'){ 

				 try{ 

				 $archivo_licitaciones->set_id(null); 

				 $archivo_licitaciones->set_nombre_esp( $_POST['nombre_esp']); 

				 $archivo_licitaciones->set_nombre_eng( $_POST['nombre_eng']); 

				 $nom_archivo = time(); 

				 if((strlen($archivo_licitaciones->get_archivo()) > 0) && (empty($_POST['archivo']))) { 

					 if(file_exists(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo()); } 

				 } 

				 if(FileManager::subeArchivo($_FILES['file_archivo'], 'archivo_licitaciones', $nom_archivo)) { 

					 if((strlen($archivo_licitaciones->get_archivo()) > 0)) { 

	 					 if(file_exists(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo()); } 

					 } 

					 $_POST['archivo'] = $nom_archivo . substr($_FILES['file_archivo'][name], strrpos($_FILES['file_archivo'][name], '.')); 

				 } 

			 	$archivo_licitaciones->set_archivo($_POST['archivo']); 

				 $archivo_licitaciones->set_fecha_creacion( $_POST['fecha_creacion']); 

				 $archivo_licitaciones->set_licitacion_id( $_POST['licitacion_id']); 

				 $archivo_licitaciones->guarda(); 

				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 

 			 } 

 			 break; 

 

		 case 'actualiza': 

 			 if($nivel_acceso['modificacion'] == 'S'){ 

				 try{ 

				 $archivo_licitaciones->carga($_POST['id']); 

				 $archivo_licitaciones->set_id($_POST['id']); 

				 $archivo_licitaciones->set_nombre_esp($_POST['nombre_esp']); 

				 $archivo_licitaciones->set_nombre_eng($_POST['nombre_eng']); 

			 $nom_archivo = time(); 

			 if((strlen($archivo_licitaciones->get_archivo()) > 0) && (empty($_POST['archivo']))) { 

				 if(file_exists(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo()); } 

			 } 

			 if(FileManager::subeArchivo($_FILES['file_archivo'], 'archivo_licitaciones', $nom_archivo)) { 

				 if((strlen($archivo_licitaciones->get_archivo()) > 0)) { 

 					 if(file_exists(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo())){ unlink(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $archivo_licitaciones->get_archivo()); } 

				 } 

				 $_POST['archivo'] = $nom_archivo . substr($_FILES['file_archivo'][name], strrpos($_FILES['file_archivo'][name], '.')); 

			 } 

				 $archivo_licitaciones->set_archivo($_POST['archivo']); 

			 

				 $archivo_licitaciones->set_fecha_creacion($_POST['fecha_creacion']); 

				 $archivo_licitaciones->set_licitacion_id($_POST['licitacion_id']); 

				 if(!$archivo_licitaciones->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 

				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 

 			 } 

 			 break; 

 

		 case 'edita': 

 			 $archivo_licitaciones->carga($_POST['id']); 

			 $json_arr = array( 'id' => $archivo_licitaciones->get_id(),'nombre_esp' => $archivo_licitaciones->get_nombre_esp(),'nombre_eng' => $archivo_licitaciones->get_nombre_eng(),'archivo' => $archivo_licitaciones->get_archivo(),'fecha_creacion' => (($archivo_licitaciones->get_fecha_creacion() != null) ? date('d/m/Y H:i', strtotime($archivo_licitaciones->get_fecha_creacion())) : '') ,'licitacion_id' => $archivo_licitaciones->get_licitacion_id()); 

 			 die(json_encode($json_arr)); 

 			 break; 

 

		 case 'elimina': 

 			 if($nivel_acceso['baja'] == 'S'){ 

				 $archivo_licitaciones->set_id($_POST['id']); 

 				 if(!$archivo_licitaciones->elimina()) { die('Ocurrio un error al elimina el registro.'); } 

				 die(''); 

 			 } 

 			 break; 

 

		 case 'lista': 

 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 

 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden' => '1 desc'); 

			 if(isset($_POST['id']) && !empty($_POST['id'])) 

				 $opciones['filtro'] .= (($opciones['filtro'] == '')?' WHERE ':' AND ') . sprintf(' archivo_licitaciones.licitacion_id = %s ', DBManager::formatSQLValue($_POST['id'])); 

			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 

				 $opciones['buscar'] = $_POST['buscar']; 

			 } 

			 $arr = archivo_licitaciones::lista($opciones); 



			 if(count($arr['datos']) > 0) { 

			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 

			 $html.= '<thead><tr>'; 

				 $html.= '<th>Id</th>'; 

				 $html.= '<th>Nombre esp</th>'; 

				 $html.= '<th>Nombre eng</th>'; 

				 $html.= '<th>Archivo</th>'; 

				 $html.= '<th>Fecha creacion</th>'; 

				 $html.= '<th>&nbsp;</th>'; 

			 $html.= '</tr></thead>'; 

			 $estilo_fila = ''; 

			 foreach ($arr['datos'] as $row) { 

				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 

				 $html.= '<tr' . $estilo_fila . '>'; 

					 $html.= '<td>'.$row['id'].'</td>'; 

					 $html.= '<td>'.$row['nombre_esp'].'</td>'; 

					 $html.= '<td>'.$row['nombre_eng'].'</td>'; 

					 $html.= '<td><a href="'.CONF_SITE_URL."upload/archivo_licitaciones/".$row['archivo'].'">Archivo</a> </td>'; 

					 $html.= '<td>' . (($row['fecha_creacion'] != '') ? date('d/m/Y H:i', strtotime($row['fecha_creacion'])) : '') . '</td>'; 

					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 

					 if($nivel_acceso['modificacion'] == 'S') 

						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 

					 if($nivel_acceso['baja'] == 'S') 

						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 

					 $html.= '</td>'; 

				 $html.= '</tr>'; 

			 } 

			 $html.= '</table>'; 

			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 

			 $pagerCtrl->useJavaScript(true); 

			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 

			 $pagerCtrl->set_currentPage($pagina_actual); 

			 $html .= $pagerCtrl->get_control(); 

			 } else { 

				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 

			 } 

			 die($html); 

			 break; 

 	 } 

 

} 

 ?> 