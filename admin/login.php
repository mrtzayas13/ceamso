<?php
session_start();
include_once '../inc/config.inc.php';

$mensaje = '';

if(isset($_POST['txt_usuario'])){
    
    $datos_usuario = usuarios::lista(array('filtro' => sprintf('WHERE usuario = %s AND clave = md5(%s)',
                    DBManager::formatSQLValue($_POST['txt_usuario']),
                    DBManager::formatSQLValue($_POST['txt_clave']))));
					
    if(count($datos_usuario['datos']) > 0){
        $_SESSION['s_id_usuario'] = $datos_usuario['datos'][0]['id_usuario'];
		
        header("location: index.php");
    }else{
        $mensaje = '<div class="notification error png_bg"><div>Usuario o clave incorrecto.</div></div>';
    }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

    <head>

        <title><?php echo CONF_SITE_TITLE; ?></title>       
        <?php include 'inc/head.inc.php'; ?>

    </head>

    <body id="login">
		
	<div id="login-wrapper" class="png_bg">
            
            <div id="login-top">
                
                <h1>{code:project_name}</h1>
		
				<img id="logo" src="<?php echo CONF_ADMIN_URL; ?>images/logo.png" alt="{code:project_name}" />
                
            </div>
            
            <div id="login-content">
				
                <form action="" method="post" name="frm_login" id="frm_login">
                    
                    <?php echo $mensaje; ?>
                    
                    <p>
                        <label>Usuario</label>
                        <input class="text-input required" type="text" name="txt_usuario" id="txt_usuario" />
                    </p>
                    <div class="clear"></div>
                    <p>
                        <label>Clave</label>
						<input class="text-input required" type="password" name="txt_clave" id="txt_clave" />
                    </p>
                    <div class="clear"></div>
                    <div class="clear"></div>
                    <p>
                        <input class="button" type="submit" value="Ingresar" />
                    </p>
                    
                </form>
                
            </div>
                
        </div>
        
        <script language="javascript" type="text/javascript">
			
			$("#frm_login").validate({
               errorElement: "span" 
            });
			
        </script>

    </body>

</html>