<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('newsletter'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $newsletter  = new newsletter(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $newsletter->set_id(null); 
				 $newsletter->set_nombre( $_POST['nombre']); 
				 $newsletter->set_email( $_POST['email']); 
				 $newsletter->set_recibir_llamados( ( $_POST['recibir_llamados'] == 'on') ? 1 : 0); 
				 $newsletter->set_recibir_novedades( ( $_POST['recibir_novedades'] == 'on') ? 1 : 0); 
				 $newsletter->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $newsletter->carga($_POST['id']); 
				 $newsletter->set_id($_POST['id']); 
				 $newsletter->set_nombre($_POST['nombre']); 
				 $newsletter->set_email($_POST['email']); 
				 $newsletter->set_recibir_llamados( ( $_POST['recibir_llamados'] == 'on') ? 1 : 0); 
				 $newsletter->set_recibir_novedades( ( $_POST['recibir_novedades'] == 'on') ? 1 : 0); 
				 if(!$newsletter->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $newsletter->carga($_POST['id']); 
			 $json_arr = array( 'id' => $newsletter->get_id(),'nombre' => $newsletter->get_nombre(),'email' => $newsletter->get_email(),'recibir_llamados' => $newsletter->get_recibir_llamados(),'recibir_novedades' => $newsletter->get_recibir_novedades()); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $newsletter->set_id($_POST['id']); 
 				 if(!$newsletter->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => ''); 
			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = newsletter::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id</th>'; 
				 $html.= '<th>Nombre</th>'; 
				 $html.= '<th>Email</th>'; 
				 $html.= '<th>Recibir llamados</th>'; 
				 $html.= '<th>Recibir novedades</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id'].'</td>'; 
					 $html.= '<td>'.$row['nombre'].'</td>'; 
					 $html.= '<td>'.$row['email'].'</td>'; 
					 $html.= '<td>'.(( $row['recibir_llamados'] == 1) ? 'SI' : 'NO').'</td>'; 
					 $html.= '<td>'.(( $row['recibir_novedades'] == 1) ? 'SI' : 'NO').'</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 