<?php
session_start();
include_once '../inc/config.inc.php';
include_once 'inc/validate-authetication.inc.php';

$codigo_seccion_administrable = 'historias_exitos';
$nivel_acceso = $usuario_logueado->recupera_permisos($codigo_seccion_administrable);
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') )
    header('location: index.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

    <head>

        <title><?php echo CONF_SITE_TITLE; ?></title>       
        <?php include 'inc/head.inc.php'; ?>

    </head>

    <body>

        <div id="body-wrapper"> 

            <?php include "inc/sidebar.tpl.php"; ?>

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <h2>Administración de Historias exitos</h2>
                <p id="page-intro"></p>

                <div class="clear"></div> <!-- End .clear -->

                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>Listado de Historias exitos</h3>

                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_mensajes" class="notification png_bg" style="display:none;"></div>

                        <div class="button_row">

                            
                            <?php if($nivel_acceso['alta'] == 'S'): ?>
                            <button id="btn_nuevo">Nuevo</button>
                            <?php endif; ?>

                            <div style="float:right;">
                                <form name="frm_busqueda" id="frm_busqueda" action="" method="post">
                                    <input type="text" name="txt_busqueda" id="txt_busqueda" class="text-input" /> 
                                    <input type="hidden" name="hdn_busqueda" id="hdn_busqueda" />
                                    <button id="btn_busqueda">Buscar</button>
                                    <!-- <img src="<?php echo CONF_ADMIN_URL; ?>images/icons/zoom.png" /> -->
                                </form>
                            </div>

                        </div>

                        <div id="dv_filtro_aplicado" style="display: none;" class="notification information png_bg"></div>

                        <div id="dv_grilla"></div>

                        <!-- #BLOQUE OBLIGATORIO --> 

                        <!-- BLOQUE OBLIGATORIO --> 

                        <div id="dv_formulario" title="edicion" style='display: none;'>

                            <form name='frm_datos' id="frm_datos" action="" method="post" enctype="multipart/formdata">
                                
                                <div id="dv_errores" class="notification png_bg" style="display:none;"></div>
                                
                                <input type="hidden" name='accion' id='accion' value='' />

				 <input type='hidden' name='id' id='id' />

				 <table cellspacing='0' cellpadding='3'>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Nombre</td>
						 <td>
							 <input type='text' maxlength='255' name='nombre' id='nombre' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Institucion esp</td>
						 <td>
							 <input type='text' maxlength='255' name='institucion_esp' id='institucion_esp' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Institucion eng</td>
						 <td>
							 <input type='text' maxlength='255' name='institucion_eng' id='institucion_eng' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Titulo esp</td>
						 <td>
							 <input type='text' maxlength='255' name='titulo_esp' id='titulo_esp' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Titulo eng</td>
						 <td>
							 <input type='text' maxlength='255' name='titulo_eng' id='titulo_eng' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Cita esp</td>
						 <td>
							 <textarea class='text-input textarea '   maxlength='200' name='cita_esp' id='cita_esp' rows='8'></textarea>						 
							<span id="cita_esp_text"></span>
						</td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Cita eng</td>
						 <td>
							 <textarea class='text-input textarea '  maxlength='200' name='cita_eng' id='cita_eng' rows='8'></textarea>						 
							<span id="cita_eng_text"></span>
						</td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Copete esp</td>
						 <td>
							 <input type='text' maxlength='255' name='copete_esp' id='copete_esp' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Copete eng</td>
						 <td>
							 <input type='text' maxlength='255' name='copete_eng' id='copete_eng' class='text-input large-input ' />
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Descripcion largo esp</td>
						 <td>
							 <textarea class='text-input textarea wysiwyg'  maxlength='65535' name='descripcion_largo_esp' id='descripcion_largo_esp' rows='8'></textarea>						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Descripcion largo eng</td>
						 <td>
							 <textarea class='text-input textarea wysiwyg'  maxlength='65535' name='descripcion_largo_eng' id='descripcion_largo_eng' rows='8'></textarea>						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Imagen</td>
						 <td>
							 <input type='file' name='file_imagen' id='file_imagen' />
							 <div id='pnl_imagen' style='display:none;' class='file-info'>
								 <input type='checkbox' name='imagen' id='imagen' /> Mantener el archivo.<br />
 								 <img id='img_imagen' />
							 </div>
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Activo</td>
						 <td>
							 <select name='activo' id='activo' class='large-input'> 
								 <option value='SI'>SI</option>  
								 <option value='NO'>NO</option>  
							 </select> 
						 </td>
					 </tr>
					 <tr>
						 <td style='width:18px; text-align:center;'>						 </td>
						 <td>Fecha creacion</td>
						 <td>
							 <input type='text' maxlength='' name='fecha_creacion' id='fecha_creacion' class='text-input large-input datetimepicker' />
						 </td>
					 </tr>
				 </table>

                                <div class='button_row' style="text-align: center;">
                                    <input type="submit" name="btn_guarda" id="btn_guarda" class="button" value="Guardar" />
                                </div>

                            </form>

                        </div>

                        <!-- #BLOQUE OBLIGATORIO --> 

                    </div> <!-- End .content-box-header -->

                </div>

                <div id="footer">
                    <small>
                        <!-- &#169; Copyright <?php echo date("Y"); ?> Nombre Empresa | --> | <a href="#">Top</a>
                    </small>
                </div><!-- End #footer -->

            </div>

        </div>

        <script language="javascript" type="text/javascript">
            
            var code_file = 'historias_exitos.code.php';
            
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $.timepicker.setDefaults($.timepicker.regional['es']);
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });
            $('.datetimepicker').datetimepicker({
                dateFormat: 'dd/mm/yy',
                timeFormat: 'hh:mm'
            });
            

			
            var pagina_actual = 1;
                       
            $("#btn_guarda, #btn_nuevo, .btn_volver").button();
            $("#btn_busqueda").button({icons:{ primary: "ui-icon-search" }});
            $("#frm_busqueda").submit(function(evento){ 
                evento.preventDefault();
                $('#hdn_busqueda').val($('#txt_busqueda').val()); 
                if($('#txt_busqueda').val() != ''){
                    $('#dv_filtro_aplicado').show();
                    $('#dv_filtro_aplicado').html('<div>Filtro aplicado: <strong>"' + $('#txt_busqueda').val() + '"</strong><a class="close" href="javascript:;" onclick="eliminaFiltro()"><img alt="Cerrar" title="Quitar filtro" src="<?php echo CONF_ADMIN_URL; ?>images/icons/cross_grey_small.png" /></a></div>');
                }else{
                    $('#dv_filtro_aplicado').hide();
                }
                cargaGrilla(1);
            });
            
            function eliminaFiltro(){
                $('#hdn_busqueda').val('');
                $('#txt_busqueda').val('');
                $('#dv_filtro_aplicado').effect('blind',{},500);
                cargaGrilla();
            }
            
            $("#dv_formulario").dialog({
                modal: true,
                autoOpen: false,
                width: 500,
                open: function(event, ui) { 
                    $('a[title]').qtip();
                    $("#dv_errores").hide();

                }
            });
            
            $("#frm_datos").validate({
                errorElement: "span"
		, rules: { 
			 fecha_creacion: {  required: true  }  
		 }
            });
            
            <?php if($nivel_acceso['alta'] == 'S'): ?>
            $("#btn_nuevo").click(function(){
                
                $("#accion").val("inserta");
                
		$('#id').val(''); 
		$('#nombre').val(''); 
		$('#institucion_esp').val(''); 
		$('#institucion_eng').val(''); 
		$('#titulo_esp').val(''); 
		$('#titulo_eng').val(''); 
		$('#cita_esp').text(''); 
		$('#cita_esp_text').text("Quedan 200 caracteres de 200"); 
		$('#cita_eng').text(''); 
		$('#cita_eng_text').text("Quedan 200 caracteres de 200"); 
		$('#copete_esp').val(''); 
		$('#copete_eng').val(''); 
		$('#descripcion_largo_esp').wysiwyg('setContent', ''); 
		$('#descripcion_largo_eng').wysiwyg('setContent', ''); 
			$('#imagen').removeAttr('checked'); 
			$('#pnl_imagen').hide(); 
			$('#img_imagen').attr('src', '').hide(); 
		$('#file_imagen').replaceWith("<input type='file' name='file_imagen' id='file_imagen' />"); 
				$('#img_imagen').attr('src', '').hide(); 
		$('#imagen').val(''); 
		$('#activo').val(''); 
		$('#fecha_creacion').val(''); 


                $("#dv_formulario").dialog({ title: 'Insertar registro' });
                $("#dv_formulario").dialog("open");
                
            });
            <?php endif ?>
            
            <?php if( ($nivel_acceso['alta'] == 'S') || ($nivel_acceso['modificacion'] == 'S')): ?>
            $("#frm_datos").submit(function(evento){
                
                evento.preventDefault();
                
                $("#dv_errores").hide();
                
                if($(this).valid()){
                    $(this).ajaxSubmit({
                        type: 'POST',
                        url: code_file,
                        success: function(mensaje){
                            if(mensaje == ""){
                                $("#dv_mensajes").html("<div>Sus datos se actualizaron correctamente.</div>");
                                $("#dv_mensajes").addClass("success").removeClass("error");
                                $("#dv_mensajes").show("fade", null, 1000);
                                $("#dv_formulario").dialog("close");
                                cargaGrilla(pagina_actual);
                            }else{
                                $("#dv_errores").html("<div>" + mensaje + "</div>");
                                $("#dv_errores").addClass("error").removeClass("success");
                                $("#dv_errores").show("fade", null, 1000);
                            }
                            
                        }
                    });
                }
            });
            <?php endif; ?>
            
            function cargaGrilla(p_pagina){
                $.ajax({
                    type: 'POST',
                    url: code_file,
                    data: { accion: 'lista', pagina: p_pagina, buscar: $('#hdn_busqueda').val()  },
                    success: function(p_html){
						pagina_actual = p_pagina;
                        $('#dv_grilla').html(p_html);
						$('a[title]').qtip();
                    }
                });
            }
            
            <?php if($nivel_acceso['baja'] == 'S'): ?>
            function eliminaRegistro(p_id){
                if(confirm("Esta seguro que quiere eliminar el registro?")){
                    $.ajax({
                        type: 'POST',
                        url: code_file,
                        data: { accion: 'elimina', id: p_id },
                        success: function(p_mensaje){
                            if(p_mensaje == ""){
                                $("#dv_mensajes").html("<div>Sus datos se actualizaron correctamente.</div>");
                                $("#dv_mensajes").addClass("success").removeClass("error");
                                cargaGrilla(pagina_actual);
                            }else{
                                $("#dv_mensajes").html("<div>" + p_mensaje + "</div>");
                                $("#dv_mensajes").addClass("error").removeClass("success");
                            }
                            $("#dv_mensajes").show("fade", null, 1000);
                        }
                    });
                }
            }
            <?php endif; ?>
            
            <?php if($nivel_acceso['modificacion'] == 'S'): ?>
            function editaRegistro(p_id){
                $("#accion").val("actualiza");
                $.ajax({
                    type: 'POST',
                    url: code_file,
                    data: { accion: 'edita', id: p_id },
                    dataType: 'json',
                    success: function(datos){
                        
			$('#id').val(datos.id); 
			$('#nombre').val(datos.nombre); 
			$('#institucion_esp').val(datos.institucion_esp); 
			$('#institucion_eng').val(datos.institucion_eng); 
			$('#titulo_esp').val(datos.titulo_esp); 
			$('#titulo_eng').val(datos.titulo_eng); 
			$('#cita_esp').text(datos.cita_esp); 
			$('#cita_esp_text').text((datos.cita_esp==null || datos.cita_esp===false) ? "Quedan 200 caracteres de 200" : "Quedan " + (200 - datos.cita_esp.length) + " caracteres de 200"); 
			$('#cita_eng').text(datos.cita_eng); 
			$('#cita_eng_text').text((datos.cita_eng==null || datos.cita_eng===false) ? "Quedan 200 caracteres de 200" : "Quedan " + (200 - datos.cita_eng.length) + " caracteres de 200"); 
			$('#copete_esp').val(datos.copete_esp); 
			$('#copete_eng').val(datos.copete_eng); 
			$('#descripcion_largo_esp').wysiwyg('setContent', datos.descripcion_largo_esp); 
			$('#descripcion_largo_eng').wysiwyg('setContent', datos.descripcion_largo_eng); 
			$('#file_imagen').replaceWith("<input type='file' name='file_imagen' id='file_imagen' />"); 
				if((datos.imagen != '') && (datos.imagen != null)) { 
					$('#imagen').attr('checked',true); 
					$('#pnl_imagen').show(); 
					$('#img_imagen').attr('src','<?php echo CONF_UPLOAD_PATH ?>historias_exitos/' + datos.imagen).show(); 
				}else{
 					$('#imagen').removeAttr('checked'); 
					$('#pnl_imagen').hide(); 
				} 
			$('#imagen').val(datos.imagen); 
			$('#activo').val(datos.activo); 
			$('#fecha_creacion').val(datos.fecha_creacion); 

                        
                        $("#dv_formulario").dialog({ title: 'Edita registro' });
                        $("#dv_formulario").dialog("open");
                    }
                });
            }
            <?php endif; ?>
            
            $(document).ready(function(){
			
				$('#cita_esp').on('keyup',function(){
					if($(this).val().length == 200)
					{
						$('#cita_esp_text').css('color', 'red');
					}else{
						$('#cita_esp_text').css('color', 'black');
					}
					
					$('#cita_esp_text').text("Quedan " + (200 - $(this).val().length) + " caracteres de 200"); 
				});
				
				$('#cita_eng').on('keyup',function(){
					if($(this).val().length == 200)
					{
						$('#cita_eng_text').css('color', 'red');
					}else{
						$('#cita_eng_text').css('color', 'black');
					}
					
					$('#cita_eng_text').text("Quedan " + (200 - $(this).val().length) + " caracteres de 200"); 
				});
				
                cargaGrilla();
                $('a[title]').qtip();
            });
           
            
        </script>

    </body>

</html>