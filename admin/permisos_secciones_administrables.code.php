<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('permisos_secciones_administrables'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $permisos_secciones_administrables  = new permisos_secciones_administrables(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $permisos_secciones_administrables->set_id_permiso_seccion_administrable(null); 
				 $permisos_secciones_administrables->set_id_rol( $_POST['id_rol']); 
				 $permisos_secciones_administrables->set_codigo_seccion_administrable( $_POST['codigo_seccion_administrable']); 
				 $permisos_secciones_administrables->set_alta( $_POST['alta']); 
				 $permisos_secciones_administrables->set_baja( $_POST['baja']); 
				 $permisos_secciones_administrables->set_modificacion( $_POST['modificacion']); 
				 $permisos_secciones_administrables->set_consulta( $_POST['consulta']); 
				 $permisos_secciones_administrables->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $permisos_secciones_administrables->carga($_POST['id_permiso_seccion_administrable']); 
				 $permisos_secciones_administrables->set_id_permiso_seccion_administrable($_POST['id_permiso_seccion_administrable']); 
				 $permisos_secciones_administrables->set_id_rol($_POST['id_rol']); 
				 $permisos_secciones_administrables->set_codigo_seccion_administrable($_POST['codigo_seccion_administrable']); 
				 $permisos_secciones_administrables->set_alta($_POST['alta']); 
				 $permisos_secciones_administrables->set_baja($_POST['baja']); 
				 $permisos_secciones_administrables->set_modificacion($_POST['modificacion']); 
				 $permisos_secciones_administrables->set_consulta($_POST['consulta']); 
				 if(!$permisos_secciones_administrables->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $permisos_secciones_administrables->carga($_POST['id_permiso_seccion_administrable']); 
			 $json_arr = array( 'id_permiso_seccion_administrable' => $permisos_secciones_administrables->get_id_permiso_seccion_administrable(),'id_rol' => $permisos_secciones_administrables->get_id_rol(),'codigo_seccion_administrable' => $permisos_secciones_administrables->get_codigo_seccion_administrable(),'alta' => $permisos_secciones_administrables->get_alta(),'baja' => $permisos_secciones_administrables->get_baja(),'modificacion' => $permisos_secciones_administrables->get_modificacion(),'consulta' => $permisos_secciones_administrables->get_consulta()); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $permisos_secciones_administrables->set_id_permiso_seccion_administrable($_POST['id_permiso_seccion_administrable']); 
 				 if(!$permisos_secciones_administrables->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden'=> ' 1 desc'); if(isset($_POST['id_rol']) && !empty($_POST['id_rol'])) 
				 $opciones['filtro'] .= (($opciones['filtro'] == '')?' WHERE ':' AND ') . sprintf('roles.id_rol = %s ', DBManager::formatSQLValue($_POST['id_rol'])); 
			 if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = permisos_secciones_administrables::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id permiso seccion administrable</th>'; 
				 $html.= '<th>Id rol</th>'; 
				 $html.= '<th>Codigo seccion administrable</th>'; 
				 $html.= '<th>Alta</th>'; 
				 $html.= '<th>Baja</th>'; 
				 $html.= '<th>Modificacion</th>'; 
				 $html.= '<th>Consulta</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id_permiso_seccion_administrable'].'</td>'; 
					 $html.= '<td>'.$row['roles_rol'].'</td>'; 
					 $html.= '<td>'.$row['secciones_administrables_codigo_seccion_administrable'].'</td>'; 
					 $html.= '<td>'.$row['alta'].'</td>'; 
					 $html.= '<td>'.$row['baja'].'</td>'; 
					 $html.= '<td>'.$row['modificacion'].'</td>'; 
					 $html.= '<td>'.$row['consulta'].'</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id_permiso_seccion_administrable'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id_permiso_seccion_administrable'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 