<?php 
 session_start(); 
include_once '../inc/config.inc.php'; 
include_once 'inc/validate-authetication.inc.php'; 
include_once 'inc/validate-request.inc.php'; 

/* Chequeo de permisos de usuario */
if(!isset($_SESSION['s_id_usuario'])){ die('Acceso no permitido'); } 
else{ $usuario_logueado->carga($_SESSION['s_id_usuario']); } 
$nivel_acceso = $usuario_logueado->recupera_permisos('usuarios'); 
if( ($nivel_acceso['alta']!='S') && ($nivel_acceso['baja']!='S') && ($nivel_acceso['modificacion'] != 'S') && ($nivel_acceso['consulta']!='S') ) 
	die('Acceso no permitido');

/* Acciones a realizar */
if (isset($_POST['accion'])) { 

	 $usuarios  = new usuarios(); 

	 switch($_POST['accion']) { 

		 case 'inserta': 
 			 if($nivel_acceso['alta'] == 'S'){ 
				 try{ 
				 $usuarios->set_id_usuario(null); 
				 $usuarios->set_usuario( $_POST['usuario']); 
				 if(!Security::validatePasswordFormat($_POST['clave'])){ 
					 die(Security::get_last_error()); 
				 } 
				 $usuarios->set_clave( md5($_POST['clave'])); 
				 $usuarios->set_nombre( $_POST['nombre']); 
				 $usuarios->set_apellido( $_POST['apellido']); 
				 $usuarios->set_email( $_POST['email']); 
				 $usuarios->set_id_rol( $_POST['id_rol']); 
				 $usuarios->guarda(); 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'actualiza': 
 			 if($nivel_acceso['modificacion'] == 'S'){ 
				 try{ 
				 $usuarios->carga($_POST['id_usuario']); 
				 $usuarios->set_id_usuario($_POST['id_usuario']); 
				 $usuarios->set_usuario($_POST['usuario']); 
				 if(!Security::validatePasswordFormat($_POST['clave'])){ 
					 die(Security::get_last_error()); 
				 } 
				 $usuarios->set_clave( md5($_POST['clave'])); 
				 $usuarios->set_nombre($_POST['nombre']); 
				 $usuarios->set_apellido($_POST['apellido']); 
				 $usuarios->set_email($_POST['email']); 
				 $usuarios->set_id_rol($_POST['id_rol']); 
				 if(!$usuarios->guarda()) { die('No se pueden guardar los datos. Verifique que haya completado todos los campos requeridos y que la información sea válida y vuelva a intentarlo.'); } 
				 }catch(Exception $exc){ die($exc->getMessage()); }				 die(''); 
 			 } 
 			 break; 
 
		 case 'edita': 
 			 $usuarios->carga($_POST['id_usuario']); 
			 $json_arr = array( 'id_usuario' => $usuarios->get_id_usuario(),'usuario' => $usuarios->get_usuario(),'clave' => '','nombre' => $usuarios->get_nombre(),'apellido' => $usuarios->get_apellido(),'email' => $usuarios->get_email(),'id_rol' => $usuarios->get_id_rol()); 
 			 die(json_encode($json_arr)); 
 			 break; 
 
		 case 'elimina': 
 			 if($nivel_acceso['baja'] == 'S'){ 
				 $usuarios->set_id_usuario($_POST['id_usuario']); 
 				 if(!$usuarios->elimina()) { die('Ocurrio un error al elimina el registro.'); } 
				 die(''); 
 			 } 
 			 break; 
 
		 case 'lista': 
 			 $pagina_actual = (isset($_POST['pagina'])) ? $_POST['pagina'] : 1; 
 			 $opciones = array('num_pagina'=>$pagina_actual, 'reg_x_pag'=> 15, 'filtro' => '', 'orden'=> ' 1 desc'); if(isset($_POST['buscar']) && !empty($_POST['buscar'])){ 
				 $opciones['buscar'] = $_POST['buscar']; 
			 } 
			 $arr = usuarios::lista($opciones); 

			 if(count($arr['datos']) > 0) { 
			 $html = '<table cellpadding="3" cellspacing="0" class="grilla">'; 
			 $html.= '<thead><tr>'; 
				 $html.= '<th>Id usuario</th>'; 
				 $html.= '<th>Usuario</th>'; 
				 $html.= '<th>Nombre</th>'; 
				 $html.= '<th>Apellido</th>'; 
				 $html.= '<th>Email</th>'; 
				 $html.= '<th>Id rol</th>'; 
				 $html.= '<th>&nbsp;</th>'; 
			 $html.= '</tr></thead>'; 
			 $estilo_fila = ''; 
			 foreach ($arr['datos'] as $row) { 
				 $estilo_fila = ($estilo_fila == '') ? ' class = "alt-row" ' : ''; 
				 $html.= '<tr' . $estilo_fila . '>'; 
					 $html.= '<td>'.$row['id_usuario'].'</td>'; 
					 $html.= '<td>'.$row['usuario'].'</td>'; 
					 $html.= '<td>'.$row['nombre'].'</td>'; 
					 $html.= '<td>'.$row['apellido'].'</td>'; 
					 $html.= '<td>'.$row['email'].'</td>'; 
					 $html.= '<td>'.$row['roles_rol'].'</td>'; 
					 $html.= '<td style="text-align:center; white-space:nowrap;">'; 
					 if($nivel_acceso['modificacion'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="editaRegistro(\'' . $row['id_usuario'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/pencil.png" title="Edicion" alt="Edicion" /></a> '; 
					 if($nivel_acceso['baja'] == 'S') 
						 $html.= '<a href="javascript:;" onclick="eliminaRegistro(\'' . $row['id_usuario'] . '\')"><img style=\'vertical-align:middle;\' src="images/icons/buttons/cross.png" title="Eliminar" alt="Eliminar" /></a>'; 
					 $html.= '</td>'; 
				 $html.= '</tr>'; 
			 } 
			 $html.= '</table>'; 
			 $pagerCtrl = new PagerControl($arr['cant_paginas']); 
			 $pagerCtrl->useJavaScript(true); 
			 $pagerCtrl->set_jsFunctionName('cargaGrilla'); 
			 $pagerCtrl->set_currentPage($pagina_actual); 
			 $html .= $pagerCtrl->get_control(); 
			 } else { 
				 $html = "<div class='notification information png_bg'><div>No hay registros.</div></div>"; 
			 } 
			 die($html); 
			 break; 
 	 } 
 
} 
 ?> 