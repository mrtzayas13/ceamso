<?php 

/** 
 * publicaciones 
 * 
 * 05/07/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class publicaciones { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_area_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true)); 
	 protected $_titulo_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_titulo_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_esp = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_eng = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_archivo = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_activo = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array()); 

	 /** 
	  * Crea una nueva instacia del objeto publicaciones. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_area_id($p_area_id){ 
		 if(!DataValidator::validate($p_area_id, $this->_area_id['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>area_id</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_area_id['value'] = $p_area_id; 
	 } 
	 public function get_area_id(){ return $this->_area_id['value']; } 

	 public function set_titulo_esp($p_titulo_esp){ 
		 if(!DataValidator::validate($p_titulo_esp, $this->_titulo_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_esp['value'] = $p_titulo_esp; 
	 } 
	 public function get_titulo_esp(){ return $this->_titulo_esp['value']; } 

	 public function set_titulo_eng($p_titulo_eng){ 
		 if(!DataValidator::validate($p_titulo_eng, $this->_titulo_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_eng['value'] = $p_titulo_eng; 
	 } 
	 public function get_titulo_eng(){ return $this->_titulo_eng['value']; } 

	 public function set_descripcion_esp($p_descripcion_esp){ 
		 if(!DataValidator::validate($p_descripcion_esp, $this->_descripcion_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_esp['value'] = $p_descripcion_esp; 
	 } 
	 public function get_descripcion_esp(){ return $this->_descripcion_esp['value']; } 

	 public function set_descripcion_eng($p_descripcion_eng){ 
		 if(!DataValidator::validate($p_descripcion_eng, $this->_descripcion_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_eng['value'] = $p_descripcion_eng; 
	 } 
	 public function get_descripcion_eng(){ return $this->_descripcion_eng['value']; } 

	 public function set_archivo($p_archivo){ 
		 if(!DataValidator::validate($p_archivo, $this->_archivo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>archivo</strong>:<br/>' . DataValidator::get_error_text()); 
		 // Si existe un archivo ya cargado se elimina del disco en caso de que el valor nuevo sea diferente. 
 		 if(strlen($this->get_archivo()) > 0 && $this->get_archivo() != $p_archivo){ 
 			 if(file_exists(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $this->get_archivo())){ 
 				 unlink(CONF_ABS_UPLOAD_PATH . '/publicaciones/' . $this->get_archivo()); 
 			 } 
 		 } 
 		 $this->_archivo['value'] = $p_archivo; 
	 } 
	 public function get_archivo(){ return $this->_archivo['value']; } 

	 public function set_activo($p_activo){ 
		 if(!DataValidator::validate($p_activo, $this->_activo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>activo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_activo['value'] = $p_activo; 
	 } 
	 public function get_activo(){ return $this->_activo['value']; } 

	 public function set_fecha_creacion($p_fecha_creacion){ 
		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 
	 } 
	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM publicaciones WHERE id = {id}'); 
		 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_area_id['value'] = $datos[0]['area_id']; 
			 $this->_titulo_esp['value'] = $datos[0]['titulo_esp']; 
			 $this->_titulo_eng['value'] = $datos[0]['titulo_eng']; 
			 $this->_descripcion_esp['value'] = $datos[0]['descripcion_esp']; 
			 $this->_descripcion_eng['value'] = $datos[0]['descripcion_eng']; 
			 $this->_archivo['value'] = $datos[0]['archivo']; 
			 $this->_activo['value'] = $datos[0]['activo']; 
			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_area_id['value'] = null; 
			 $this->_titulo_esp['value'] = ''; 
			 $this->_titulo_eng['value'] = ''; 
			 $this->_descripcion_esp['value'] = null; 
			 $this->_descripcion_eng['value'] = null; 
			 $this->_archivo['value'] = ''; 
			 $this->_activo['value'] = null; 
			 $this->_fecha_creacion['value'] = null; 
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO publicaciones(area_id, titulo_esp, titulo_eng, descripcion_esp, descripcion_eng, archivo, activo)VALUES({area_id}, {titulo_esp}, {titulo_eng}, {descripcion_esp}, {descripcion_eng}, {archivo}, {activo})'); 
			 $query->addParam('area_id', $this->_area_id['value'], $this->_area_id['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('descripcion_esp', $this->_descripcion_esp['value'], $this->_descripcion_esp['datatype']); 
			 $query->addParam('descripcion_eng', $this->_descripcion_eng['value'], $this->_descripcion_eng['datatype']); 
			 $query->addParam('archivo', $this->_archivo['value'], $this->_archivo['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE publicaciones SET area_id = {area_id}, titulo_esp = {titulo_esp}, titulo_eng = {titulo_eng}, descripcion_esp = {descripcion_esp}, descripcion_eng = {descripcion_eng}, archivo = {archivo}, activo = {activo} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('area_id', $this->_area_id['value'], $this->_area_id['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('descripcion_esp', $this->_descripcion_esp['value'], $this->_descripcion_esp['datatype']); 
			 $query->addParam('descripcion_eng', $this->_descripcion_eng['value'], $this->_descripcion_eng['datatype']); 
			 $query->addParam('archivo', $this->_archivo['value'], $this->_archivo['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM publicaciones WHERE id = {id}'); 
		 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' publicaciones LEFT JOIN areas ON publicaciones.area_id = areas.id  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  publicaciones.* , areas.area_esp as areas_area_esp , areas.area_eng as areas_area_eng  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "publicaciones.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "areas.area_esp LIKE " . DBManager::formatSQLValue('%'.$p_valor.'%') . " "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(publicaciones.titulo_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(publicaciones.titulo_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(publicaciones.descripcion_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(publicaciones.descripcion_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(publicaciones.archivo) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(CAST(publicaciones.activo AS \"varchar\"(100))) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>