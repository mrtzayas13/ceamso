<?php 

/** 
 * archivo_licitaciones 
 * 
 * 05/07/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class archivo_licitaciones { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_nombre_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_nombre_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_archivo = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array()); 
	 protected $_licitacion_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true)); 

	 /** 
	  * Crea una nueva instacia del objeto archivo_licitaciones. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_nombre_esp($p_nombre_esp){ 
		 if(!DataValidator::validate($p_nombre_esp, $this->_nombre_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>nombre_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_nombre_esp['value'] = $p_nombre_esp; 
	 } 
	 public function get_nombre_esp(){ return $this->_nombre_esp['value']; } 

	 public function set_nombre_eng($p_nombre_eng){ 
		 if(!DataValidator::validate($p_nombre_eng, $this->_nombre_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>nombre_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_nombre_eng['value'] = $p_nombre_eng; 
	 } 
	 public function get_nombre_eng(){ return $this->_nombre_eng['value']; } 

	 public function set_archivo($p_archivo){ 
		 if(!DataValidator::validate($p_archivo, $this->_archivo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>archivo</strong>:<br/>' . DataValidator::get_error_text()); 
		 // Si existe un archivo ya cargado se elimina del disco en caso de que el valor nuevo sea diferente. 
 		 if(strlen($this->get_archivo()) > 0 && $this->get_archivo() != $p_archivo){ 
 			 if(file_exists(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $this->get_archivo())){ 
 				 unlink(CONF_ABS_UPLOAD_PATH . '/archivo_licitaciones/' . $this->get_archivo()); 
 			 } 
 		 } 
 		 $this->_archivo['value'] = $p_archivo; 
	 } 
	 public function get_archivo(){ return $this->_archivo['value']; } 

	 public function set_fecha_creacion($p_fecha_creacion){ 
		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 
	 } 
	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 

	 public function set_licitacion_id($p_licitacion_id){ 
		 if(!DataValidator::validate($p_licitacion_id, $this->_licitacion_id['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>licitacion_id</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_licitacion_id['value'] = $p_licitacion_id; 
	 } 
	 public function get_licitacion_id(){ return $this->_licitacion_id['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM archivo_licitaciones WHERE id = {id}'); 
		 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_nombre_esp['value'] = $datos[0]['nombre_esp']; 
			 $this->_nombre_eng['value'] = $datos[0]['nombre_eng']; 
			 $this->_archivo['value'] = $datos[0]['archivo']; 
			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 
			 $this->_licitacion_id['value'] = $datos[0]['licitacion_id']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_nombre_esp['value'] = ''; 
			 $this->_nombre_eng['value'] = ''; 
			 $this->_archivo['value'] = ''; 
			 $this->_fecha_creacion['value'] = null; 
			 $this->_licitacion_id['value'] = null; 
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO archivo_licitaciones(nombre_esp, nombre_eng, archivo, licitacion_id)VALUES({nombre_esp}, {nombre_eng}, {archivo},  {licitacion_id})'); 
			 $query->addParam('nombre_esp', $this->_nombre_esp['value'], $this->_nombre_esp['datatype']); 
			 $query->addParam('nombre_eng', $this->_nombre_eng['value'], $this->_nombre_eng['datatype']); 
			 $query->addParam('archivo', $this->_archivo['value'], $this->_archivo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
			 $query->addParam('licitacion_id', $this->_licitacion_id['value'], $this->_licitacion_id['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE archivo_licitaciones SET nombre_esp = {nombre_esp}, nombre_eng = {nombre_eng}, archivo = {archivo}, licitacion_id = {licitacion_id} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('nombre_esp', $this->_nombre_esp['value'], $this->_nombre_esp['datatype']); 
			 $query->addParam('nombre_eng', $this->_nombre_eng['value'], $this->_nombre_eng['datatype']); 
			 $query->addParam('archivo', $this->_archivo['value'], $this->_archivo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
			 $query->addParam('licitacion_id', $this->_licitacion_id['value'], $this->_licitacion_id['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM archivo_licitaciones WHERE id = {id}'); 
		 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' archivo_licitaciones LEFT JOIN licitaciones ON archivo_licitaciones.licitacion_id = licitaciones.id  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  archivo_licitaciones.* , licitaciones.codigo as licitaciones_codigo , licitaciones.titulo_esp as licitaciones_titulo_esp , licitaciones.titulo_eng as licitaciones_titulo_eng  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "archivo_licitaciones.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(archivo_licitaciones.nombre_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(archivo_licitaciones.nombre_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(archivo_licitaciones.archivo) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "licitaciones.codigo LIKE " . DBManager::formatSQLValue('%'.$p_valor.'%') . " "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>