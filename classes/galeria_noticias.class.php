<?php 



/** 

 * galeria_noticias 

 * 

 * 18/06/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 

 * @version	1.0 

 * @autor 		PHPGen - version 2.0

 */ 



class galeria_noticias { 



	 protected $_dbmanager = null; 



	 // Propiedades del objeto que representan los campos de la tabla. 

	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 

	 protected $_imagen = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 

	 protected $_noticia_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true)); 

	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array()); 



	 /** 

	  * Crea una nueva instacia del objeto galeria_noticias. Inicializa las propiedades del objeto.

	  */ 

	 public function __construct() { 

		 $this->_dbmanager = new DBManager(); 

	 } 



	 /************************************ 

	  * PROPIEDADES PUBLICAS DE LA CLASE * 

	  ************************************/ 



	 public function set_id($p_id){ 

		 $this->_id['value'] = $p_id; 

	 } 

	 public function get_id(){ return $this->_id['value']; } 



	 public function set_imagen($p_imagen){ 

		 if(!DataValidator::validate($p_imagen, $this->_imagen['validators'])) 

			 throw new Exception('Error al establecer el valor de <strong>imagen</strong>:<br/>' . DataValidator::get_error_text()); 

		 // Si existe un archivo ya cargado se elimina del disco en caso de que el valor nuevo sea diferente. 

 		 if(strlen($this->get_imagen()) > 0 && $this->get_imagen() != $p_imagen){ 

 			 if(file_exists(CONF_ABS_UPLOAD_PATH . '/galeria_noticias/' . $this->get_imagen())){ 

 				 unlink(CONF_ABS_UPLOAD_PATH . '/galeria_noticias/' . $this->get_imagen()); 

 			 } 

 		 } 

 		 $this->_imagen['value'] = $p_imagen; 

	 } 

	 public function get_imagen(){ return $this->_imagen['value']; } 



	 public function upload_imagen($p_posted_file, $p_bgcolor = null) { 



		 // Verificia si existe el archivo.

		 if(!is_uploaded_file($p_posted_file['tmp_name'])){ return false; } 



		 // Valida la existencia de los directorios.

		 if(!is_dir(CONF_ABS_UPLOAD_PATH)) 

			 mkdir(CONF_ABS_UPLOAD_PATH); 

		 if(!is_dir(CONF_ABS_UPLOAD_PATH . '/galeria_noticias/')) 

			  mkdir(CONF_ABS_UPLOAD_PATH . '/galeria_noticias/'); 



		 $extension  =  strtolower(substr($p_posted_file['name'], strrpos($p_posted_file['name'], '.') + 1)); 

		 $nom_imagen =  time(); 

		 $proceso_exitoso = false; 



		 try{ 

			 $img = new ImageManager(); 

			 $img->setOutputFormat(strtoupper($extension)); 

			 $img->fileToResize($p_posted_file['tmp_name']); 

			 $img->setAlignment('center'); 

			 if($p_bgcolor != null && is_array($p_bgcolor)){ 

				 $img->setBackgroundColor($p_bgcolor); 

			 }else{ 

				 if(strtoupper($extension) == 'PNG'){ 

					 $img->setTransparency(array(0, 0, 0), true); 

				 }else{ 

					 $img->setBackgroundColor(array(255, 255, 255)); 

				 } 

			 } 

			 $img->setTarget(CONF_ABS_UPLOAD_PATH . '/galeria_noticias/'); 



			 $img->setSize(370, 250); 

			 $img->setOutputFile($nom_imagen); 

			 $img->Resize(); 

			 $img->setSize(1140);

			 $img->setOutputFile('g__' . $nom_imagen); 

			 $img->Resize(); 



			 $this->set_imagen($nom_imagen . '.' . $extension); 

 			 $proceso_exitoso = true; 

 

		 }catch(Exception $exc){ 

			 EventLog::writeEntry($exc->getMessage(), 'error'); 

		 } 

		 return $proceso_exitoso; 

 	 } 



	 public function set_noticia_id($p_noticia_id){ 

		 if(!DataValidator::validate($p_noticia_id, $this->_noticia_id['validators'])) 

			 throw new Exception('Error al establecer el valor de <strong>noticia_id</strong>:<br/>' . DataValidator::get_error_text()); 

		 $this->_noticia_id['value'] = $p_noticia_id; 

	 } 

	 public function get_noticia_id(){ return $this->_noticia_id['value']; } 



	 public function set_fecha_creacion($p_fecha_creacion){ 

		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 

			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 

		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 

	 } 

	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 



	 /************************************************** 

	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 

	  **************************************************/ 



	 /** 

	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.

	  * @param Integer $p_id ID del registro a cargar. 

	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 

	  */ 

	 public function carga($p_id) { 

		 $query = new DBQuery('SELECT * FROM galeria_noticias WHERE id = {id}'); 

			 $query->addParam('id', $p_id, $this->_id['datatype']); 

		 $datos = $this->_dbmanager->executeQuery($query); 

		 if(count($datos) > 0) { 

			 $this->_id['value'] = $datos[0]['id']; 

			 $this->_imagen['value'] = $datos[0]['imagen']; 

			 $this->_noticia_id['value'] = $datos[0]['noticia_id']; 

			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 

		 }else{ 

			 $this->_id['value'] = null; 

			 $this->_imagen['value'] = ''; 

			 $this->_noticia_id['value'] = null; 

			 $this->_fecha_creacion['value'] = null; 

		 } 

		 return ($this->_id['value'] == null) ? false : true; 

	 } 



	 /** 

	  * Guarda la información de las propiedades en la BD.

	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 

	  */ 

	 public function guarda() { 

		 if($this->_id['value'] == null) { 

			 $query = new DBQuery('INSERT INTO galeria_noticias(imagen, noticia_id)VALUES({imagen}, {noticia_id})'); 

			 $query->addParam('imagen', $this->_imagen['value'], $this->_imagen['datatype']); 

			 $query->addParam('noticia_id', $this->_noticia_id['value'], $this->_noticia_id['datatype']); 

			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 

		 }else{ 

			 $query = new DBQuery('UPDATE galeria_noticias SET imagen = {imagen}, noticia_id = {noticia_id} WHERE id = {id}'); 

			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 

			 $query->addParam('imagen', $this->_imagen['value'], $this->_imagen['datatype']); 

			 $query->addParam('noticia_id', $this->_noticia_id['value'], $this->_noticia_id['datatype']); 

			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 

		 } 

		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 

		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 

		 return ($filas_afectadas == -1)?false:true; 

	 } 



	 /** 

	  * Elimina un registro de la base de datos.

	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 

	  */ 

	 public function elimina() { 

		 $query = new DBQuery('DELETE FROM galeria_noticias WHERE id = {id}'); 

			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 

		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 

		 if($filas_afectadas == -1) { 

			 return false; 

		 } 

		 return true; 

	 } 



	 /** 

	  * Recupera lista de registros de la tabla.

	  * @param Array Lista de opciones utilizadas para recuperar los datos. 

	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 

	  * @return Array Datos de la tabla. 

	  */ 

	 public static function lista($p_opciones) { 



		 $dbmanager = new DBManager(); 

		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 

		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 

		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 

		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 

		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 

		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 



		 $sql_from = ' galeria_noticias LEFT JOIN noticias ON galeria_noticias.noticia_id = noticias.id  '; 



		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 

		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 

		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 



		 $sql = 'SELECT  galeria_noticias.* , noticias.titulo_esp as noticias_titulo , noticias.copete_esp as noticias_copete , noticias.youtube_url as noticias_youtube_url  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 

		 if($paginar === true){ 

			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 

		 } 

		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 



		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 

		 return $retorno; 

	 } 



	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 

		 $filtro = ''; 

		 if(preg_match('/^\d+$/', $p_valor)) { 

			 $filtro.= ($filtro == '')?' ': ' OR '; 

			 $filtro.= "galeria_noticias.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 

		 } 

		 $filtro.= ($filtro == '')?' ': ' OR '; 

		 $filtro.= "LOWER(galeria_noticias.imagen) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 

		 $filtro.= ($filtro == '')?' ': ' OR '; 

		 $filtro.= "noticias.titulo LIKE " . DBManager::formatSQLValue('%'.$p_valor.'%') . " "; 

		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 

		 return $filtro; 

	 } 



} 

?>