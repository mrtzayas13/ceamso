<?php 

/** 
 * blog 
 * 
 * 19/06/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class blog { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_titulo_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_titulo_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_copete_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_copete_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_esp = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_eng = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_youtube_url = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_activo = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array()); protected $_usuario_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true)); 

	 /** 
	  * Crea una nueva instacia del objeto blog. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_titulo_esp($p_titulo_esp){ 
		 if(!DataValidator::validate($p_titulo_esp, $this->_titulo_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_esp['value'] = $p_titulo_esp; 
	 } 
	 public function get_titulo_esp(){ return $this->_titulo_esp['value']; } 

	 public function set_titulo_eng($p_titulo_eng){ 
		 if(!DataValidator::validate($p_titulo_eng, $this->_titulo_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_eng['value'] = $p_titulo_eng; 
	 } 
	 public function get_titulo_eng(){ return $this->_titulo_eng['value']; } 

	 public function set_copete_esp($p_copete_esp){ 
		 if(!DataValidator::validate($p_copete_esp, $this->_copete_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>copete_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_copete_esp['value'] = $p_copete_esp; 
	 } 
	 public function get_copete_esp(){ return $this->_copete_esp['value']; } 

	 public function set_copete_eng($p_copete_eng){ 
		 if(!DataValidator::validate($p_copete_eng, $this->_copete_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>copete_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_copete_eng['value'] = $p_copete_eng; 
	 } 
	 public function get_copete_eng(){ return $this->_copete_eng['value']; } 

	 public function set_descripcion_esp($p_descripcion_esp){ 
		 if(!DataValidator::validate($p_descripcion_esp, $this->_descripcion_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_esp['value'] = $p_descripcion_esp; 
	 } 
	 public function get_descripcion_esp(){ return $this->_descripcion_esp['value']; } 

	 public function set_descripcion_eng($p_descripcion_eng){ 
		 if(!DataValidator::validate($p_descripcion_eng, $this->_descripcion_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_eng['value'] = $p_descripcion_eng; 
	 } 
	 public function get_descripcion_eng(){ return $this->_descripcion_eng['value']; } 

	 public function set_youtube_url($p_youtube_url){ 
		 if(!DataValidator::validate($p_youtube_url, $this->_youtube_url['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>youtube_url</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_youtube_url['value'] = $p_youtube_url; 
	 } 
	 public function get_youtube_url(){ return $this->_youtube_url['value']; } 

	 public function set_activo($p_activo){ 
		 if(!DataValidator::validate($p_activo, $this->_activo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>activo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_activo['value'] = $p_activo; 
	 } 
	 public function get_activo(){ return $this->_activo['value']; } 

	 public function set_fecha_creacion($p_fecha_creacion){ 
		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 
	 } 
	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 

	 public function set_usuario_id($p_usuario_id){ 
		 if(!DataValidator::validate($p_usuario_id, $this->_usuario_id['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>usuario_id</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_usuario_id['value'] = $p_usuario_id; 
	 } 
	 public function get_usuario_id(){ return $this->_usuario_id['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM blog WHERE id = {id}'); 
			 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_titulo_esp['value'] = $datos[0]['titulo_esp']; 
			 $this->_titulo_eng['value'] = $datos[0]['titulo_eng']; 
			 $this->_copete_esp['value'] = $datos[0]['copete_esp']; 
			 $this->_copete_eng['value'] = $datos[0]['copete_eng']; 
			 $this->_descripcion_esp['value'] = $datos[0]['descripcion_esp']; 
			 $this->_descripcion_eng['value'] = $datos[0]['descripcion_eng']; 
			 $this->_youtube_url['value'] = $datos[0]['youtube_url']; 
			 $this->_activo['value'] = $datos[0]['activo']; 
			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 
			 $this->_usuario_id['value'] = $datos[0]['usuario_id']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_titulo_esp['value'] = ''; 
			 $this->_titulo_eng['value'] = ''; 
			 $this->_copete_esp['value'] = ''; 
			 $this->_copete_eng['value'] = ''; 
			 $this->_descripcion_esp['value'] = null; 
			 $this->_descripcion_eng['value'] = null; 
			 $this->_youtube_url['value'] = ''; 
			 $this->_activo['value'] = null; 
			 $this->_fecha_creacion['value'] = null; 
			 $this->_usuario_id['value'] = null; 
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO blog(titulo_esp, titulo_eng, copete_esp, copete_eng, descripcion_esp, descripcion_eng, youtube_url, activo, usuario_id)VALUES({titulo_esp}, {titulo_eng}, {copete_esp}, {copete_eng}, {descripcion_esp}, {descripcion_eng}, {youtube_url}, {activo}, {usuario_id})'); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('copete_esp', $this->_copete_esp['value'], $this->_copete_esp['datatype']); 
			 $query->addParam('copete_eng', $this->_copete_eng['value'], $this->_copete_eng['datatype']); 
			 $query->addParam('descripcion_esp', $this->_descripcion_esp['value'], $this->_descripcion_esp['datatype']); 
			 $query->addParam('descripcion_eng', $this->_descripcion_eng['value'], $this->_descripcion_eng['datatype']); 
			 $query->addParam('youtube_url', $this->_youtube_url['value'], $this->_youtube_url['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
			 $query->addParam('usuario_id', $this->_usuario_id['value'], $this->_usuario_id['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE blog SET titulo_esp = {titulo_esp}, titulo_eng = {titulo_eng}, copete_esp = {copete_esp}, copete_eng = {copete_eng}, descripcion_esp = {descripcion_esp}, descripcion_eng = {descripcion_eng}, youtube_url = {youtube_url}, activo = {activo}, usuario_id = {usuario_id} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('copete_esp', $this->_copete_esp['value'], $this->_copete_esp['datatype']); 
			 $query->addParam('copete_eng', $this->_copete_eng['value'], $this->_copete_eng['datatype']); 
			 $query->addParam('descripcion_esp', $this->_descripcion_esp['value'], $this->_descripcion_esp['datatype']); 
			 $query->addParam('descripcion_eng', $this->_descripcion_eng['value'], $this->_descripcion_eng['datatype']); 
			 $query->addParam('youtube_url', $this->_youtube_url['value'], $this->_youtube_url['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
			 $query->addParam('usuario_id', $this->_usuario_id['value'], $this->_usuario_id['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM blog WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' blog LEFT JOIN usuarios ON blog.usuario_id = usuarios.id_usuario  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  blog.* , usuarios.usuario as usuarios_usuario , usuarios.clave as usuarios_clave , usuarios.nombre as usuarios_nombre , usuarios.apellido as usuarios_apellido , usuarios.email as usuarios_email  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "blog.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.titulo_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.titulo_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.copete_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.copete_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.descripcion_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.descripcion_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(blog.youtube_url) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 //$filtro.= ($filtro == '')?' ': ' OR '; 
		 //$filtro.= "LOWER(CAST(blog.activo AS \"varchar\"(100))) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "usuarios.usuario LIKE " . DBManager::formatSQLValue('%'.$p_valor.'%') . " "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>