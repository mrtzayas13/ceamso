<?php
    include('inc/config.inc.php');
    define("SITENAME", "CEAMSO");
    define("GRALKEYS", "ceamso, democracia, sustentabilidad, sustentable, desarrollo");
    define("URL", substr_replace("http://" . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']) ,"",-1) ); 
    define("FILENAME", basename($_SERVER['SCRIPT_NAME'])); 

    $db = new DBManager();

    //BLOG
	$qblog = new DBQuery("SELECT * FROM blog n where n.activo = 'SI' order by id desc limit 3");
	$blog = $db->executeQuery($qblog);

	//NOTICIAS
	$qnoticias = new DBQuery("SELECT * FROM noticias n where n.activo = 'SI' order by fecha_creacion desc limit 3");
	$noticias = $db->executeQuery($qnoticias);

	//LLAMADOS
	$qlicitaciones = new DBQuery("SELECT * FROM licitaciones n where n.activo = 'SI' order by id desc limit 3");
	$licitaciones = $db->executeQuery($qlicitaciones);

	//VIDEOS
	$qvideos = new DBQuery("SELECT * FROM videos n where n.activo = 'SI' order by id desc limit 4");
	$videos = $db->executeQuery($qvideos);

	//AREAS
	$qareas = new DBQuery("SELECT * FROM areas n where n.activo = 'SI' order by area{$idioma}");
	$areas = $db->executeQuery($qareas);

?>
<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es-ES"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="es-ES"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="es-ES"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es-ES"> <!--<![endif]-->