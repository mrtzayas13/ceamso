<?php 

/** 
 * newsletter 
 * 
 * 18/10/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class newsletter { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_nombre = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_email = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_recibir_llamados = Array('value' => null, 'datatype' => DBType::String, 'validators' => array('digits' => true)); 
	 protected $_recibir_novedades = Array('value' => null, 'datatype' => DBType::String, 'validators' => array('digits' => true)); 

	 /** 
	  * Crea una nueva instacia del objeto newsletter. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_nombre($p_nombre){ 
		 if(!DataValidator::validate($p_nombre, $this->_nombre['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>nombre</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_nombre['value'] = $p_nombre; 
	 } 
	 public function get_nombre(){ return $this->_nombre['value']; } 

	 public function set_email($p_email){ 
		 if(!DataValidator::validate($p_email, $this->_email['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>email</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_email['value'] = $p_email; 
	 } 
	 public function get_email(){ return $this->_email['value']; } 

	 public function set_recibir_llamados($p_recibir_llamados){ 
		 if(!DataValidator::validate($p_recibir_llamados, $this->_recibir_llamados['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>recibir_llamados</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_recibir_llamados['value'] = $p_recibir_llamados; 
	 } 
	 public function get_recibir_llamados(){ return $this->_recibir_llamados['value']; } 

	 public function set_recibir_novedades($p_recibir_novedades){ 
		 if(!DataValidator::validate($p_recibir_novedades, $this->_recibir_novedades['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>recibir_novedades</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_recibir_novedades['value'] = $p_recibir_novedades; 
	 } 
	 public function get_recibir_novedades(){ return $this->_recibir_novedades['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM newsletter WHERE id = {id}'); 
		 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_nombre['value'] = $datos[0]['nombre']; 
			 $this->_email['value'] = $datos[0]['email']; 
			 $this->_recibir_llamados['value'] = $datos[0]['recibir_llamados']; 
			 $this->_recibir_novedades['value'] = $datos[0]['recibir_novedades']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_nombre['value'] = ''; 
			 $this->_email['value'] = ''; 
			 $this->_recibir_llamados['value'] = null; 
			 $this->_recibir_novedades['value'] = null; 
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO newsletter(nombre, email, recibir_llamados, recibir_novedades)VALUES({nombre}, {email}, {recibir_llamados}, {recibir_novedades})'); 
			 $query->addParam('nombre', $this->_nombre['value'], $this->_nombre['datatype']); 
			 $query->addParam('email', $this->_email['value'], $this->_email['datatype']); 
			 $query->addParam('recibir_llamados', $this->_recibir_llamados['value'], $this->_recibir_llamados['datatype']); 
			 $query->addParam('recibir_novedades', $this->_recibir_novedades['value'], $this->_recibir_novedades['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE newsletter SET nombre = {nombre}, email = {email}, recibir_llamados = {recibir_llamados}, recibir_novedades = {recibir_novedades} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('nombre', $this->_nombre['value'], $this->_nombre['datatype']); 
			 $query->addParam('email', $this->_email['value'], $this->_email['datatype']); 
			 $query->addParam('recibir_llamados', $this->_recibir_llamados['value'], $this->_recibir_llamados['datatype']); 
			 $query->addParam('recibir_novedades', $this->_recibir_novedades['value'], $this->_recibir_novedades['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM newsletter WHERE id = {id}'); 
		 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' newsletter  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  newsletter.*  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "newsletter.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(newsletter.nombre) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(newsletter.email) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>