<?php 

/** 
 * historias_exitos 
 * 
 * 23/10/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class historias_exitos { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_nombre = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_institucion_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_institucion_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_titulo_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_titulo_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_cita_esp = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_cita_eng = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_copete_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_copete_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_largo_esp = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_largo_eng = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_imagen = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_activo = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array('required' => true)); 

	 /** 
	  * Crea una nueva instacia del objeto historias_exitos. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_nombre($p_nombre){ 
		 if(!DataValidator::validate($p_nombre, $this->_nombre['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>nombre</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_nombre['value'] = $p_nombre; 
	 } 
	 public function get_nombre(){ return $this->_nombre['value']; } 

	 public function set_institucion_esp($p_institucion_esp){ 
		 if(!DataValidator::validate($p_institucion_esp, $this->_institucion_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>institucion_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_institucion_esp['value'] = $p_institucion_esp; 
	 } 
	 public function get_institucion_esp(){ return $this->_institucion_esp['value']; } 

	 public function set_institucion_eng($p_institucion_eng){ 
		 if(!DataValidator::validate($p_institucion_eng, $this->_institucion_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>institucion_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_institucion_eng['value'] = $p_institucion_eng; 
	 } 
	 public function get_institucion_eng(){ return $this->_institucion_eng['value']; } 

	 public function set_titulo_esp($p_titulo_esp){ 
		 if(!DataValidator::validate($p_titulo_esp, $this->_titulo_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_esp['value'] = $p_titulo_esp; 
	 } 
	 public function get_titulo_esp(){ return $this->_titulo_esp['value']; } 

	 public function set_titulo_eng($p_titulo_eng){ 
		 if(!DataValidator::validate($p_titulo_eng, $this->_titulo_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_eng['value'] = $p_titulo_eng; 
	 } 
	 public function get_titulo_eng(){ return $this->_titulo_eng['value']; } 

	 public function set_cita_esp($p_cita_esp){ 
		 if(!DataValidator::validate($p_cita_esp, $this->_cita_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>cita_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_cita_esp['value'] = $p_cita_esp; 
	 } 
	 public function get_cita_esp(){ return $this->_cita_esp['value']; } 

	 public function set_cita_eng($p_cita_eng){ 
		 if(!DataValidator::validate($p_cita_eng, $this->_cita_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>cita_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_cita_eng['value'] = $p_cita_eng; 
	 } 
	 public function get_cita_eng(){ return $this->_cita_eng['value']; } 

	 public function set_copete_esp($p_copete_esp){ 
		 if(!DataValidator::validate($p_copete_esp, $this->_copete_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>copete_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_copete_esp['value'] = $p_copete_esp; 
	 } 
	 public function get_copete_esp(){ return $this->_copete_esp['value']; } 

	 public function set_copete_eng($p_copete_eng){ 
		 if(!DataValidator::validate($p_copete_eng, $this->_copete_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>copete_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_copete_eng['value'] = $p_copete_eng; 
	 } 
	 public function get_copete_eng(){ return $this->_copete_eng['value']; } 

	 public function set_descripcion_largo_esp($p_descripcion_largo_esp){ 
		 if(!DataValidator::validate($p_descripcion_largo_esp, $this->_descripcion_largo_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_largo_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_largo_esp['value'] = $p_descripcion_largo_esp; 
	 } 
	 public function get_descripcion_largo_esp(){ return $this->_descripcion_largo_esp['value']; } 

	 public function set_descripcion_largo_eng($p_descripcion_largo_eng){ 
		 if(!DataValidator::validate($p_descripcion_largo_eng, $this->_descripcion_largo_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_largo_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_largo_eng['value'] = $p_descripcion_largo_eng; 
	 } 
	 public function get_descripcion_largo_eng(){ return $this->_descripcion_largo_eng['value']; } 

	 public function set_imagen($p_imagen){ 
		 if(!DataValidator::validate($p_imagen, $this->_imagen['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>imagen</strong>:<br/>' . DataValidator::get_error_text()); 
		 // Si existe un archivo ya cargado se elimina del disco en caso de que el valor nuevo sea diferente. 
 		 if(strlen($this->get_imagen()) > 0 && $this->get_imagen() != $p_imagen){ 
 			 if(file_exists(CONF_ABS_UPLOAD_PATH . '/historias_exitos/' . $this->get_imagen())){ 
 				 unlink(CONF_ABS_UPLOAD_PATH . '/historias_exitos/' . $this->get_imagen()); 
 			 } 
 		 } 
 		 $this->_imagen['value'] = $p_imagen; 
	 } 
	 public function get_imagen(){ return $this->_imagen['value']; } 

	 public function upload_imagen($p_posted_file, $p_bgcolor = null) { 

		 // Verificia si existe el archivo.
		 if(!is_uploaded_file($p_posted_file['tmp_name'])){ return false; } 

		 // Valida la existencia de los directorios.
		 if(!is_dir(CONF_ABS_UPLOAD_PATH)) 
			 mkdir(CONF_ABS_UPLOAD_PATH); 
		 if(!is_dir(CONF_ABS_UPLOAD_PATH . '/historias_exitos/')) 
			  mkdir(CONF_ABS_UPLOAD_PATH . '/historias_exitos/'); 

		 $extension  =  strtolower(substr($p_posted_file['name'], strrpos($p_posted_file['name'], '.') + 1)); 
		 $nom_imagen =  time(); 
		 $proceso_exitoso = false; 

		 try{ 
			 $img = new ImageManager(); 
			 $img->setOutputFormat(strtoupper($extension)); 
			 $img->fileToResize($p_posted_file['tmp_name']); 
			 $img->setAlignment('center'); 
			 if($p_bgcolor != null && is_array($p_bgcolor)){ 
				 $img->setBackgroundColor($p_bgcolor); 
			 }else{ 
				 if(strtoupper($extension) == 'PNG'){ 
					 $img->setTransparency(array(0, 0, 0), true); 
				 }else{ 
					 $img->setBackgroundColor(array(255, 255, 255)); 
				 } 
			 } 
			 $img->setTarget(CONF_ABS_UPLOAD_PATH . '/historias_exitos/'); 

			 $img->setSize(220, 220); 
			 $img->setOutputFile($nom_imagen); 
			 $img->Resize(); 

			 $this->set_imagen($nom_imagen . '.' . $extension); 
 			 $proceso_exitoso = true; 
 
		 }catch(Exception $exc){ 
			 EventLog::writeEntry($exc->getMessage(), 'error'); 
		 } 
		 return $proceso_exitoso; 
 	 } 

	 public function set_activo($p_activo){ 
		 if(!DataValidator::validate($p_activo, $this->_activo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>activo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_activo['value'] = $p_activo; 
	 } 
	 public function get_activo(){ return $this->_activo['value']; } 

	 public function set_fecha_creacion($p_fecha_creacion){ 
		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 
	 } 
	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM historias_exitos WHERE id = {id}'); 
		 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_nombre['value'] = $datos[0]['nombre']; 
			 $this->_institucion_esp['value'] = $datos[0]['institucion_esp']; 
			 $this->_institucion_eng['value'] = $datos[0]['institucion_eng']; 
			 $this->_titulo_esp['value'] = $datos[0]['titulo_esp']; 
			 $this->_titulo_eng['value'] = $datos[0]['titulo_eng']; 
			 $this->_cita_esp['value'] = $datos[0]['cita_esp']; 
			 $this->_cita_eng['value'] = $datos[0]['cita_eng']; 
			 $this->_copete_esp['value'] = $datos[0]['copete_esp']; 
			 $this->_copete_eng['value'] = $datos[0]['copete_eng']; 
			 $this->_descripcion_largo_esp['value'] = $datos[0]['descripcion_largo_esp']; 
			 $this->_descripcion_largo_eng['value'] = $datos[0]['descripcion_largo_eng']; 
			 $this->_imagen['value'] = $datos[0]['imagen']; 
			 $this->_activo['value'] = $datos[0]['activo']; 
			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_nombre['value'] = ''; 
			 $this->_institucion_esp['value'] = ''; 
			 $this->_institucion_eng['value'] = ''; 
			 $this->_titulo_esp['value'] = ''; 
			 $this->_titulo_eng['value'] = ''; 
			 $this->_cita_esp['value'] = null; 
			 $this->_cita_eng['value'] = null; 
			 $this->_copete_esp['value'] = ''; 
			 $this->_copete_eng['value'] = ''; 
			 $this->_descripcion_largo_esp['value'] = null; 
			 $this->_descripcion_largo_eng['value'] = null; 
			 $this->_imagen['value'] = ''; 
			 $this->_activo['value'] = null; 
			 $this->_fecha_creacion['value'] = null; 
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO historias_exitos(nombre, institucion_esp, institucion_eng, titulo_esp, titulo_eng, cita_esp, cita_eng, copete_esp, copete_eng, descripcion_largo_esp, descripcion_largo_eng, imagen, activo, fecha_creacion)VALUES({nombre}, {institucion_esp}, {institucion_eng}, {titulo_esp}, {titulo_eng}, {cita_esp}, {cita_eng}, {copete_esp}, {copete_eng}, {descripcion_largo_esp}, {descripcion_largo_eng}, {imagen}, {activo}, {fecha_creacion})'); 
			 $query->addParam('nombre', $this->_nombre['value'], $this->_nombre['datatype']); 
			 $query->addParam('institucion_esp', $this->_institucion_esp['value'], $this->_institucion_esp['datatype']); 
			 $query->addParam('institucion_eng', $this->_institucion_eng['value'], $this->_institucion_eng['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('cita_esp', $this->_cita_esp['value'], $this->_cita_esp['datatype']); 
			 $query->addParam('cita_eng', $this->_cita_eng['value'], $this->_cita_eng['datatype']); 
			 $query->addParam('copete_esp', $this->_copete_esp['value'], $this->_copete_esp['datatype']); 
			 $query->addParam('copete_eng', $this->_copete_eng['value'], $this->_copete_eng['datatype']); 
			 $query->addParam('descripcion_largo_esp', $this->_descripcion_largo_esp['value'], $this->_descripcion_largo_esp['datatype']); 
			 $query->addParam('descripcion_largo_eng', $this->_descripcion_largo_eng['value'], $this->_descripcion_largo_eng['datatype']); 
			 $query->addParam('imagen', $this->_imagen['value'], $this->_imagen['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE historias_exitos SET nombre = {nombre}, institucion_esp = {institucion_esp}, institucion_eng = {institucion_eng}, titulo_esp = {titulo_esp}, titulo_eng = {titulo_eng}, cita_esp = {cita_esp}, cita_eng = {cita_eng}, copete_esp = {copete_esp}, copete_eng = {copete_eng}, descripcion_largo_esp = {descripcion_largo_esp}, descripcion_largo_eng = {descripcion_largo_eng}, imagen = {imagen}, activo = {activo}, fecha_creacion = {fecha_creacion} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('nombre', $this->_nombre['value'], $this->_nombre['datatype']); 
			 $query->addParam('institucion_esp', $this->_institucion_esp['value'], $this->_institucion_esp['datatype']); 
			 $query->addParam('institucion_eng', $this->_institucion_eng['value'], $this->_institucion_eng['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('cita_esp', $this->_cita_esp['value'], $this->_cita_esp['datatype']); 
			 $query->addParam('cita_eng', $this->_cita_eng['value'], $this->_cita_eng['datatype']); 
			 $query->addParam('copete_esp', $this->_copete_esp['value'], $this->_copete_esp['datatype']); 
			 $query->addParam('copete_eng', $this->_copete_eng['value'], $this->_copete_eng['datatype']); 
			 $query->addParam('descripcion_largo_esp', $this->_descripcion_largo_esp['value'], $this->_descripcion_largo_esp['datatype']); 
			 $query->addParam('descripcion_largo_eng', $this->_descripcion_largo_eng['value'], $this->_descripcion_largo_eng['datatype']); 
			 $query->addParam('imagen', $this->_imagen['value'], $this->_imagen['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM historias_exitos WHERE id = {id}'); 
		 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' historias_exitos  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  historias_exitos.*  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "historias_exitos.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.nombre) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.institucion_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.institucion_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.titulo_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.titulo_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.cita_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.cita_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.copete_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.copete_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.descripcion_largo_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.descripcion_largo_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(historias_exitos.imagen) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(CAST(historias_exitos.activo AS \"varchar\"(100))) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>