<?php 

/** 
 * staff 
 * 
 * 06/12/2018 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class staff { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_imagen = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_nombre_apellido = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_cargo = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_grupo = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_activo = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array('date' => true)); 

	 /** 
	  * Crea una nueva instacia del objeto staff. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_imagen($p_imagen){ 
		 if(!DataValidator::validate($p_imagen, $this->_imagen['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>imagen</strong>:<br/>' . DataValidator::get_error_text()); 
		 // Si existe un archivo ya cargado se elimina del disco en caso de que el valor nuevo sea diferente. 
 		 if(strlen($this->get_imagen()) > 0 && $this->get_imagen() != $p_imagen){ 
 			 if(file_exists(CONF_ABS_UPLOAD_PATH . '/staff/' . $this->get_imagen())){ 
 				 unlink(CONF_ABS_UPLOAD_PATH . '/staff/' . $this->get_imagen()); 
 			 } 
 		 } 
 		 $this->_imagen['value'] = $p_imagen; 
	 } 
	 public function get_imagen(){ return $this->_imagen['value']; } 

	 public function upload_imagen($p_posted_file, $p_bgcolor = null) { 

		 // Verificia si existe el archivo.
		 if(!is_uploaded_file($p_posted_file['tmp_name'])){ return false; } 

		 // Valida la existencia de los directorios.
		 if(!is_dir(CONF_ABS_UPLOAD_PATH)) 
			 mkdir(CONF_ABS_UPLOAD_PATH); 
		 if(!is_dir(CONF_ABS_UPLOAD_PATH . '/staff/')) 
			  mkdir(CONF_ABS_UPLOAD_PATH . '/staff/'); 

		 $extension  =  strtolower(substr($p_posted_file['name'], strrpos($p_posted_file['name'], '.') + 1)); 
		 $nom_imagen =  time(); 
		 $proceso_exitoso = false; 

		 try{ 
			 $img = new ImageManager(); 
			 $img->setOutputFormat(strtoupper($extension)); 
			 $img->fileToResize($p_posted_file['tmp_name']); 
			 $img->setAlignment('center'); 
			 if($p_bgcolor != null && is_array($p_bgcolor)){ 
				 $img->setBackgroundColor($p_bgcolor); 
			 }else{ 
				 if(strtoupper($extension) == 'PNG'){ 
					 $img->setTransparency(array(0, 0, 0), true); 
				 }else{ 
					 $img->setBackgroundColor(array(255, 255, 255)); 
				 } 
			 } 
			 $img->setTarget(CONF_ABS_UPLOAD_PATH . '/staff/'); 

			 $img->setSize(150, 150); 
			 $img->setOutputFile($nom_imagen); 
			 $img->Resize(); 
			 $img->setSize(500, 500); 
			 $img->setOutputFile('g__' . $nom_imagen); 
			 $img->Resize(); 

			 $this->set_imagen($nom_imagen . '.' . $extension); 
 			 $proceso_exitoso = true; 
 
		 }catch(Exception $exc){ 
			 EventLog::writeEntry($exc->getMessage(), 'error'); 
		 } 
		 return $proceso_exitoso; 
 	 } 

	 public function set_nombre_apellido($p_nombre_apellido){ 
		 if(!DataValidator::validate($p_nombre_apellido, $this->_nombre_apellido['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>nombre_apellido</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_nombre_apellido['value'] = $p_nombre_apellido; 
	 } 
	 public function get_nombre_apellido(){ return $this->_nombre_apellido['value']; } 

	 public function set_cargo($p_cargo){ 
		 if(!DataValidator::validate($p_cargo, $this->_cargo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>cargo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_cargo['value'] = $p_cargo; 
	 } 
	 public function get_cargo(){ return $this->_cargo['value']; } 

	 public function set_grupo($p_grupo){ 
		 if(!DataValidator::validate($p_grupo, $this->_grupo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>grupo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_grupo['value'] = $p_grupo; 
	 } 
	 public function get_grupo(){ return $this->_grupo['value']; } 

	 public function set_descripcion($p_descripcion){ 
		 if(!DataValidator::validate($p_descripcion, $this->_descripcion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion['value'] = $p_descripcion; 
	 } 
	 public function get_descripcion(){ return $this->_descripcion['value']; } 

	 public function set_activo($p_activo){ 
		 if(!DataValidator::validate($p_activo, $this->_activo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>activo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_activo['value'] = $p_activo; 
	 } 
	 public function get_activo(){ return $this->_activo['value']; } 

	 public function set_fecha_creacion($p_fecha_creacion){ 
		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 
	 } 
	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM staff WHERE id = {id}'); 
		 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_imagen['value'] = $datos[0]['imagen']; 
			 $this->_nombre_apellido['value'] = $datos[0]['nombre_apellido']; 
			 $this->_cargo['value'] = $datos[0]['cargo']; 
			 $this->_grupo['value'] = $datos[0]['grupo']; 
			 $this->_descripcion['value'] = $datos[0]['descripcion']; 
			 $this->_activo['value'] = $datos[0]['activo']; 
			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_imagen['value'] = ''; 
			 $this->_nombre_apellido['value'] = ''; 
			 $this->_cargo['value'] = ''; 
			 $this->_grupo['value'] = ''; 
			 $this->_descripcion['value'] = null; 
			 $this->_activo['value'] = null; 
			 $this->_fecha_creacion['value'] = null; 
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO staff(imagen, nombre_apellido, cargo, grupo, descripcion, activo)VALUES({imagen}, {nombre_apellido}, {cargo}, {grupo}, {descripcion}, {activo})'); 
			 $query->addParam('imagen', $this->_imagen['value'], $this->_imagen['datatype']); 
			 $query->addParam('nombre_apellido', $this->_nombre_apellido['value'], $this->_nombre_apellido['datatype']); 
			 $query->addParam('cargo', $this->_cargo['value'], $this->_cargo['datatype']); 
			 $query->addParam('grupo', $this->_grupo['value'], $this->_grupo['datatype']); 
			 $query->addParam('descripcion', $this->_descripcion['value'], $this->_descripcion['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE staff SET imagen = {imagen}, nombre_apellido = {nombre_apellido}, cargo = {cargo}, grupo = {grupo}, descripcion = {descripcion}, activo = {activo} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('imagen', $this->_imagen['value'], $this->_imagen['datatype']); 
			 $query->addParam('nombre_apellido', $this->_nombre_apellido['value'], $this->_nombre_apellido['datatype']); 
			 $query->addParam('cargo', $this->_cargo['value'], $this->_cargo['datatype']); 
			 $query->addParam('grupo', $this->_grupo['value'], $this->_grupo['datatype']); 
			 $query->addParam('descripcion', $this->_descripcion['value'], $this->_descripcion['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM staff WHERE id = {id}'); 
		 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' staff  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  staff.*  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "staff.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(staff.nombre_apellido) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(staff.cargo) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(staff.grupo) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 

		
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>