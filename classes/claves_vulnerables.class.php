<?php 

/** 
 * claves_vulnerables 
 * 
 * 18/06/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class claves_vulnerables { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id_clave_vulnerable = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_clave_vulnerable = Array('value' => '', 'datatype' => DBType::String, 'validators' => array('required' => true)); 

	 /** 
	  * Crea una nueva instacia del objeto claves_vulnerables. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id_clave_vulnerable($p_id_clave_vulnerable){ 
		 $this->_id_clave_vulnerable['value'] = $p_id_clave_vulnerable; 
	 } 
	 public function get_id_clave_vulnerable(){ return $this->_id_clave_vulnerable['value']; } 

	 public function set_clave_vulnerable($p_clave_vulnerable){ 
		 if(!DataValidator::validate($p_clave_vulnerable, $this->_clave_vulnerable['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>clave_vulnerable</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_clave_vulnerable['value'] = $p_clave_vulnerable; 
	 } 
	 public function get_clave_vulnerable(){ return $this->_clave_vulnerable['value']; } 

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id_clave_vulnerable ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id_clave_vulnerable) { 
		 $query = new DBQuery('SELECT * FROM claves_vulnerables WHERE id_clave_vulnerable = {id_clave_vulnerable}'); 
			 $query->addParam('id_clave_vulnerable', $p_id_clave_vulnerable, $this->_id_clave_vulnerable['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id_clave_vulnerable['value'] = $datos[0]['id_clave_vulnerable']; 
			 $this->_clave_vulnerable['value'] = $datos[0]['clave_vulnerable']; 
		 }else{ 
			 $this->_id_clave_vulnerable['value'] = null; 
			 $this->_clave_vulnerable['value'] = ''; 
		 } 
		 return ($this->_id_clave_vulnerable['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id_clave_vulnerable['value'] == null) { 
			 $query = new DBQuery('INSERT INTO claves_vulnerables(clave_vulnerable)VALUES({clave_vulnerable})'); 
			 $query->addParam('clave_vulnerable', $this->_clave_vulnerable['value'], $this->_clave_vulnerable['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE claves_vulnerables SET clave_vulnerable = {clave_vulnerable} WHERE id_clave_vulnerable = {id_clave_vulnerable}'); 
			 $query->addParam('id_clave_vulnerable', $this->_id_clave_vulnerable['value'], $this->_id_clave_vulnerable['datatype']); 
			 $query->addParam('clave_vulnerable', $this->_clave_vulnerable['value'], $this->_clave_vulnerable['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id_clave_vulnerable() == null) { $this->set_id_clave_vulnerable($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM claves_vulnerables WHERE id_clave_vulnerable = {id_clave_vulnerable}'); 
			 $query->addParam('id_clave_vulnerable', $this->_id_clave_vulnerable['value'], $this->_id_clave_vulnerable['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' claves_vulnerables  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  claves_vulnerables.*  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "claves_vulnerables.id_clave_vulnerable = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(claves_vulnerables.clave_vulnerable) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>