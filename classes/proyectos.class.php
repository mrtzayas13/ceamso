<?php 

/** 
 * proyectos 
 * 
 * 19/06/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		PHPGen - version 2.0
 */ 

class proyectos { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_area_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true)); 
	 protected $_tipo = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_titulo_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_titulo_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_copete_esp = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_copete_eng = Array('value' => '', 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_esp = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_descripcion_eng = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_inicio = Array('value' => null, 'datatype' => DBTYpe::Date, 'validators' => array('date' => true)); 
	 protected $_fecha_fin = Array('value' => null, 'datatype' => DBTYpe::Date, 'validators' => array('date' => true)); 
	 protected $_activo = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 
	 protected $_fecha_creacion = Array('value' => null, 'datatype' => DBTYpe::DateTime, 'validators' => array()); 
	 protected $_categoria = Array('value' => null, 'datatype' => DBType::String, 'validators' => array()); 

	 /** 
	  * Crea una nueva instacia del objeto proyectos. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	 public function set_id($p_id){ 
		 $this->_id['value'] = $p_id; 
	 } 
	 public function get_id(){ return $this->_id['value']; } 

	 public function set_area_id($p_area_id){ 
		 if(!DataValidator::validate($p_area_id, $this->_area_id['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>area_id</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_area_id['value'] = $p_area_id; 
	 } 
	 public function get_area_id(){ return $this->_area_id['value']; } 

	 public function set_tipo($p_tipo){ 
		 if(!DataValidator::validate($p_tipo, $this->_tipo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>tipo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_tipo['value'] = $p_tipo; 
	 } 
	 public function get_tipo(){ return $this->_tipo['value']; } 

	 public function set_titulo_esp($p_titulo_esp){ 
		 if(!DataValidator::validate($p_titulo_esp, $this->_titulo_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_esp['value'] = $p_titulo_esp; 
	 } 
	 public function get_titulo_esp(){ return $this->_titulo_esp['value']; } 

	 public function set_titulo_eng($p_titulo_eng){ 
		 if(!DataValidator::validate($p_titulo_eng, $this->_titulo_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>titulo_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_titulo_eng['value'] = $p_titulo_eng; 
	 } 
	 public function get_titulo_eng(){ return $this->_titulo_eng['value']; } 

	 public function set_copete_esp($p_copete_esp){ 
		 if(!DataValidator::validate($p_copete_esp, $this->_copete_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>copete_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_copete_esp['value'] = $p_copete_esp; 
	 } 
	 public function get_copete_esp(){ return $this->_copete_esp['value']; } 

	 public function set_copete_eng($p_copete_eng){ 
		 if(!DataValidator::validate($p_copete_eng, $this->_copete_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>copete_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_copete_eng['value'] = $p_copete_eng; 
	 } 
	 public function get_copete_eng(){ return $this->_copete_eng['value']; } 

	 public function set_descripcion_esp($p_descripcion_esp){ 
		 if(!DataValidator::validate($p_descripcion_esp, $this->_descripcion_esp['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_esp</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_esp['value'] = $p_descripcion_esp; 
	 } 
	 public function get_descripcion_esp(){ return $this->_descripcion_esp['value']; } 

	 public function set_descripcion_eng($p_descripcion_eng){ 
		 if(!DataValidator::validate($p_descripcion_eng, $this->_descripcion_eng['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>descripcion_eng</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_descripcion_eng['value'] = $p_descripcion_eng; 
	 } 
	 public function get_descripcion_eng(){ return $this->_descripcion_eng['value']; } 

	 public function set_fecha_inicio($p_fecha_inicio){ 
		 if(!DataValidator::validate($p_fecha_inicio, $this->_fecha_inicio['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_inicio</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_inicio['value'] = $p_fecha_inicio; 
	 } 
	 public function get_fecha_inicio(){ return $this->_fecha_inicio['value']; } 

	 public function set_fecha_fin($p_fecha_fin){ 
		 if(!DataValidator::validate($p_fecha_fin, $this->_fecha_fin['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_fin</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_fin['value'] = $p_fecha_fin; 
	 } 
	 public function get_fecha_fin(){ return $this->_fecha_fin['value']; } 

	 public function set_activo($p_activo){ 
		 if(!DataValidator::validate($p_activo, $this->_activo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>activo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_activo['value'] = $p_activo; 
	 } 
	 public function get_activo(){ return $this->_activo['value']; } 

	 public function set_fecha_creacion($p_fecha_creacion){ 
		 if(!DataValidator::validate($p_fecha_creacion, $this->_fecha_creacion['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>fecha_creacion</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_fecha_creacion['value'] = $p_fecha_creacion; 
	 } 
	 public function get_fecha_creacion(){ return $this->_fecha_creacion['value']; } 

	 public function set_categoria($p_categoria){ 
		if(!DataValidator::validate($p_categoria, $this->_categoria['validators'])) 
			throw new Exception('Error al establecer el valor de <strong>categoria</strong>:<br/>' . DataValidator::get_error_text()); 
		$this->_categoria['value'] = $p_categoria; 
	} 
	public function get_categoria(){ return $this->_categoria['value']; } 

	

	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) { 
		 $query = new DBQuery('SELECT * FROM proyectos WHERE id = {id}'); 
			 $query->addParam('id', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 
		 if(count($datos) > 0) { 
			 $this->_id['value'] = $datos[0]['id']; 
			 $this->_area_id['value'] = $datos[0]['area_id']; 
			 $this->_tipo['value'] = $datos[0]['tipo']; 
			 $this->_titulo_esp['value'] = $datos[0]['titulo_esp']; 
			 $this->_titulo_eng['value'] = $datos[0]['titulo_eng']; 
			 $this->_copete_esp['value'] = $datos[0]['copete_esp']; 
			 $this->_copete_eng['value'] = $datos[0]['copete_eng']; 
			 $this->_descripcion_esp['value'] = $datos[0]['descripcion_esp']; 
			 $this->_descripcion_eng['value'] = $datos[0]['descripcion_eng']; 
			 $this->_fecha_inicio['value'] = $datos[0]['fecha_inicio']; 
			 $this->_fecha_fin['value'] = $datos[0]['fecha_fin']; 
			 $this->_activo['value'] = $datos[0]['activo']; 
			 $this->_fecha_creacion['value'] = $datos[0]['fecha_creacion']; 
			 $this->_categoria['value'] = $datos[0]['categoria']; 
		 }else{ 
			 $this->_id['value'] = null; 
			 $this->_area_id['value'] = null; 
			 $this->_tipo['value'] = null; 
			 $this->_titulo_esp['value'] = ''; 
			 $this->_titulo_eng['value'] = ''; 
			 $this->_copete_esp['value'] = ''; 
			 $this->_copete_eng['value'] = ''; 
			 $this->_descripcion_esp['value'] = null; 
			 $this->_descripcion_eng['value'] = null; 
			 $this->_fecha_inicio['value'] = null; 
			 $this->_fecha_fin['value'] = null; 
			 $this->_activo['value'] = null; 
			 $this->_fecha_creacion['value'] = null; 
			 $this->_categoria['value'] = null;
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() { 
		 if($this->_id['value'] == null) { 
			 $query = new DBQuery('INSERT INTO proyectos(area_id, tipo, titulo_esp, titulo_eng, copete_esp, copete_eng, descripcion_esp, descripcion_eng, fecha_inicio, fecha_fin, activo, categoria)VALUES({area_id}, {tipo}, {titulo_esp}, {titulo_eng}, {copete_esp}, {copete_eng}, {descripcion_esp}, {descripcion_eng}, {fecha_inicio}, {fecha_fin}, {activo}, {categoria})'); 
			 $query->addParam('area_id', $this->_area_id['value'], $this->_area_id['datatype']); 
			 $query->addParam('tipo', $this->_tipo['value'], $this->_tipo['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('copete_esp', $this->_copete_esp['value'], $this->_copete_esp['datatype']); 
			 $query->addParam('copete_eng', $this->_copete_eng['value'], $this->_copete_eng['datatype']); 
			 $query->addParam('descripcion_esp', $this->_descripcion_esp['value'], $this->_descripcion_esp['datatype']); 
			 $query->addParam('descripcion_eng', $this->_descripcion_eng['value'], $this->_descripcion_eng['datatype']); 
			 $query->addParam('fecha_inicio', $this->_fecha_inicio['value'], $this->_fecha_inicio['datatype']); 
			 $query->addParam('fecha_fin', $this->_fecha_fin['value'], $this->_fecha_fin['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
			 $query->addParam('categoria', $this->_categoria['value'], $this->_categoria['datatype']); 
		 }else{ 
			 $query = new DBQuery('UPDATE proyectos SET area_id = {area_id}, tipo = {tipo}, titulo_esp = {titulo_esp}, titulo_eng = {titulo_eng}, copete_esp = {copete_esp}, copete_eng = {copete_eng}, descripcion_esp = {descripcion_esp}, descripcion_eng = {descripcion_eng}, fecha_inicio = {fecha_inicio}, fecha_fin = {fecha_fin}, activo = {activo}, categoria = {categoria} WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
			 $query->addParam('area_id', $this->_area_id['value'], $this->_area_id['datatype']); 
			 $query->addParam('tipo', $this->_tipo['value'], $this->_tipo['datatype']); 
			 $query->addParam('titulo_esp', $this->_titulo_esp['value'], $this->_titulo_esp['datatype']); 
			 $query->addParam('titulo_eng', $this->_titulo_eng['value'], $this->_titulo_eng['datatype']); 
			 $query->addParam('copete_esp', $this->_copete_esp['value'], $this->_copete_esp['datatype']); 
			 $query->addParam('copete_eng', $this->_copete_eng['value'], $this->_copete_eng['datatype']); 
			 $query->addParam('descripcion_esp', $this->_descripcion_esp['value'], $this->_descripcion_esp['datatype']); 
			 $query->addParam('descripcion_eng', $this->_descripcion_eng['value'], $this->_descripcion_eng['datatype']); 
			 $query->addParam('fecha_inicio', $this->_fecha_inicio['value'], $this->_fecha_inicio['datatype']); 
			 $query->addParam('fecha_fin', $this->_fecha_fin['value'], $this->_fecha_fin['datatype']); 
			 $query->addParam('activo', $this->_activo['value'], $this->_activo['datatype']); 
			 $query->addParam('fecha_creacion', $this->_fecha_creacion['value'], $this->_fecha_creacion['datatype']); 
			 $query->addParam('categoria', $this->_categoria['value'], $this->_categoria['datatype']); 
		 } 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($this->get_id() == null) { $this->set_id($this->_dbmanager->lastID()); } 
		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM proyectos WHERE id = {id}'); 
			 $query->addParam('id', $this->_id['value'], $this->_id['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 
		 $reg_x_pag = (isset($p_opciones['reg_x_pag'])) ? $p_opciones['reg_x_pag'] : CONF_REG_X_PAG; 
		 $num_pagina = (isset($p_opciones['num_pagina'])) ? $p_opciones['num_pagina'] : 1; 
		 $paginar = (isset($p_opciones['paginar'])) ? $p_opciones['paginar'] : true; 
		 $orden = (isset($p_opciones['orden'])) ? ' ORDER BY ' . $p_opciones['orden'] : ''; 

		 $sql_from = ' proyectos LEFT JOIN areas ON proyectos.area_id = areas.id  '; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		 $cant_paginas = ceil($cant_filas / $reg_x_pag); 
		 $num_inicio = (($num_pagina - 1) * $reg_x_pag); 

		 $sql = 'SELECT  proyectos.* , areas.area_esp as areas_area_esp , areas.area_eng as areas_area_eng  FROM ' . $sql_from . ' ' . $filtro . ' ' . $orden; 
		 if($paginar === true){ 
			 $sql.= ' LIMIT ' . $num_inicio . ', ' . $reg_x_pag; 
		 } 
		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos, 'cant_paginas' => $cant_paginas); 
		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "proyectos.id = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "areas.area_esp LIKE " . DBManager::formatSQLValue('%'.$p_valor.'%') . " "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(CAST(proyectos.tipo AS \"varchar\"(100))) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(proyectos.titulo_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(proyectos.titulo_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(proyectos.copete_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(proyectos.copete_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(proyectos.descripcion_esp) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(proyectos.descripcion_eng) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 if(preg_match('/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "proyectos.fecha_inicio = " . DBManager::formatSQLValue($p_valor, "Date") . " "; 
		 } 
		 if(preg_match('/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "proyectos.fecha_fin = " . DBManager::formatSQLValue($p_valor, "Date") . " "; 
		 } 
		 //$filtro.= ($filtro == '')?' ': ' OR '; 
		 //$filtro.= "LOWER(CAST(proyectos.activo AS \"varchar\"(100))) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>