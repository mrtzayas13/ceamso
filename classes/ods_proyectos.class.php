<?php 

/** 
 * ods_proyectos 
 * 
 * 19/06/2016 - Autor - Registrá en este espacio las modificaciones realizadas en la clase iniciando la linea con la fecha en que haces los cambios y tu nombre. No te olvide de cambiar el nro. de version. * 
 * @version	1.0 
 * @autor 		Marcelo Valdez - MarceVal95
 */ 

class ods_proyectos { 

	 protected $_dbmanager = null; 

	 // Propiedades del objeto que representan los campos de la tabla. 
	 protected $_id = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('required' => true, 'digits' => true)); 
	 protected $_id_proyectos = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true));
	 protected $_id_objetivo = Array('value' => null, 'datatype' => DBTYpe::Integer, 'validators' => array('digits' => true));


	 /** 
	  * Crea una nueva instacia del objeto ods_proyectos. Inicializa las propiedades del objeto.
	  */ 
	 public function __construct() { 
		 $this->_dbmanager = new DBManager(); 
	 } 

	 /************************************ 
	  * PROPIEDADES PUBLICAS DE LA CLASE * 
	  ************************************/ 

	public function set_id($p_id){ 
		$this->_id['value'] = $p_id; 
	}
	public function get_id(){ return $this->_id['value']; }
	
	
	public function set_id_proyectos($p_id_proyectos){
		 if(!DataValidator::validate($p_id_proyectos, $this->_id_proyectos['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>id_proyectos</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_id_proyectos['value'] = $p_id_proyectos; 
	 } 
	 public function get_id_proyectos(){ return $this->_id_proyectos['value']; } 

	 public function set_id_objetivo($p_id_objetivo){ 
		 if(!DataValidator::validate($p_id_objetivo, $this->_id_objetivo['validators'])) 
			 throw new Exception('Error al establecer el valor de <strong>id_objetivo</strong>:<br/>' . DataValidator::get_error_text()); 
		 $this->_id_objetivo['value'] = $p_id_objetivo; 
	 } 
	 public function get_id_objetivo(){ return $this->_id_objetivo['value']; } 


	 /************************************************** 
	  * METODOS PARA RECUPERACION Y GUARADADO DE DATOS * 
	  **************************************************/ 

	 /** 
	  * Recupera en las propiedades del objeto la información de un registro en la base de datos.
	  * @param Integer $p_id ID del registro a cargar. 
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function carga($p_id) {
         $query = new DBQuery('SELECT * FROM objetivo_proyectos WHERE id_proyectos = {id_proyectos}'); 
         $query->addParam('id_proyectos', $p_id, $this->_id['datatype']); 
		 $datos = $this->_dbmanager->executeQuery($query); 

		 if(count($datos) > 0) {
			 $this->_id['value'] = $datos[0]['id'];
			 $this->_id_proyectos['value'] = $datos[0]['id_proyectos']; 
			 $this->_id_objetivo['value'] = $datos[0]['id_objetivo'];
 
		 }else{
			 $this->_id['value'] = null; 
			 $this->_id_proyectos['value'] = ''; 
			 $this->_id_objetivo['value'] = '';
		 } 
		 return ($this->_id['value'] == null) ? false : true; 
	 } 

	 /** 
	  * Guarda la información de las propiedades en la BD.
	  * @return Boolean Verdadero cuando el registro se cargo correctamente. 
	  */ 
	 public function guarda() {
         $query = new DBQuery('INSERT INTO objetivo_proyectos(id_proyectos, id_objetivo)VALUES({id_proyectos}, {id_objetivo})');
         $query->addParam('id_proyectos', $this->_id_proyectos['value'], $this->_id_proyectos['datatype']);
         $query->addParam('id_objetivo', $this->_id_objetivo['value'], $this->_id_objetivo['datatype']); 

		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 

		 return ($filas_afectadas == -1)?false:true; 
	 } 

	 /** 
	  * Elimina un registro de la base de datos.
	  * @return Boolean Verdadero cuando el registro se elimino correctamente. 
	  */ 
	 public function elimina() { 
		 $query = new DBQuery('DELETE FROM objetivo_proyectos WHERE id_proyectos = {id_proyectos}'); 
			 $query->addParam('id_proyectos', $this->_id_proyectos['value'], $this->_id_proyectos['datatype']); 
		 $filas_afectadas = $this->_dbmanager->executeNonQuery($query); 
		 if($filas_afectadas == -1) { 
			 return false; 
		 } 
		 return true; 
	 } 

	 /** 
	  * Recupera lista de registros de la tabla.
	  * @param Array Lista de opciones utilizadas para recuperar los datos. 
	  * 	Las opciones validas son: filtro, buscar, reg_x_pag, num_pagina, paginar, orden. 
	  * @return Array Datos de la tabla. 
	  */ 
	 public static function lista($p_opciones) { 

		 $dbmanager = new DBManager(); 
		 $filtro = (isset($p_opciones['filtro'])) ? $p_opciones['filtro'] : ''; 
		 if(isset($p_opciones['buscar'])) { $filtro = self::recupera_filtro_global($p_opciones['buscar'], $filtro); } 

		 $sql_from = ' objetivo_proyectos'; 

		 $cant_filas = $dbmanager->executeScalar(new DBQuery('SELECT count(*) FROM ' . $sql_from . ' ' . $filtro)); 
		  
		 $sql = 'SELECT  objetivo_proyectos.*  FROM ' . $sql_from . ' ' . $filtro; 

		 $datos = $dbmanager->executeQuery(new DBQuery($sql)); 

		 $retorno = array('datos' => $datos); 

		 return $retorno; 
	 } 

	 protected static function recupera_filtro_global($p_valor, $p_filtro = '') { 
		 $filtro = ''; 
		 if(preg_match('/^\d+$/', $p_valor)) { 
			 $filtro.= ($filtro == '')?' ': ' OR '; 
			 $filtro.= "objetivo_proyectos.id_proyectos = " . DBManager::formatSQLValue($p_valor,"Integer") . " "; 
		 } 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(objetivo_proyectos.id_proyectos) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro.= ($filtro == '')?' ': ' OR '; 
		 $filtro.= "LOWER(objetivo_proyectos.id_objetivo) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 //$filtro.= ($filtro == '')?' ': ' OR '; 
		 //$filtro.= "LOWER(CAST(objetivo_proyectos.activo AS \"varchar\"(100))) LIKE LOWER(" . DBManager::formatSQLValue('%'.$p_valor.'%') . ") "; 
		 $filtro = (($p_filtro == '')?' WHERE (' : $p_filtro . ' AND (') . $filtro . ' ) '; 
		 return $filtro; 
	 } 

} 
?>