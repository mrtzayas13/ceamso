<?php include('inc/config.php'); ?>



<head>



<title>Llamados y licitaciones | <?=SITENAME;?></title>
<meta name="description" content="Listado de licitaciones activas e inactivas." />
<meta name="keywords" content="<?=GRALKEYS;?>, llamados, licitaciones, licitacion" />

<meta property="og:url" content="http://www.ceamso.org.py/llamados.php" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Llamados y licitaciones" />
<meta property="og:description" content="Listado de licitaciones activas e inactivas." />
<meta property="og:image" content="http://www.ceamso.org.py/images/ceamso-fb.jpg" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="400" />

<?php include('inc/head.php'); ?>

<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/moment-timezone-with-data.js"></script>
<script type="text/javascript" src="js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>


<script type="text/javascript"> 



	$(document).ready(function(){	

		cargaPaginacion(1);	

	});

	

	var pagina_actual = 1;

	function cargaPaginacion(p_pagina){

		var ordenar = $('#ordenar').val();
		
		var estado = $( "#estado:checked" ).length;
		 
		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 

		$.ajax({

			type: 'GET',

			url: '<?=CONF_SITE_URL?>ajax/llamados.php',

			data: { 

				p: p_pagina,
				orden : ordenar,
				estado : estado

			},

			success: function(p_html){

				pagina_actual = p_pagina;

				$('#contenido').html(p_html);
				$('[data-toggle="tooltip"]').tooltip();

				$('[data-countdown]').each(function() {
					var date = $(this).data('countdown');
					var finalDate = moment.tz(date, "America/Asuncion");
					var $this = $(this);
					$this.countdown(finalDate.toDate(), function(event) {

						$(this).html(event.strftime(''

						+ '<p><strong>%D</strong>DIAS</p>'

						+ '<p><strong>%H</strong>HORAS</p>'

						+ '<p><strong>%M</strong>MIN</p>'));

					});

				});

			    var $container = $('.listado-llamados');

			    // initialize

			    $container.masonry({
			      itemSelector: '.llamado',
			      columnWidth: 370,
			      gutter: 15
			    });

			    var msnry = $container.data('masonry');
			    $container.prepend( '<div class="item">foo</div>' );
				$container.masonry( 'reloadItems' );
				$container.masonry( 'layout' );
			}
		});
	}
</script>



</head>



<body class="sec-llamados">



	<?php include('inc/header.php'); ?>



	<section id="titulo">

		<div class="container">

			<h1 class="underline"><strong>Llamados y licitaciones</strong><span></span></h1>

		</div>

	</section>



	<section id="content">

		<div class="container">

			<fieldset class="row filter clear">

				<label for="ordenar" class="left pt10">Ordenar por</label>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

					<select name="ordenar" id="ordenar" class="class select" onchange="cargaPaginacion(1)">

						<option value="inicio">Fecha de publicacion</option>

						<option value="cierre">Fecha de cierre</option>

					</select>
	
				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pt10">
					<input type="checkbox" name="estado" onChange="cargaPaginacion(1)" id="estado" checked> Mostrar llamados inactivos
				</div>

				<div class="corte clear"></div>
				<a href="./#suscribirse" class="btn linea-gris right btn-susc">Suscribite al boletin de llamados</a>
				<a href="http://cv.ceamso.org.py" class="btn linea-gris right btn-cv" target="_BLANK">Registrá tu currículum</a>

			</fieldset>

			<div id="contenido">





			</div>

		</div>

	</section>



	<?php include('inc/footer.php'); ?>



</body>

</html>

