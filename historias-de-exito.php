<?php include('inc/config.php'); ?>

<head>

<title>Historias de Éxito | <?=SITENAME;?></title>
<meta name="description" content="Nuestros beneficiaros cuentan sus historias." />
<meta name="keywords" content="<?=GRALKEYS;?>, hisotiras, beneficiarios" />

<?php include('inc/head.php'); ?>

<script type="text/javascript"> 

	$(document).ready(function(){	
		cargaPaginacion(1);	
	});
	
	var pagina_actual = 1;
	function cargaPaginacion(p_pagina){

		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 
		$.ajax({
			type: 'GET',
			url: '<?=CONF_SITE_URL?>ajax/historias-de-exito.php',
			data: { p: p_pagina},
			success: function(p_html){
				pagina_actual = p_pagina;
				$('#contenido').html(p_html);
			}
		});
	}
</script>

</head>

<body class="sec-historias">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">
			<h1 class="underline">Historias de <strong>Éxito</strong><span></span></h1>
			<p class="copete">Nuestros beneficiarios cuentan sus historias</p>
		</div>
	</section>

	<section id="content">
		<div class="container" id="contenido">

			

		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
