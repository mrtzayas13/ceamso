<?php include('inc/config.php'); ?>

<head>

<title>Publicaciones | <?=SITENAME;?></title>
<meta name="description" content="Descargá nuestras publicaciones." />
<meta name="keywords" content="<?=GRALKEYS;?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript">
	$(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>


<script type="text/javascript"> 

	$(document).ready(function(){	
		cargaPaginacion(1,0);

		$(".btns-areas li a").click(function(){
			$(".btns-areas li a").removeClass('active');
			$(this).addClass('active');
		});	
	});
	
	var pagina_actual = 1;
	function cargaPaginacion(p_pagina, area_id ){

		$.blockUI({ message: '<br><h2>Aguarde...</h2><br>' }); 
		$.ajax({
			type: 'GET',
			url: '<?=CONF_SITE_URL?>ajax/publicaciones.php',
			data: { 
				p: p_pagina,
				area_id : area_id
			},
			success: function(p_html){
				pagina_actual = p_pagina;
				$('#contenido').html(p_html);
			}
		});
	}
</script>

</head>

<body class="sec-publicaciones">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">
			<h1 class="underline"><strong>Publicaciones</strong><span></span></h1>
		</div>
	</section>

	<section id="content">
		<div class="container">
			<ul class="btns-areas">
				<?php foreach($areas as $rs){?>
				<li><a href="javascript:;" onclick="cargaPaginacion(1,<?php echo $rs['id']?>)" rel="<?php echo $rs['area'.$idioma]?>"><?php echo $rs['area'.$idioma]?></a></li>
				<?php }?>
			</ul>
			<br>
			
			<div id="contenido">
				
				

			</div>

		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
