<?php include('inc/config.php'); ?>



<?php



$id = TextHelper::cleanNumber($_GET['id']);
$qnoticias = new DBQuery("SELECT *
							FROM historias_exitos n 
							WHERE n.activo = 'SI' AND n.id = '{$id}'");
$rs = $db->executeQuery($qnoticias);

if(empty($rs))
	header("Location: " . URL);

?>

<head>

<title>Titulo de Historia de exito | <?=SITENAME;?></title>
<meta name="description" content="" />
<meta name="keywords" content="<?=GRALKEYS;?>, noticias, novedades" />

<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $rs[0]['titulo'.$idioma]?>" />
<meta property="og:description" content="<?php echo strip_tags (trim($rs[0]['descripcion'.$idioma]))?>" />
<meta property="og:image" content="<?php echo CONF_SITE_URL."upload/historias_exitos/". $rs[0]['imagen']?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "a20e072a-7689-4359-b30f-f29d1e956e8a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slider-galeria-inn').slick({
			dots: false,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			adaptiveHeight: true
		});
	});
</script>

</head>

<body class="sec-historias historias-inn">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">

			<p class="breadcrumb"><a href="blog.php">Historias de éxito</a></p>
			<h1 class="underline"><strong><?php echo $rs[0]['titulo'.$idioma]?></strong><span></span></h1>

		</div>
	</section>

	<section id="content">
		<div class="container">
			<div class="row mb30">

				<div class="col-lg-8 col-lg-push-4 col-md-8 col-md-push-4 col-sm-12 texto-corrido">

					<p class="copete"><?php echo $rs[0]['copete'.$idioma]?></p>
					<?php echo $rs[0]['descripcion_largo'.$idioma]?>
				</div>

				<div class="col-lg-4 col-lg-pull-8 col-md-4 col-md-pull-8 col-sm-12 text-center">
					<article class="historia-exito">
						<figure class="img"><img src="<?php echo CONF_SITE_URL.'/upload/historias_exitos/'.$rs[0]['imagen'] ?>" alt=""></figure>
						<div class="content">
							<p href="#" class="desc"><img src="images/comilla.png" alt="'" class="mr10"><?php echo $rs[0]['cita'.$idioma]?><img src="images/comilla.png" alt="'" class="ml10"></p>
							<p class="nombre"><?php echo $rs[0]['nombre']?></p>
							<p class="cargo"><?php echo $rs[0]['institucion'.$idioma]?></p>
						</div>
					</article>

					<div class="compartir-block">
						<p><img src="images/ico-share.png" alt=""> COMPARTIR EN</p>
						<span class='st_facebook_large' displayText='Facebook'></span>
						<span class='st_twitter_large' displayText='Tweet'></span>
						<span class='st_googleplus_large' displayText='Google +'></span>
						<span class='st_linkedin_large' displayText='LinkedIn'></span>
						<span class='st_email_large' displayText='Email'></span>
					</div>

					<a href="historias-de-exito" class="btn linea-gris mt20" style="display: block;"><img src="images/back-arrow.png" alt="Volver">Volver</a>
				</div>

			</div>
		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>

