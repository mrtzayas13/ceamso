<?php include('inc/config.php'); ?>

<head>

<title>Staff y Consejo directivo | <?=SITENAME;?></title>
<meta name="description" content="Nuestro Staff, miembros fundadores y consejo directivo." />
<meta name="keywords" content="<?=GRALKEYS;?>" />

<?php include('inc/head.php'); ?>

<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
	    var $container = $('.listado-staff');
	    // initialize
	    $container.masonry({
	      itemSelector: '.staff',
	      columnWidth: 370,
	      gutter: 15
	    });
	    var msnry = $container.data('masonry');
	});

</script>

</head>

<body class="sec-ceamso">

	<?php include('inc/header.php'); ?>

	<section id="titulo">
		<div class="container">
			<!-- <p class="breadcrumb">¿Quiénes Somos?</p> -->
			<h1 class="underline">¿Quiénes <strong>Somos?</strong><span></span></h1>
		</div>
	</section>

	<section id="content">
		<div class="container">
			<h2 class="underline">MIEMBROS <strong>FUNDADORES</strong><span></span></h2>
			<div class="listado-staff">
				<article class="staff">
					<figure class="img"><img src="images/staff/patricia-franco-bogado.jpg" alt="Patricia Franco Bogado"></figure>
					<div class="content">
						<h3>Patricia Franco Bogado</h3>
					</div>
				</article>				

				<article class="staff">
					<figure class="img"><img src="images/staff/ofelia-yegros.jpg" alt="María Ofelia Yegros López"></figure>
					<div class="content">
						<h3>María Ofelia Yegros López</h3>
					</div>
				</article>

				<article class="staff">
					<figure class="img"><img src="images/staff/carlos-alberto-galarza-jara.jpg" alt="Carlos Alberto Galarza Jara"></figure>
					<div class="content">
						<h3>Carlos Alberto Galarza Jara</h3>
					</div>
				</article>			
			</div>
			<br>

			<h2 class="underline">CONSEJO <strong>DIRECTIVO</strong><span></span></h2>
			<div class="listado-staff">
				<article class="staff">
					<figure class="img"><img src="images/staff/jose-blas-villalba.jpg" alt="José Blás Villalba"></figure>
					<div class="content">
						<h3>José Blás Villalba</h3>
						<p class="cargo">Presidente del Consejo Directivo</p>
						<!-- <p></p> -->
					</div>
				</article>

				<article class="staff">
					<figure class="img"><img src="images/staff/ricardo-rodriguez-escobar.jpg" alt="Ricardo Rodríguez Escobar"></figure>
					<div class="content">
						<h3>Ricardo Rodríguez Escobar</h3>
						<p class="cargo">Secretario del Consejo Directivo</p>
						<!-- <p></p> -->
					</div>
				</article>

				<article class="staff">
					<figure class="img"><img src="images/staff/rossana-gomez.jpg" alt="Rossana Gomez"></figure>
					<div class="content">
						<h3>Rossana Gomez</h3>
						<p class="cargo">Miembro del Consejo Directivo</p>
						<!-- <p></p> -->
					</div>
				</article>
			</div>

		</div>
	</section>

	<?php include('inc/footer.php'); ?>

</body>
</html>
